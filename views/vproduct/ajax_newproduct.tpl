<div style="margin: 0 10px;">
    {include file="./cat_brands_list.tpl"}
</div>
{foreach from=$products item=pd}
    <section class="productListWrap hoz" onclick="location = '/?/vProduct/view/id={$pd.product_id}&showwxpaytitle=1';">
        <a class="productList{if $stype ne 'hoz'} clearfix{/if}">
            <img src="{$pd.catimg}" />
            <section>
                <title class="title">{$pd.product_name}</title>
                <span class='prices'>&yen;{$pd.sale_prices}</span>
            </section>
        </a>
    </section>
{/foreach}