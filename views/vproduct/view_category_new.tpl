<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>{$title} - {$settings.shopname}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="/static/css/wshop_view_category.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
    </head>
    <body style="overflow:hidden;padding:0;-webkit-overflow-scrolling: none;">
        
        <input type="hidden" value="{$Query.searchkey}" id="searchkey" />
        <input type="hidden" value="{$cat}" id="cat" />
        <input type="hidden" value="{$serial_id}" id="serial_id" />
        <input type="hidden" value="{$orderby}" id="orderby" />
        {include file="../global/search_box1.tpl"}
        <div id="viewCatRight"> </div>
        <script data-main="/static/script/Wshop/shop_vcategory.js?v={$cssversion}" src="http://apps.bdimg.com/libs/require.js/2.1.9/require.min.js"></script>
    </body>
</html>