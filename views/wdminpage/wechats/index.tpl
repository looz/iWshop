{include file='../__header_v2.tpl'}
<div class="pd15" ng-controller="wechats" ng-app="ngApp">
	<div class="pheader clearfix">
	        <div class="pull-right">
	            <button class="btn btn-default" ng-click="createBackup()"><i class="glyphicon glyphicon-plus"></i>添加公众号</button>
	        </div>
	</div>
	{literal}
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>公众号名称</th>
				<th>帐号</th>
				<th>原始帐号</th>
				<th>app_id</th>
				<th>app_secret</th>
				<th>token</th>
				<th>entry_hash</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<tr ng-repeat="wechat in wechats" >
			<td>{{wechat.wechat_name}}</td>
			<td>{{wechat.account}}</td>
			<td>{{wechat.original_account}}</td>
			<td>{{wechat.app_id}}</td>
			<td>{{wechat.app_secret}}</td>
			<td>{{wechat.token}}</td>
			<td>{{wechat.entry_hash}}</td>
			<td>
				<a href="javascript:;" ng-click="delete(wechat)">编辑</a> <a href="javascript:;" ng-click="delete(wechat)">删除</a>
			</td>
		</tr>
		</tbody>
	</table>
	{/literal}

</div>
{include file='../__footer.tpl'}
<script type="text/javascript" src="/static/script/Wdmin/wechats/wechats.js"></script>