{include file='../__header_v2.tpl'}
<link rel="stylesheet" href="/static/css/wshop_admin_auth.css">
<div class="pd15">
	{include file='../settings/auth_actions.html'}

    <form  method="post" action="">
    	<input name="group_id" type="hidden" value="{$data.group_id}">
		{foreach $data.rules  as $rule}
            <div class="rule-row">
                <div class="top">{($rule.title)?$rule.title:$rule.name}</div>
                <div class="main">
                    <div class="checkbox">
                        {foreach $rule['_child'] as $rl}

						{$checked=(in_array($rl['code'],$data.group_rules))?' checked="checked" ':''}
						
						{$active=($checked)?' active ':''}

                        <label class="button {$active}"><input name="rules[]" value="{$rl['code']}" type="checkbox" {$checked} />{($rl['title'])?$rl['title']:$rl['name']}</label>
                        {/foreach}

                    </div>
                </div>
            </div>
            {/foreach}
            <div class="clearfix"></div>

            <div class="fix_bottom fixed">
                <a class="btn btn-success" id='saveAccessBtn' style="width:150px" href="javascript:;">保存设置</a>
            </div>

            <br>
    </form>
	
</div>
{include file='../__footer.tpl'}
<i id="scriptTag">/static/script/Wdmin/settings/auth.js</i>