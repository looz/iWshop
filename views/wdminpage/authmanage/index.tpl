{include file='../__header_v2.tpl'}

<div class="pd15">
	{include file='../settings/auth_actions.html'}

	<table class="table table-hover table-bordered text-center">
	    <thead>
	        <tr>
	            <th> ID </th>
	            <th> 用户组 </th>
	            <th> 描述 </th>
	            <th> 授权 </th>
	            <th> 状态 </th>
	            <th> 操作 </th>
	        </tr>
	    </thead>
	    <tbody>

	    {foreach $groups.list as $group}
	        <tr>
	            <td field="id"> {$group.id} </td>
	            <td field="title"> {$group.title} </td>
	            <td field="description"> {$group.description} </td>
	            <td>
	                <a href="/?/AuthManage/allotGroupAccess/group_id={$group.id}">访问授权</a>
	                <a href="/?/AuthManage/allotGroupUser/group_id={$group.id}">成员授权</a>
	            </td>
	            <td field="status" class="status-{$group.status}"> {($group.status==1)?'启用':'禁用'} </td>
	            <td>
	                <a href="/?/AuthManage/changeStatus/group_id={$group.id}&status={$group.status}" class="icon-retweet">切换</a>
	                <a href="/?/AuthManage/editGroup/group_id={$group.id}" class="del" title="编辑组">编辑</a>
	            </td>
	        </tr>
	    {/foreach}
	    </tbody>
	</table>
</div>




{include file='../__footer.tpl'}