<?php

// 数据库host
$config->db['host'] = '__DBHOST__';

// 数据库端口
$config->db['port'] = '__DBPORT__';

// 数据库名
$config->db['db'] = '__DBNAME__';

// 数据库用户
$config->db['user'] = '__DBUSER__';

// 数据库密码
$config->db['pass'] = '__DBPASS__';