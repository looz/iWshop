<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/6/29
 * Time: 0:12
 */
interface iSession {

    public function start();

    /**
     *
     * @param type $key
     * @param type $value
     * @return type
     */
    public function set($key, $value);

    /**
     *
     * @param type $key
     * @return type
     */
    public function get($key);

    /**
     * 获取UID
     * @return type
     */
    public function getUID();

    /**
     * 获取OpenID
     * @return type
     */
    public function getOpenID();

    /**
     * 清空session
     * @return mixed
     */
    public function clear();

    /**
     * 删除一个session的key和value
     * @return: array
     */
    function del($key);

}