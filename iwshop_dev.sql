-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: 127.0.0.1	Database: iwshop_dev
-- ------------------------------------------------------
-- Server version 	5.5.5-10.1.13-MariaDB
-- Date: Tue, 09 Aug 2016 16:57:42 +0800

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(255) DEFAULT NULL,
  `admin_account` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_last_login` datetime DEFAULT NULL,
  `admin_ip_address` varchar(255) DEFAULT NULL,
  `admin_auth` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`admin_account`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='后台管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `admin` VALUES (1,'超级管理员','admin','4a0894d6e8f3b5c6ee0c519bcb98b6b7fd0affcb343ace3a093f29da4b2535604b61f0aebd60c0f0e49cc53adba3fffb','2016-08-09 15:46:28','127.0.0.1','stat,orde,prod,gmes,user,comp,sett',0);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `admin_login_records`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_login_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `ldate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='后台管理员登录记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_login_records`
--

LOCK TABLES `admin_login_records` WRITE;
/*!40000 ALTER TABLE `admin_login_records` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `admin_login_records` VALUES (1,'admin','127.0.0.1','2016-07-23 09:23:16'),(2,'admin','127.0.0.1','2016-07-23 09:24:02'),(3,'admin','127.0.0.1','2016-07-23 09:26:45'),(4,'admin','127.0.0.1','2016-07-23 09:26:51'),(5,'admin','127.0.0.1','2016-07-23 09:29:01'),(6,'admin','127.0.0.1','2016-07-25 08:30:52'),(7,'admin','127.0.0.1','2016-07-25 09:04:46'),(8,'admin','127.0.0.1','2016-07-25 11:25:03'),(9,'admin','127.0.0.1','2016-07-27 09:02:18'),(10,'admin','127.0.0.1','2016-07-27 17:41:02'),(11,'admin','127.0.0.1','2016-07-28 09:02:42'),(12,'admin','127.0.0.1','2016-07-28 17:13:53'),(13,'admin','127.0.0.1','2016-07-28 17:25:43'),(14,'admin','127.0.0.1','2016-07-28 17:33:49'),(15,'zsoner','127.0.0.1','2016-08-06 09:05:19'),(16,'zsoner','127.0.0.1','2016-08-06 09:05:30'),(17,'zsoner','127.0.0.1','2016-08-06 09:05:35'),(18,'admin','127.0.0.1','2016-08-06 09:05:41'),(19,'admin','127.0.0.1','2016-08-06 15:32:03'),(20,'admin','127.0.0.1','2016-08-06 18:01:41'),(21,'admin','127.0.0.1','2016-08-06 18:01:55'),(22,'admin','127.0.0.1','2016-08-06 18:02:10'),(23,'admin','127.0.0.1','2016-08-06 18:06:48'),(24,'zsoner','127.0.0.1','2016-08-08 09:01:11'),(25,'admin','127.0.0.1','2016-08-08 09:01:17'),(26,'admin','127.0.0.1','2016-08-08 10:00:22'),(27,'admin','127.0.0.1','2016-08-09 15:46:28');
/*!40000 ALTER TABLE `admin_login_records` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `articles`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumb_media_id` varchar(255) DEFAULT NULL COMMENT '图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得',
  `author` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content_source_url` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `digest` varchar(255) DEFAULT NULL,
  `show_cover_pic` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图文消息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `auth_group`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '用户组所属模块',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '组类型',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` text NOT NULL COMMENT '用户组拥有的规则code，多个规则 , 隔开',
  `update_time` int(10) unsigned NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='权限组表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `auth_group` VALUES (1,'',0,'测试组','',1,'1OxMW4,1CxHz3,I0MXo1,POQ45,1S7DU2,yQGtY1,1dHtF4,1gN6L3,1mSd24,THsPA1,1shp13,1ejRy3,1rH4Y2,1mf2e3,m7IPe',1469268393,0);
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `auth_group_access`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_access`
--

LOCK TABLES `auth_group_access` WRITE;
/*!40000 ALTER TABLE `auth_group_access` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `auth_group_access` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `auth_rule`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '规则所属module',
  `pid` int(8) NOT NULL DEFAULT '0',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;0-主菜单',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  `is_menu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是按钮',
  `code` varchar(12) NOT NULL DEFAULT '',
  `is_btn` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是按钮',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `module` (`status`,`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8 COMMENT='权限节点表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `auth_rule` VALUES (1,'',0,0,'AuthManage','',1,'',0,'5UhMm',0),(2,'',1,1,'AuthManage/index','查看列表',1,'',0,'1OxMW4',0),(3,'',1,1,'AuthManage/addGroup','新增组',1,'',0,'1CxHz3',0),(4,'',1,1,'AuthManage/editGroup','编辑组',1,'',0,'I0MXo1',0),(5,'',1,1,'AuthManage/allotGroupAccess','分配组权限',1,'',0,'1MVrA3',0),(6,'',1,1,'AuthManage/allotGroupUser','分配组用户',1,'',0,'2SazF2',0),(7,'',1,1,'AuthManage/addUserToGroup','新增用户到组',1,'',0,'aA0Tx',0),(8,'',1,1,'AuthManage/deleteGroupUser','删除组用户',1,'',0,'POQ45',0),(9,'',1,1,'AuthManage/changeStatus','改变状态',1,'',0,'1S7DU2',0),(10,'',1,1,'AuthManage/refreshNode','更新节点',1,'',0,'yQGtY1',0),(11,'',0,0,'FancyPage','',1,'',0,'Aks4p1',0),(12,'',11,1,'FancyPage/fancyAlterGroup','fancy弹出组',1,'',0,'Nh7Sg1',0),(13,'',11,1,'FancyPage/fancyAddGroup','fancy新增组',1,'',0,'quurE',0),(14,'',11,1,'FancyPage/orderRefund','订单退款',1,'',0,'Kgglo1',0),(15,'',11,1,'FancyPage/ajaxSelectProduct','动态选择产品',1,'',0,'1dHtF4',0),(16,'',11,1,'FancyPage/ajaxPdBlocks','动态pdblocks',1,'',0,'1GNDt2',0),(17,'',0,0,'WdminPage','',1,'',0,'16E0r3',0),(18,'',17,1,'WdminPage/index','查看列表',1,'',0,'1dFFG4',0),(19,'',17,1,'WdminPage/home','home',1,'',0,'Cbaqp1',0),(20,'',17,1,'WdminPage/orders_manage','订单管理',1,'',0,'PFmB31',0),(21,'',17,1,'WdminPage/orders_refund','订单退款',1,'',0,'7tEiH1',0),(22,'',17,1,'WdminPage/orders_comment','订单评论',1,'',0,'1fHNT3',0),(23,'',17,1,'WdminPage/ajax_orders_comment','动态订单评论',1,'',0,'oZRsi',0),(24,'',17,1,'WdminPage/list_products','列表产品',1,'',0,'erzi42',0),(25,'',17,1,'WdminPage/list_product_instock','列表产品库存',1,'',0,'1polO4',0),(26,'',17,1,'WdminPage/list_product_instock_in','列表产品库存in',1,'',0,'rmOv51',0),(27,'',17,1,'WdminPage/iframe_alter_product','框架弹出产品',1,'',0,'a7PwE1',0),(28,'',17,1,'WdminPage/iframe_list_product','框架列表产品',1,'',0,'1nocn3',0),(29,'',17,1,'WdminPage/ajax_add_category','动态新增分类',1,'',0,'1XNBg2',0),(30,'',17,1,'WdminPage/ajax_add_brand','动态新增品牌',1,'',0,'1MzMt3',0),(31,'',17,1,'WdminPage/alter_products_category','弹出产品分类',1,'',0,'1e0kU2',0),(32,'',17,1,'WdminPage/alter_category','弹出分类',1,'',0,'1gN6L3',0),(33,'',17,1,'WdminPage/gmess_list','素材列表',1,'',0,'zXNor1',0),(34,'',17,1,'WdminPage/gmess_sent','素材sent',1,'',0,'19gdm2',0),(35,'',17,1,'WdminPage/gmess_send','素材发送',1,'',0,'1mGqR2',0),(36,'',17,1,'WdminPage/ajax_gmess_list','动态素材列表',1,'',0,'1vjeR4',0),(37,'',17,1,'WdminPage/ajax_gmess_user_list','动态素材用户列表',1,'',0,'zMgZx',0),(38,'',17,1,'WdminPage/customer_messages','用户消息s',1,'',0,'1mSd24',0),(39,'',17,1,'WdminPage/message_session','消息session',1,'',0,'ZF2mW1',0),(40,'',17,1,'WdminPage/iframe_list_customer','框架列表用户',1,'',0,'g196k',0),(41,'',17,1,'WdminPage/list_customers','列表用户',1,'',0,'1lZeW4',0),(42,'',17,1,'WdminPage/deleted_products','已删除产品',1,'',0,'1D8VB3',0),(43,'',17,1,'WdminPage/alter_product_specs','弹出产品规格',1,'',0,'1E2L64',0),(44,'',17,1,'WdminPage/list_customer_orders','列表用户订单',1,'',0,'LkbcL1',0),(45,'',17,1,'WdminPage/list_companys','列表公司s',1,'',0,'THsPA1',0),(46,'',17,1,'WdminPage/company_withdrawal','公司withdrawal',1,'',0,'1oKqJ4',0),(47,'',17,1,'WdminPage/list_company_users','列表公司用户s',1,'',0,'1BVm84',0),(48,'',17,1,'WdminPage/settings_autoresponse','设置自动回复',1,'',0,'1Y3tt',0),(49,'',17,1,'WdminPage/iframe_alter_autoresponse','框架弹出自动回复',1,'',0,'HYSyS',0),(50,'',17,1,'WdminPage/settings_base','设置base',1,'',0,'EawES',0),(51,'',17,1,'WdminPage/settings_menu','设置菜单',1,'',0,'1shp13',0),(52,'',17,1,'WdminPage/ajax_alter_product_spec','动态弹出产品规格',1,'',0,'BVdIG1',0),(53,'',17,1,'WdminPage/ajax_alter_product_spec_detail','动态弹出产品规格详情',1,'',0,'HbtHV',0),(54,'',17,1,'WdminPage/ajax_alter_product_spec_rep','动态弹出产品规格rep',1,'',0,'1aMoF4',0),(55,'',17,1,'WdminPage/list_company_income','列表公司income',1,'',0,'1f25Z4',0),(56,'',17,1,'WdminPage/alter_company','弹出公司',1,'',0,'1CFtI3',0),(57,'',17,1,'WdminPage/company_bills','公司bills',1,'',0,'1ejRy3',0),(58,'',17,1,'WdminPage/alter_product_serials','弹出产品系列',1,'',0,'1HcGG3',0),(59,'',17,1,'WdminPage/iframe_alter_serial','框架弹出系列',1,'',0,'1Ww394',0),(60,'',17,1,'WdminPage/orders_history_address','订单历史新增ress',1,'',0,'52HU61',0),(61,'',17,1,'WdminPage/iframe_alter_customer','框架弹出用户',1,'',0,'18uHR4',0),(62,'',17,1,'WdminPage/sale_trend','销售trend',1,'',0,'QIgjj',0),(63,'',17,1,'WdminPage/area_ans','areaans',1,'',0,'1rH4Y2',0),(64,'',17,1,'WdminPage/user_ans','用户ans',1,'',0,'1f5qR4',0),(65,'',17,1,'WdminPage/com_sale','com销售',1,'',0,'p2riJ',0),(66,'',17,1,'WdminPage/overview','overview',1,'',0,'Qm0XE2',0),(67,'',17,1,'WdminPage/customer_profile','用户profile',1,'',0,'bxHBT1',0),(68,'',17,1,'WdminPage/alter_brand','弹出品牌',1,'',0,'dOx1R',0),(69,'',17,1,'WdminPage/alter_product_brand','弹出产品品牌',1,'',0,'mUWGi',0),(70,'',17,1,'WdminPage/corporation','corporation',1,'',0,'1Lyaj3',0),(71,'',17,1,'WdminPage/addEnterprise','新增enterprise',1,'',0,'1D8NL3',0),(72,'',17,1,'WdminPage/settings_banners','设置广告图s',1,'',0,'e70Za1',0),(73,'',17,1,'WdminPage/settings_banner_edit','设置广告图编辑',1,'',0,'nKQSR',0),(74,'',17,1,'WdminPage/settings_ads','设置ads',1,'',0,'mQjhR',0),(75,'',17,1,'WdminPage/settings_expfee','设置expfee',1,'',0,'gdbp81',0),(76,'',17,1,'WdminPage/user_level','用户级别',1,'',0,'GkV182',0),(77,'',17,1,'WdminPage/settings_alter_envs','设置弹出红包',1,'',0,'3KgKX1',0),(78,'',17,1,'WdminPage/settings_envs','设置红包',1,'',0,'KHl29',0),(79,'',17,1,'WdminPage/settings_auth','设置认证',1,'',0,'01U8b',0),(80,'',17,1,'WdminPage/auth_edit','认证编辑',1,'',0,'Di2NA2',0),(81,'',17,1,'WdminPage/user_envsend','用户红包发送',1,'',0,'vPVat',0),(82,'',17,1,'WdminPage/settings_expcompany','设置exp公司',1,'',0,'CezWh',0),(83,'',17,1,'WdminPage/alter_section','弹出section',1,'',0,'1lTnv3',0),(84,'',17,1,'WdminPage/settings_section','设置section',1,'',0,'35VWj1',0),(85,'',17,1,'WdminPage/settings_envs_robb','设置红包robb',1,'',0,'1mf2e3',0),(86,'',17,1,'WdminPage/envsRobList','红包rob列表',1,'',0,'jITG',0),(87,'',17,1,'WdminPage/gmess_category','素材分类',1,'',0,'1dZUA3',0),(88,'',17,1,'WdminPage/suppliers_list','suppliers列表',1,'',0,'creGu1',0),(89,'',17,1,'WdminPage/credit_exchange','信用卡ex改变',1,'',0,'qjqvx1',0),(90,'',17,1,'WdminPage/user_feedbacks','用户反馈s',1,'',0,'1VRk23',0),(91,'',17,1,'WdminPage/orders_withdrawal','订单withdrawal',1,'',0,'m7IPe',0),(92,'',17,1,'WdminPage/orders_express_record','订单物流记录',1,'',0,'o8Kv41',0),(93,'',17,1,'WdminPage/settings_navigation','设置导航',1,'',0,'1ij9c2',0),(94,'',17,1,'WdminPage/alter_navigation','弹出导航',1,'',0,'1t0Hx3',0),(95,'',17,1,'WdminPage/order_stat','订单信息',1,'',0,'tHyIm1',0),(96,'',17,1,'WdminPage/company_tree','公司tree',1,'',0,'9pUbT',0),(97,'',17,1,'WdminPage/company_rebates','公司回扣',1,'',0,'6lZO22',0),(98,'',17,1,'WdminPage/company_rebates_record','公司回扣记录',1,'',0,'5esw01',0),(99,'',17,1,'WdminPage/company_setting','公司设置ting',1,'',0,'1wJw93',0),(100,'',17,1,'WdminPage/company_level','公司级别',1,'',0,'YAymm1',0),(101,'',17,1,'WdminPage/user_balance_record','用户流水记录',1,'',0,'1d9zW4',0),(102,'',17,1,'WdminPage/user_credit_record','用户信用卡记录',1,'',0,'2Ae5L2',0),(103,'',0,0,'WdminStat','',1,'',0,'1bV3E4',0),(104,'',103,1,'WdminStat/update_wechat_stat','更新微信信息',1,'',0,'1tpCi3',0),(105,'',103,1,'WdminStat/check_wechat_stat_status','检查微信信息状态',1,'',0,'10c4D4',0),(106,'',103,1,'WdminStat/getSaleStat','获取销售信息',1,'',0,'uwQK42',0),(107,'',103,1,'WdminStat/getSalePercent','获取销售所占比',1,'',0,'1Ndxr2',0),(108,'',103,1,'WdminStat/getHotSaleProduct','获取热销产品',1,'',0,'1LEeX3',0),(109,'',103,1,'WdminStat/getUserStat','获取用户信息',1,'',0,'1688H3',0),(110,'',103,1,'WdminStat/getUserSexPercent','获取用户sex所占比',1,'',0,'u5AYG2',0),(111,'',103,1,'WdminStat/getHotBuyUser','获取hotbuy用户',1,'',0,'1Gh7o3',0),(112,'',103,1,'WdminStat/getCompanyUserPercent','获取公司用户所占比',1,'',0,'5x3lY1',0),(113,'',0,0,'wBoard','',1,'',0,'1NCwX4',0),(114,'',113,1,'wBoard/getList','获取列表',1,'',0,'L7JIG1',0),(115,'',113,1,'wBoard/addBoard','新增board',1,'',0,'1faTc4',0),(116,'',113,1,'wBoard/deleteBoard','删除board',1,'',0,'1tLgn3',0),(117,'',0,0,'wBrands','',1,'',0,'WtmHH2',0),(118,'',117,1,'wBrands/vBrand','v品牌',1,'',0,'1rHwg3',0),(119,'',117,1,'wBrands/create','新增',1,'',0,'8UaBu',0),(120,'',117,1,'wBrands/set','设置',1,'',0,'1TRKS4',0),(121,'',117,1,'wBrands/del','删除',1,'',0,'1BT7Z4',0),(122,'',117,1,'wBrands/gets','获取',1,'',0,'cxss2',0),(123,'',117,1,'wBrands/uploadImage','上传图片',1,'',0,'1OxBf2',0),(124,'',0,0,'wCompany','',1,'',0,'1Ke154',0),(125,'',124,1,'wCompany/getInfo','获取信息',1,'',0,'1W48p3',0),(126,'',124,1,'wCompany/modify','修改',1,'',0,'JdTW32',0),(127,'',124,1,'wCompany/getList','获取列表',1,'',0,'QuRXk1',0),(128,'',124,1,'wCompany/deleteCompany','删除公司',1,'',0,'uOdtV1',0),(129,'',124,1,'wCompany/getUnVerifedCount','获取un已验证数目',1,'',0,'aASeA2',0),(130,'',124,1,'wCompany/getVerifedCount','获取已验证数目',1,'',0,'16ufT2',0),(131,'',124,1,'wCompany/companyReqDeny','公司reqdeny',1,'',0,'1FveS4',0),(132,'',124,1,'wCompany/setCompanyAgreement','设置公司agreement',1,'',0,'15jAO2',0),(133,'',0,0,'wCompanyLevel','',1,'',0,'1hCkM4',0),(134,'',133,1,'wCompanyLevel/deleteLevel','删除级别',1,'',0,'wd0mP',0),(135,'',133,1,'wCompanyLevel/modi','修改',1,'',0,'12Vjl2',0),(136,'',133,1,'wCompanyLevel/getInfo','获取信息',1,'',0,'11jOZ3',0),(137,'',133,1,'wCompanyLevel/getList','获取列表',1,'',0,'UH4HQ1',0),(138,'',133,1,'wCompanyLevel/getListAll','获取列出所有',1,'',0,'TUR8i1',0),(139,'',0,0,'wCredit','',1,'',0,'MxQ7Y',0),(140,'',139,1,'wCredit/modify','修改',1,'',0,'15guf3',0),(141,'',139,1,'wCredit/delete','删除',1,'',0,'UR866',0),(142,'',139,1,'wCredit/add','新增',1,'',0,'7Xeek',0),(143,'',0,0,'wEnvs','',1,'',0,'1oWpr3',0),(144,'',143,1,'wEnvs/send','发送',1,'',0,'yua4n1',0),(145,'',0,0,'wFeedBack','',1,'',0,'1OFir2',0),(146,'',145,1,'wFeedBack/getList','获取列表',1,'',0,'lsjHK',0),(147,'',145,1,'wFeedBack/deleteFeedBack','删除反馈',1,'',0,'KOQFD2',0),(148,'',0,0,'wGmess','',1,'',0,'zyoT8',0),(149,'',148,1,'wGmess/getGmessList','获取素材列表',1,'',0,'1lrVx3',0),(150,'',148,1,'wGmess/ajaxDelByMsgId','动态删除通过msgid',1,'',0,'6N4zD1',0),(151,'',148,1,'wGmess/gmess_edit','素材编辑',1,'',0,'1cyfC4',0),(152,'',148,1,'wGmess/getCloudCategorys','获取云分类s',1,'',0,'1Qm403',0),(153,'',148,1,'wGmess/getCloudList','获取云列表',1,'',0,'1ixCH4',0),(154,'',148,1,'wGmess/cloneGmess','克隆素材',1,'',0,'809T4',0),(155,'',148,1,'wGmess/sendGmessNWay','发送素材nway',1,'',0,'14TOe4',0),(156,'',148,1,'wGmess/alterGmessPage','弹出素材页',1,'',0,'AqSGv1',0),(157,'',148,1,'wGmess/sendGemss','发送gemss',1,'',0,'dxyLS1',0),(158,'',0,0,'wImages','',1,'',0,'1jAX94',0),(159,'',158,1,'wImages/ImageUpload','图片上传',1,'',0,'1e85M2',0),(160,'',0,0,'wOrder','',1,'',0,'frliY1',0),(161,'',160,1,'wOrder/order_exports','订单导出',1,'',0,'1quvL2',0),(162,'',160,1,'wOrder/getOrderInfo','获取订单信息',1,'',0,'19A1j2',0),(163,'',160,1,'wOrder/getOrderList','获取订单列表',1,'',0,'gv2MV',0),(164,'',160,1,'wOrder/deleteOrder','删除订单',1,'',0,'1l8UQ3',0),(165,'',160,1,'wOrder/ajaxGetOrderStatnums','动态获取订单数目',1,'',0,'aAY852',0),(166,'',160,1,'wOrder/expressSend','物流发送',1,'',0,'1f5jI4',0),(167,'',160,1,'wOrder/getExpressCompanys','获取物流公司s',1,'',0,'1KofS2',0),(168,'',160,1,'wOrder/getExpressStaff','获取物流staff',1,'',0,'1cVTp3',0),(169,'',160,1,'wOrder/getExpressStaffHistroy','获取物流staffhistroy',1,'',0,'1BDhS3',0),(170,'',160,1,'wOrder/ajaxLoadOrderExpress','动态加载订单物流',1,'',0,'jCauk',0),(171,'',160,1,'wOrder/get_refund_record','获取退款记录',1,'',0,'wLduC1',0),(172,'',160,1,'wOrder/refund','退款',1,'',0,'18xA64',0),(173,'',160,1,'wOrder/getExpressRecords','获取物流记录s',1,'',0,'wVY9D2',0),(174,'',160,1,'wOrder/getStatList','获取tat列表',1,'',0,'1MCsv3',0),(175,'',160,1,'wOrder/getOrderAddresses','获取订单地址',1,'',0,'1Y4WS4',0),(176,'',160,1,'wOrder/orderPayed','订单已支付',1,'',0,'BcBik1',0),(177,'',0,0,'wProduct','',1,'',0,'ZWN8D1',0),(178,'',177,1,'wProduct/ajax_product_list_stock','动态产品列表库存',1,'',0,'1K0Is2',0),(179,'',177,1,'wProduct/ajax_product_list','动态产品列表',1,'',0,'1bD6d4',0),(180,'',177,1,'wProduct/switchOnline','切换在线',1,'',0,'1ygYh2',0),(181,'',177,1,'wProduct/ajaxGetCategroys','动态获取categroys',1,'',0,'1ljas3',0),(182,'',177,1,'wProduct/ajaxAlterCategroy','动态弹出categroy',1,'',0,'14uWb4',0),(183,'',177,1,'wProduct/ajaxDelCategroy','动态删除categroy',1,'',0,'1cBJF3',0),(184,'',177,1,'wProduct/generateStaticDesc','生成静态desc',1,'',0,'1PqHR4',0),(185,'',177,1,'wProduct/productReverse','产品reverse',1,'',0,'1YVqV2',0),(186,'',177,1,'wProduct/removeProduct','删除产品',1,'',0,'1ld5Q2',0),(187,'',177,1,'wProduct/ajaxAddCategroy','动态新增categroy',1,'',0,'1Le2i2',0),(188,'',177,1,'wProduct/ImageUpload','图片上传',1,'',0,'hTj3j1',0),(189,'',0,0,'wRebate','',1,'',0,'E72xB',0),(190,'',189,1,'wRebate/getRebateList','获取回扣列表',1,'',0,'f7FHb',0),(191,'',189,1,'wRebate/getRebateRules','获取回扣规则s',1,'',0,'1NR5a2',0),(192,'',189,1,'wRebate/checkRebate','检查回扣',1,'',0,'kTsHH1',0),(193,'',189,1,'wRebate/alterRuleInfo','弹出规则信息',1,'',0,'pGoBl',0),(194,'',189,1,'wRebate/deleteRule','删除规则',1,'',0,'jRpTa',0),(195,'',189,1,'wRebate/comfirmRebate','确认回扣',1,'',0,'ExbFd1',0),(196,'',189,1,'wRebate/rebateCheck','回扣检查',1,'',0,'1haZK3',0),(197,'',189,1,'wRebate/getRebateCount','获取回扣数目',1,'',0,'1obAM2',0),(198,'',0,0,'wSettings','',1,'',0,'aWTSD',0),(199,'',198,1,'wSettings/updateSettings','更新设置',1,'',0,'uFwB31',0),(200,'',198,1,'wSettings/ajaxGetSettings','动态获取ettings',1,'',0,'1CWT93',0),(201,'',198,1,'wSettings/ajaxGetExpTemplate','动态获取物流模板',1,'',0,'1T8en3',0),(202,'',198,1,'wSettings/updateExpTemplate','更新物流模板',1,'',0,'1mWFc2',0),(203,'',198,1,'wSettings/addEnvs','新增红包',1,'',0,'AVDS92',0),(204,'',198,1,'wSettings/delteEnvs','删除te红包',1,'',0,'1y8Bd2',0),(205,'',198,1,'wSettings/alterSection','弹出section',1,'',0,'1tJGT4',0),(206,'',198,1,'wSettings/clearEnvsRobRecord','清除红包rob记录',1,'',0,'JFIui',0),(207,'',198,1,'wSettings/ajaxAlterEnvs','动态弹出红包',1,'',0,'osWs92',0),(208,'',198,1,'wSettings/deleteEnvsRob','删除红包rob',1,'',0,'1gJ674',0),(209,'',198,1,'wSettings/getAccount','获取账户',1,'',0,'1cDJ43',0),(210,'',198,1,'wSettings/getAccounts','获取账户s',1,'',0,'1G9FM2',0),(211,'',198,1,'wSettings/deleteAuth','删除认证',1,'',0,'1hZnA3',0),(212,'',198,1,'wSettings/addAuth','新增认证',1,'',0,'U5P0W1',0),(213,'',198,1,'wSettings/alterNavigation','弹出导航',1,'',0,'7PPjs',0),(214,'',198,1,'wSettings/ajaxGetWechatMenu','动态获取微信菜单',1,'',0,'1GGgv3',0),(215,'',0,0,'wSupplier','',1,'',0,'lu9e92',0),(216,'',215,1,'wSupplier/delete','删除',1,'',0,'1hJJr2',0),(217,'',215,1,'wSupplier/modi','修改',1,'',0,'n9ZlK1',0),(218,'',0,0,'wSystem','',1,'',0,'1ic3j3',0),(219,'',218,1,'wSystem/logs','日志',1,'',0,'1kZg44',0),(220,'',218,1,'wSystem/getSystemLogs','获取系统日志',1,'',0,'U32pK2',0),(221,'',0,0,'wUser','',1,'',0,'10ihM2',0),(222,'',221,1,'wUser/getUserList','获取用户列表',1,'',0,'135bA3',0),(223,'',221,1,'wUser/getUserCount','获取用户数目',1,'',0,'BHYzy1',0),(224,'',221,1,'wUser/getUserLevel','获取用户级别',1,'',0,'aW1072',0),(225,'',221,1,'wUser/getUserLevelInfo','获取用户级别信息',1,'',0,'1VKLL3',0),(226,'',221,1,'wUser/alterUserLevelInfo','弹出用户级别信息',1,'',0,'1fNwr2',0),(227,'',221,1,'wUser/deleteLevel','删除级别',1,'',0,'1nzzM3',0),(228,'',221,1,'wUser/deleteUser','删除用户',1,'',0,'9vOJA2',0),(229,'',221,1,'wUser/getUserInfo','获取用户信息',1,'',0,'Lkm3d',0),(230,'',221,1,'wUser/alterUser','弹出用户',1,'',0,'a79eG1',0),(231,'',221,1,'wUser/user_exports','用户导出',1,'',0,'WRwBh',0),(232,'',221,1,'wUser/getUserBalanceRecord','获取用户流水记录',1,'',0,'aIndA',0),(233,'',221,1,'wUser/getUserCreditRecord','获取用户信用卡记录',1,'',0,'3jYuD2',0),(234,'',0,0,'wWithdrawal','',1,'',0,'x07NI2',0),(235,'',234,1,'wWithdrawal/getList','获取列表',1,'',0,'gYEkd1',0),(236,'',234,1,'wWithdrawal/audit','audit',1,'',0,'1Zdwe4',0);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_addresses`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_addresses` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `uname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `dist` varchar(255) DEFAULT NULL,
  `addrs` varchar(255) DEFAULT NULL,
  `poscode` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户地址信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_addresses`
--

LOCK TABLES `client_addresses` WRITE;
/*!40000 ALTER TABLE `client_addresses` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_addresses` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_autoenvs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_autoenvs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `envid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户关注自动红包';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_autoenvs`
--

LOCK TABLES `client_autoenvs` WRITE;
/*!40000 ALTER TABLE `client_autoenvs` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_autoenvs` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_balance_records`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_balance_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `rtype` enum('default','rebate','deposit','withdrawal') DEFAULT 'default',
  `rtime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户余额记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_balance_records`
--

LOCK TABLES `client_balance_records` WRITE;
/*!40000 ALTER TABLE `client_balance_records` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_balance_records` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_bank_card`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_bank_card` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  `sub` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `dist` varchar(255) DEFAULT NULL,
  `cardno` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户银行卡数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_bank_card`
--

LOCK TABLES `client_bank_card` WRITE;
/*!40000 ALTER TABLE `client_bank_card` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_bank_card` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_cart`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) NOT NULL COMMENT '用户编号',
  `product_id` int(11) NOT NULL COMMENT '商品编号',
  `spec_id` int(11) DEFAULT '0' COMMENT '商品规格',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '商品数量',
  PRIMARY KEY (`id`),
  KEY `index_openid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户购物车';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_cart`
--

LOCK TABLES `client_cart` WRITE;
/*!40000 ALTER TABLE `client_cart` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_cart` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_credit_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_credit_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `amount` int(5) DEFAULT NULL,
  `dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `reltype` tinyint(2) DEFAULT NULL,
  `relid` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `rtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户积分记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_credit_record`
--

LOCK TABLES `client_credit_record` WRITE;
/*!40000 ALTER TABLE `client_credit_record` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_credit_record` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_deposit_order`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_deposit_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `amount` float(11,2) DEFAULT '0.00',
  `deposit_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `deposit_status` enum('wait','payed') DEFAULT 'wait',
  `deposit_serial` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `wepay_serial` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_openid` (`openid`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户充值表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_deposit_order`
--

LOCK TABLES `client_deposit_order` WRITE;
/*!40000 ALTER TABLE `client_deposit_order` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_deposit_order` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_envelopes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_envelopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `envid` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `exp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户红包表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_envelopes`
--

LOCK TABLES `client_envelopes` WRITE;
/*!40000 ALTER TABLE `client_envelopes` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `client_envelopes` VALUES (1,'oIGpIxO4u3TBR5RnABr3EpiSH2o4',1,1,1,'2017-06-02 00:00:00');
/*!40000 ALTER TABLE `client_envelopes` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_envelopes_type`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_envelopes_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `req_amount` float DEFAULT NULL,
  `dis_amount` float DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户红包类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_envelopes_type`
--

LOCK TABLES `client_envelopes_type` WRITE;
/*!40000 ALTER TABLE `client_envelopes_type` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `client_envelopes_type` VALUES (1,'满100减10块',0,100,10,'2475,2376,2377,2378,2379,2380,2381,2382,2383,2384,2385,2386,2387,2388,2389,2390,2426,2427,2428,2434,2435,2436,2456,2457,2458,2459,2460','');
/*!40000 ALTER TABLE `client_envelopes_type` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_feedbacks`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `feedback` text,
  `ftime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户反馈信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_feedbacks`
--

LOCK TABLES `client_feedbacks` WRITE;
/*!40000 ALTER TABLE `client_feedbacks` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_feedbacks` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_level`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL DEFAULT '',
  `level_credit` int(11) NOT NULL,
  `level_discount` float DEFAULT NULL,
  `level_credit_feed` float DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `upable` tinyint(1) DEFAULT '1',
  `isdefault` tinyint(1) DEFAULT '0' COMMENT '是否默认分组',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户分组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_level`
--

LOCK TABLES `client_level` WRITE;
/*!40000 ALTER TABLE `client_level` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_level` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_message_session`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_message_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `unread` int(11) DEFAULT '0',
  `undesc` varchar(255) DEFAULT NULL,
  `lasttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_message_session`
--

LOCK TABLES `client_message_session` WRITE;
/*!40000 ALTER TABLE `client_message_session` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_message_session` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_messages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `msgtype` tinyint(2) DEFAULT '0',
  `msgcont` text,
  `msgdirect` tinyint(4) DEFAULT '0',
  `autoreped` tinyint(4) DEFAULT '0',
  `send_time` datetime DEFAULT NULL,
  `sreaded` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_messages`
--

LOCK TABLES `client_messages` WRITE;
/*!40000 ALTER TABLE `client_messages` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_messages` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_order_address`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_order_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单地址信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_order_address`
--

LOCK TABLES `client_order_address` WRITE;
/*!40000 ALTER TABLE `client_order_address` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_order_address` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_product_likes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_product_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `like_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni` (`openid`,`product_id`) USING BTREE,
  KEY `uopenid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户商品收藏';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_product_likes`
--

LOCK TABLES `client_product_likes` WRITE;
/*!40000 ALTER TABLE `client_product_likes` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_product_likes` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_sign_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_sign_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dt` date DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `openid` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dt` (`dt`,`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户签到记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_sign_record`
--

LOCK TABLES `client_sign_record` WRITE;
/*!40000 ALTER TABLE `client_sign_record` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_sign_record` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_urecord`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_urecord` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `amount` float(11,2) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_urecord`
--

LOCK TABLES `client_urecord` WRITE;
/*!40000 ALTER TABLE `client_urecord` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_urecord` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `client_withdrawal_order`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_withdrawal_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `openid` varchar(255) NOT NULL,
  `amount` float(11,2) NOT NULL DEFAULT '0.00',
  `username` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  `subbranch` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `dist` varchar(255) DEFAULT NULL,
  `cardno` varchar(255) DEFAULT NULL,
  `rtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('wait','reject','pass') DEFAULT 'wait',
  `serial` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_openid` (`openid`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户提现表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_withdrawal_order`
--

LOCK TABLES `client_withdrawal_order` WRITE;
/*!40000 ALTER TABLE `client_withdrawal_order` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `client_withdrawal_order` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `clients`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `client_id` int(25) NOT NULL AUTO_INCREMENT COMMENT '会员卡号',
  `client_nickname` varchar(512) NOT NULL,
  `client_name` varchar(512) NOT NULL COMMENT '会员姓名',
  `client_sex` varchar(1) DEFAULT NULL COMMENT '会员性别',
  `client_phone` varchar(20) NOT NULL DEFAULT '' COMMENT '会员电话',
  `client_email` varchar(255) DEFAULT NULL,
  `client_head` varchar(255) DEFAULT NULL,
  `client_head_lastmod` datetime DEFAULT NULL,
  `client_password` varchar(255) DEFAULT '' COMMENT '会员密码',
  `client_level` tinyint(3) DEFAULT '0' COMMENT '会员种类\\r\\n1为普通会员\\r\\n0为合作商',
  `client_wechat_openid` varchar(50) NOT NULL DEFAULT '' COMMENT '会员微信openid',
  `client_joindate` date NOT NULL,
  `client_province` varchar(60) DEFAULT NULL,
  `client_city` varchar(60) DEFAULT NULL,
  `client_address` varchar(60) DEFAULT '' COMMENT '会员住址',
  `client_money` float(15,2) NOT NULL DEFAULT '0.00' COMMENT '会员存款',
  `client_credit` int(15) NOT NULL DEFAULT '0' COMMENT '会员积分',
  `client_remark` varchar(255) DEFAULT '' COMMENT '会员备注',
  `client_groupid` int(11) DEFAULT '0',
  `client_storeid` int(10) DEFAULT '0' COMMENT '会员所属店号',
  `client_personid` varchar(255) DEFAULT NULL,
  `client_comid` int(11) DEFAULT '0' COMMENT '代理编号',
  `client_autoenvrec` tinyint(4) DEFAULT '0',
  `client_overdraft_amount` float(11,2) DEFAULT '0.00' COMMENT '用户信用总额',
  `is_com` tinyint(4) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `index_openid` (`client_wechat_openid`) USING BTREE,
  KEY `index_comid` (`client_comid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `clients` VALUES (1,'Zsoner','Zsoner','m','','','http://wx.qlogo.cn/mmopen/zRLVLaGEokVW2e9kT1mPMHviaurKYoD1tETRXWPGU5hmGR6gSUlQTkzIJ6rcwhcjQib2wjQ8Uos2wCZo2Uico0osibma9UibT1v4q','2016-07-22 09:29:03','',0,'oIGpIxO4u3TBR5RnABr3EpiSH2o4','2016-07-22','浙江','宁波','浙江宁波',0.00,0,'',0,0,'',0,0,0.00,1,0);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `company_bills`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comid` int(11) DEFAULT NULL,
  `bill_amount` float(10,2) DEFAULT NULL,
  `bill_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理账单信息, 废弃';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_bills`
--

LOCK TABLES `company_bills` WRITE;
/*!40000 ALTER TABLE `company_bills` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `company_bills` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `company_income_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_income_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float(11,2) NOT NULL DEFAULT '0.00',
  `date` datetime NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `com_id` varchar(255) NOT NULL,
  `pcount` int(11) NOT NULL,
  `is_seted` tinyint(4) DEFAULT '0',
  `is_reqed` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理收入记录, 废弃';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_income_record`
--

LOCK TABLES `company_income_record` WRITE;
/*!40000 ALTER TABLE `company_income_record` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `company_income_record` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `company_level`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL DEFAULT '',
  `level_discount` float(11,2) DEFAULT NULL,
  `level_rebate_point` float(11,2) DEFAULT '0.00',
  `level_remark` varchar(255) DEFAULT NULL,
  `level_addtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `upable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='代理等级';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_level`
--

LOCK TABLES `company_level` WRITE;
/*!40000 ALTER TABLE `company_level` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `company_level` VALUES (1,'默认分组',0.98,0.00,'','2016-07-21 18:07:57',1);
/*!40000 ALTER TABLE `company_level` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `company_rebate_rules`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_rebate_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT '0',
  `level_name` varchar(255) DEFAULT NULL,
  `rule_name` varchar(255) DEFAULT NULL COMMENT '规则名称',
  `rebate_level` tinyint(2) DEFAULT '1',
  `rebate_type` enum('amount','percent') DEFAULT 'amount' COMMENT '返佣方式, 固定金额或比例',
  `rebate_amount` float(11,2) DEFAULT NULL,
  `addtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1' COMMENT '是否启用此规则',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理返佣规则';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_rebate_rules`
--

LOCK TABLES `company_rebate_rules` WRITE;
/*!40000 ALTER TABLE `company_rebate_rules` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `company_rebate_rules` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `company_users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `comid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理和用户关联信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_users`
--

LOCK TABLES `company_users` WRITE;
/*!40000 ALTER TABLE `company_users` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `company_users` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `companys`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '代理的用户编号',
  `gid` int(11) DEFAULT '0' COMMENT '组ID',
  `parent` int(11) DEFAULT '0',
  `name` varchar(200) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `join_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `openid` varchar(255) DEFAULT NULL,
  `money` float DEFAULT '0',
  `alipay` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account` varchar(255) DEFAULT NULL,
  `bank_personname` varchar(255) DEFAULT NULL,
  `person_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `utype` tinyint(4) DEFAULT '0' COMMENT '已废弃字段',
  `verifed` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniuid` (`uid`),
  UNIQUE KEY `uniname` (`name`,`email`,`phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='代理信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companys`
--

LOCK TABLES `companys` WRITE;
/*!40000 ALTER TABLE `companys` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `companys` VALUES (1,1,1,0,'曾松林','5415@qq.com','18357082542','2016-07-22 09:43:43','oIGpIxO4u3TBR5RnABr3EpiSH2o4',0,'','','','','','',0,0,1);
/*!40000 ALTER TABLE `companys` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `envs_robblist`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `envs_robblist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `on` int(11) DEFAULT NULL,
  `remains` int(11) DEFAULT NULL,
  `envsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `envs_robblist`
--

LOCK TABLES `envs_robblist` WRITE;
/*!40000 ALTER TABLE `envs_robblist` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `envs_robblist` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `envs_robrecord`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `envs_robrecord` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `envsid` int(11) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `envs_robrecord`
--

LOCK TABLES `envs_robrecord` WRITE;
/*!40000 ALTER TABLE `envs_robrecord` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `envs_robrecord` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `express_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `express_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `confirm_time` datetime DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `costs` varchar(255) DEFAULT '0' COMMENT '配送时效',
  `openid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `express_record`
--

LOCK TABLES `express_record` WRITE;
/*!40000 ALTER TABLE `express_record` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `express_record` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `gmess_category`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gmess_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `sort` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gmess_category`
--

LOCK TABLES `gmess_category` WRITE;
/*!40000 ALTER TABLE `gmess_category` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `gmess_category` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `gmess_page`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gmess_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `desc` varchar(255) DEFAULT NULL,
  `catimg` varchar(255) DEFAULT NULL,
  `thumb_media_id` varchar(255) DEFAULT NULL,
  `content_source_url` varchar(255) DEFAULT NULL COMMENT '原文链接',
  `media_id` varchar(255) DEFAULT NULL,
  `createtime` date DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击数目',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gmess_page`
--

LOCK TABLES `gmess_page` WRITE;
/*!40000 ALTER TABLE `gmess_page` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `gmess_page` VALUES (1,'加盟味趣,大有所为','&lt;p&gt;&lt;img src=\\&quot;http://www.shop.com/uploads/default/8da6/c416a/8da614c04a2b12f16b219dae959993c0.jpg\\&quot; _src=\\&quot;http://www.shop.com/uploads/default/8da6/c416a/8da614c04a2b12f16b219dae959993c0.jpg\\&quot; style=\\&quot;\\&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=\\&quot;http://www.shop.com/uploads/default/0860/e3c06/0860c3eb4544ed13ad723a1d7e04b653.jpg\\&quot; _src=\\&quot;http://www.shop.com/uploads/default/0860/e3c06/0860c3eb4544ed13ad723a1d7e04b653.jpg\\&quot; style=\\&quot;\\&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=\\&quot;http://www.shop.com/uploads/default/9231/04f13/9231f40c176224c1cb917a77957ff560.jpg\\&quot; _src=\\&quot;http://www.shop.com/uploads/default/9231/04f13/9231f40c176224c1cb917a77957ff560.jpg\\&quot; style=\\&quot;\\&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=\\&quot;http://www.shop.com/uploads/default/f4f5/f245f/f4f542f712cda28a29392d9365bede4e.jpg\\&quot; _src=\\&quot;http://www.shop.com/uploads/default/f4f5/f245f/f4f542f712cda28a29392d9365bede4e.jpg\\&quot; style=\\&quot;\\&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;点击下载2016年7月最新内部招商计划书&lt;/p&gt;&lt;p&gt;&lt;a href=\\&quot;http://www.baidu.com\\&quot; target=\\&quot;_self\\&quot;&gt;&lt;span style=\\&quot;font-family: 微软雅黑, &amp;#39;Microsoft YaHei&amp;#39;; font-size: 24px; color: rgb(155, 187, 89);\\&quot;&gt;点击此处下载 &lt;span style=\\&quot;color: rgb(155, 187, 89); font-family: 微软雅黑, &amp;#39;Microsoft YaHei&amp;#39;; font-size: 24px;\\&quot;&gt;《&lt;/span&gt;深圳前海味趣网络科技有限公司（最新合伙人计划书-0712-F）.pptx》&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\\&quot;font-family: 微软雅黑, &amp;#39;Microsoft YaHei&amp;#39;; font-size: 14px;\\&quot;&gt;温馨提示：建议在WIFI环境下进行下载！！&lt;/span&gt;&lt;/p&gt;','','/uploads/default/9c78/1bc87/9c78cb159ca87563dd11bd0de8c5c06c.png','n_Byb8IU9yBsG2WRoNRFTmKVPIfqQNqh7nVdq8Jy3Wu-IjrppNZtYHGUfXZ9MxKU','','HGy7Ai78Xz0x_jcAE8Uwca4Ga6BPYvFe7xx3oUQouSJty0OmcjBkfl2hZ-VZRxon','2016-07-25',NULL,0,56),(2,'认识味趣','&lt;p style=\\&quot;color: rgb(0, 0, 0); white-space: normal; text-indent: 2em; line-height: 2em;\\&quot;&gt;&lt;span style=\\&quot;font-family: 微软雅黑, &amp;#39;Microsoft YaHei&amp;#39;;\\&quot;&gt;‍‍味趣网络，专注于厨房健康食材的深耕与服务，满足用户日益增长的对“味”的需求与渴望，让人们放心用“料”、安心用“料”、省心用“料”&lt;/span&gt;&lt;/p&gt;&lt;p style=\\&quot;color: rgb(0, 0, 0); white-space: normal; text-indent: 2em; line-height: 2em;\\&quot;&gt;&lt;span style=\\&quot;font-family: 微软雅黑, &amp;#39;Microsoft YaHei&amp;#39;;\\&quot;&gt;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。&lt;/span&gt;&lt;/p&gt;&lt;p style=\\&quot;color: rgb(0, 0, 0); white-space: normal; text-indent: 2em; line-height: 2em;\\&quot;&gt;&lt;span style=\\&quot;font-family: 微软雅黑, &amp;#39;Microsoft YaHei&amp;#39;;\\&quot;&gt;用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！&lt;/span&gt;&lt;/p&gt;&lt;p style=\\&quot;color: rgb(0, 0, 0); line-height: normal; white-space: normal;\\&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;','‍‍味趣网络，专注于厨房健康食材的深耕与服务，满足用户日益增长的对“味”的需求与渴望，让人们放心用“料”、安心用“料”、省心用“料”。','/uploads/images/logo-640.png','LQFgYe6nxtZg6o5MA_yG3OlvRp6pHLN24QKS8WKgEQWhhSrQ_FlICIQ-P0zWS32a','','TNFW5uOZ3uZHizQ32HVSNzbRBHmmeZ0mAdjY_TpA87VOb04TgOFQaiUSe8sRGdq0','2016-07-25',NULL,0,64);
/*!40000 ALTER TABLE `gmess_page` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `gmess_send_stat`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gmess_send_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `send_date` datetime DEFAULT NULL,
  `send_count` int(11) DEFAULT NULL,
  `read_count` int(11) DEFAULT '0',
  `share_count` int(11) DEFAULT '0',
  `receive_count` int(11) DEFAULT NULL,
  `send_type` tinyint(4) DEFAULT '0',
  `msg_type` enum('text','images') DEFAULT 'images',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gmess_send_stat`
--

LOCK TABLES `gmess_send_stat` WRITE;
/*!40000 ALTER TABLE `gmess_send_stat` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `gmess_send_stat` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `gmess_tasks`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gmess_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gmess_id` int(11) NOT NULL,
  `task_time` int(11) DEFAULT '0',
  `task_exec_time` int(11) DEFAULT '0',
  `task_finish_time` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gmess_tasks`
--

LOCK TABLES `gmess_tasks` WRITE;
/*!40000 ALTER TABLE `gmess_tasks` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `gmess_tasks` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `group_buy`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_buy` (
  `tuan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团购ID',
  `tuan_title` varchar(255) NOT NULL COMMENT '团购标题',
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `tuan_picture` varchar(100) NOT NULL COMMENT '团购图片',
  `tuan_start_time` datetime NOT NULL COMMENT '活动开始时间',
  `tuan_end_time` datetime NOT NULL COMMENT '活动结束时间',
  `tuan_deposit_price` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT '定金',
  `tuan_per_number` int(10) NOT NULL DEFAULT '0' COMMENT '每人限购数量',
  `tuan_send_point` int(11) NOT NULL DEFAULT '0' COMMENT '赠送积分数',
  `tuan_number` int(10) NOT NULL DEFAULT '0' COMMENT '限购数量',
  `tuan_pre_number` int(10) NOT NULL DEFAULT '0' COMMENT '虚拟购买数量',
  `tuan_desc` text NOT NULL COMMENT '团购介绍',
  `tuan_goodshow_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示商品详情',
  `tuan_now_number` int(10) NOT NULL DEFAULT '0' COMMENT '已团购数量',
  `tuan_order` int(10) NOT NULL DEFAULT '0' COMMENT '显示次序',
  `tuan_create_time` datetime NOT NULL COMMENT '团购创建时间',
  `tuan_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '团购更新时间',
  `tuan_price` decimal(10,3) DEFAULT '0.000' COMMENT '团购价',
  `tuan_bid` int(11) NOT NULL DEFAULT '0' COMMENT '团购所属品牌类目',
  `tuan_cid` int(11) NOT NULL DEFAULT '0' COMMENT '团购所属分类',
  `tuan_baoyou` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否包邮：0：不包邮；1:包邮',
  `tuan_remark` varchar(255) DEFAULT NULL COMMENT '团购简介',
  `tuan_start_code` tinyint(1) DEFAULT '0' COMMENT '是否启用验证码',
  `overdue_start_time` datetime NOT NULL COMMENT '补交余款开始时间',
  `overdue_end_time` datetime NOT NULL COMMENT '补交余款结束时间',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `is_deposit` tinyint(1) DEFAULT '0' COMMENT '是否启用担保金',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`tuan_id`),
  KEY `is_active` (`is_active`),
  KEY `sort_order` (`tuan_order`),
  KEY `tuan_start_time` (`tuan_start_time`),
  KEY `tuan_end_time` (`tuan_end_time`),
  KEY `product_id` (`product_id`),
  KEY `tuan_goodshow_status` (`tuan_goodshow_status`),
  KEY `overdue_start_time` (`overdue_start_time`),
  KEY `overdue_end_time` (`overdue_end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品团购表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_buy`
--

LOCK TABLES `group_buy` WRITE;
/*!40000 ALTER TABLE `group_buy` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `group_buy` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `group_buy_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_buy_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL COMMENT '订单ID',
  `tuan_id` int(10) NOT NULL DEFAULT '0' COMMENT '团购ID',
  `client_id` int(10) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `product_id` int(10) NOT NULL COMMENT '商品ID',
  `num` int(4) NOT NULL DEFAULT '0' COMMENT '购买数量。取值范围:大于零的整数',
  `remark` varchar(200) NOT NULL COMMENT '备注',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团购日志记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_buy_log`
--

LOCK TABLES `group_buy_log` WRITE;
/*!40000 ALTER TABLE `group_buy_log` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `group_buy_log` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `order_credit_available`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_credit_available` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cfrom` float(5,2) DEFAULT NULL,
  `cto` float(5,2) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_credit_available`
--

LOCK TABLES `order_credit_available` WRITE;
/*!40000 ALTER TABLE `order_credit_available` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `order_credit_available` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `order_rebates`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_rebates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `comid` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_amount` float(11,2) DEFAULT '0.00',
  `order_serial` varchar(255) DEFAULT NULL COMMENT '订单流水号',
  `order_time` datetime DEFAULT NULL COMMENT '下单时间',
  `rebate_amount` float(11,2) DEFAULT '0.00' COMMENT '返佣金额',
  `rebate_type` varchar(255) DEFAULT NULL COMMENT '返佣方式',
  `rebate_rate` float(11,2) DEFAULT '0.00' COMMENT '返佣比率',
  `rebate_level` tinyint(2) DEFAULT '0' COMMENT '返佣级别',
  `rtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('wait','pass','reject') DEFAULT 'wait',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_comid_orderid` (`order_id`,`comid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单返佣表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_rebates`
--

LOCK TABLES `order_rebates` WRITE;
/*!40000 ALTER TABLE `order_rebates` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `order_rebates` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `order_refundment`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_refundment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `refund_amount` float(10,2) DEFAULT '0.00',
  `refund_time` datetime DEFAULT NULL,
  `refund_type` tinyint(4) DEFAULT '0',
  `refund_serial` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `payment_type` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_refundment`
--

LOCK TABLES `order_refundment` WRITE;
/*!40000 ALTER TABLE `order_refundment` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `order_refundment` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `client_id` int(11) DEFAULT NULL COMMENT '客户编号',
  `serial_number` varchar(30) DEFAULT NULL,
  `order_time` datetime DEFAULT NULL COMMENT '订单交易时间',
  `receive_time` datetime DEFAULT NULL COMMENT '收货时间',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `order_balance` float(10,2) DEFAULT '0.00' COMMENT '余额抵现',
  `order_expfee` float(10,2) DEFAULT '0.00' COMMENT '订单运费',
  `order_amount` float(10,2) DEFAULT '0.00' COMMENT '总价',
  `order_refund_amount` float(10,2) DEFAULT '0.00',
  `order_discounted` float(11,2) DEFAULT '1.00' COMMENT '订单折扣比例',
  `supply_price_amount` float(10,2) DEFAULT '0.00',
  `original_amount` float(10,2) DEFAULT '0.00',
  `company_id` int(11) DEFAULT '0',
  `product_count` int(11) DEFAULT '0',
  `wepay_serial` varchar(50) DEFAULT NULL,
  `wepay_openid` varchar(255) DEFAULT '',
  `wepay_unionid` varchar(255) DEFAULT NULL,
  `wepayed` tinyint(1) DEFAULT '0' COMMENT '订单是否已支付',
  `leword` text,
  `status` enum('unpay','payed','received','canceled','closed','refunded','delivering','reqing') NOT NULL DEFAULT 'unpay' COMMENT '订单状态',
  `express_openid` varchar(255) DEFAULT NULL,
  `express_code` varchar(255) DEFAULT NULL,
  `express_com` varchar(255) DEFAULT NULL,
  `exptime` varchar(255) DEFAULT NULL,
  `envs_id` int(11) DEFAULT '0',
  `is_commented` tinyint(1) DEFAULT '0',
  `address_hash` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT '0',
  `rebated` tinyint(1) DEFAULT '0' COMMENT '返佣是否已经处理',
  `rebated_amount` float(11,2) DEFAULT '0.00' COMMENT '已经返佣的金额',
  PRIMARY KEY (`order_id`),
  KEY `openid` (`wepay_openid`) USING BTREE,
  KEY `serial_number` (`serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `orders_address`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(255) NOT NULL,
  `tel_number` varchar(255) NOT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_address`
--

LOCK TABLES `orders_address` WRITE;
/*!40000 ALTER TABLE `orders_address` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `orders_address` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `orders_comment`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `starts` tinyint(4) DEFAULT NULL,
  `content` text,
  `mtime` datetime DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `anonymous` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_comment`
--

LOCK TABLES `orders_comment` WRITE;
/*!40000 ALTER TABLE `orders_comment` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `orders_comment` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `orders_detail`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(20) NOT NULL COMMENT '订单编号',
  `product_id` int(20) NOT NULL COMMENT '商品编号',
  `product_count` int(10) NOT NULL COMMENT '商品数量',
  `product_discount_price` float(11,2) NOT NULL DEFAULT '0.00',
  `original_amount` float(11,2) DEFAULT NULL,
  `product_price_hash_id` int(11) NOT NULL DEFAULT '0',
  `refunded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_detail`
--

LOCK TABLES `orders_detail` WRITE;
/*!40000 ALTER TABLE `orders_detail` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `orders_detail` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_brand`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_img1` varchar(255) DEFAULT NULL,
  `brand_img2` varchar(255) DEFAULT NULL,
  `brand_cat` int(11) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`brand_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_brand`
--

LOCK TABLES `product_brand` WRITE;
/*!40000 ALTER TABLE `product_brand` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `product_brand` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_category`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  `cat_descs` text,
  `cat_image` varchar(255) NOT NULL DEFAULT '',
  `cat_parent` int(11) NOT NULL DEFAULT '0',
  `cat_level` int(11) DEFAULT '0',
  `cat_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `product_category` VALUES (1,'高端酱油',NULL,'/uploads/product_cate/jiangyou.jpg',0,0,0,1),(2,'陈醋白醋',NULL,'/uploads/product_cate/cu.jpg',0,0,0,1),(3,'调味用料',NULL,'/uploads/product_cate/tiaoweiliao.jpg',0,0,0,1),(4,'色油色粉',NULL,'/uploads/product_cate/28.jpg',0,0,0,1),(5,'调味用料',NULL,'/uploads/product_cate/23.jpg',0,0,0,0),(6,'鸡精鸡粉',NULL,'/uploads/product_cate/15.jpg',0,0,0,1),(7,'面包糠',NULL,'/uploads/product_cate/72.jpg',0,0,0,1),(8,'调味香汁',NULL,'/uploads/product_cate/25.jpg',0,0,0,1),(9,'独家特供',NULL,'/uploads/product_cate/wayqu.jpg',0,0,0,1);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_credit_exanges`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_credit_exanges` (
  `product_id` int(11) NOT NULL,
  `product_credits` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_credit_exanges`
--

LOCK TABLES `product_credit_exanges` WRITE;
/*!40000 ALTER TABLE `product_credit_exanges` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `product_credit_exanges` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(512) NOT NULL,
  `image_sort` tinyint(4) DEFAULT '0',
  `image_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `index_product` (`product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `product_images` VALUES (1,1,'uploads/alibaba/9397/9397d7386575a089ba5a8d4f03524660.jpg',0,0),(1,2,'uploads/alibaba/f716/f716e814c63c39d54374ae7f8245c87a.jpg',0,0),(1,3,'uploads/alibaba/c11a/c11aa30ee8284c94dd73e2ba9c34d783.jpg',0,0),(1,4,'uploads/alibaba/f47d/f47d842a207ad82b82711ddbb0ff5d6c.jpg',0,0),(2,5,'uploads/alibaba/170f/170fe89b3e8728707197638d1bbd4f94.jpg',0,0),(2,6,'uploads/alibaba/0c12/0c12c69c16276de7a4bf33a3e1d15a22.jpg',0,0),(2,7,'uploads/alibaba/f54a/f54ab47e18247a09098eec27baafd9a9.jpg',0,0),(2,8,'uploads/alibaba/5577/5577193a6d64b698c588d083d5de52ca.jpg',0,0),(2,9,'uploads/alibaba/eac0/eac040b2cc483edf5754cc02f05b660e.jpg',0,0),(2,10,'uploads/alibaba/f4c7/f4c7344735d40163f658f61033a47b47.jpg',0,0),(2,11,'uploads/alibaba/f4c7/f4c7344735d40163f658f61033a47b47.jpg',0,0),(3,12,'uploads/alibaba/299a/299a1a636bdf1bfa619667e6236af0b4.jpg',0,0),(3,13,'uploads/alibaba/a08c/a08c4546756eb620d169d0129be1330c.jpg',0,0),(3,14,'uploads/alibaba/7e3a/7e3a1d450de6a2f687eb435f34745a9d.jpg',0,0),(3,15,'uploads/alibaba/1185/1185ee081257ed03a8d84bc519a9780b.jpg',0,0),(3,16,'uploads/alibaba/4c30/4c3062c38f719cf01fd21e741725211d.jpg',0,0),(4,17,'uploads/alibaba/76f3/76f3b928e31f344b9c131912eeb9dc6b.jpg',0,0),(4,18,'uploads/alibaba/4952/495245acb0c378d04965f2ebfcd5c188.jpg',0,0),(4,19,'uploads/alibaba/81bd/81bd671cff6e61490b1617999df83bdf.jpg',0,0),(4,20,'uploads/alibaba/c031/c031e5cb8813d7a8726b42edd6b1a110.jpg',0,0),(4,21,'uploads/alibaba/eb1e/eb1e230c6c2cbc97afe365fb48e974bb.jpg',0,0),(4,22,'uploads/alibaba/eb1e/eb1e230c6c2cbc97afe365fb48e974bb.jpg',0,0),(5,23,'uploads/alibaba/bfeb/bfeba6516c6e36234d8a5315fdb1cf88.jpg',0,0),(5,24,'uploads/alibaba/09b3/09b341f91e3df71a77c8af21e71f9d2a.jpg',0,0),(5,25,'uploads/alibaba/5e6b/5e6baf747a172fa5820808d50ec5adfa.jpg',0,0),(5,26,'uploads/alibaba/9360/93600d3ddf264e508a47d4cdf6a685a7.jpg',0,0),(5,27,'uploads/alibaba/1390/13903823314e1848c522609db3f94671.jpg',0,0),(6,28,'uploads/alibaba/da52/da526797f1b1b9362c707739374c3866.jpg',0,0),(6,29,'uploads/alibaba/d15d/d15d510bff8c41f6550d21f75f3060b6.jpg',0,0),(6,30,'uploads/alibaba/b96a/b96a03a2828f907b6ce81b3baa6e9465.jpg',0,0),(6,31,'uploads/alibaba/4487/4487cb9e643be57b96c8487890679145.jpg',0,0),(7,32,'uploads/alibaba/7a68/7a68bf46f35e9bf1a74c59bdf8b1ad93.jpg',0,0),(7,33,'uploads/alibaba/1952/1952bd6f59b0dc05a372afc4d58d9708.jpg',0,0),(7,34,'uploads/alibaba/83bc/83bc51c7b31ec675b88c5476d1eb65c3.jpg',0,0),(7,35,'uploads/alibaba/e568/e568be069fce4e7613489ddb2d93ad15.jpg',0,0),(8,36,'uploads/alibaba/1d2c/1d2cb3c64b24c03ff17a05918e7a80c4.jpg',0,0),(8,37,'uploads/alibaba/74dc/74dc6cd28fa481146baa2b7114aa295b.jpg',0,0),(8,38,'uploads/alibaba/a7a2/a7a25f83998676f7673d514fed8face6.jpg',0,0),(8,39,'uploads/alibaba/1870/1870f3ccabff33b75a3bfbc9466f8cea.jpg',0,0),(9,40,'uploads/alibaba/1fae/1faebb6d0d373422ae9776af1807ab54.jpg',0,0),(9,41,'uploads/alibaba/a321/a321ef9cc82e7e4319cc9c96cb3a4a27.jpg',0,0),(9,42,'uploads/alibaba/e48e/e48ef3ce126abfb1b8ddf79df0124727.jpg',0,0),(9,43,'uploads/alibaba/523a/523a6b84204dfd5cd115f9c4e3cf592d.jpg',0,0),(10,44,'uploads/alibaba/b38c/b38ca25387a4afb71fd75ea17d172239.jpg',0,0),(10,45,'uploads/alibaba/05a8/05a82b6ab5d55e626670a113c45d7ede.jpg',0,0),(10,46,'uploads/alibaba/0560/0560b25a4b490c29bdda37758b668e26.jpg',0,0),(10,47,'uploads/alibaba/0594/0594401bd4d70d7a045be927a0c7b68e.jpg',0,0),(10,48,'uploads/alibaba/704c/704c21870385d6b158ee816efb008a02.jpg',0,0),(11,49,'uploads/alibaba/c13e/c13ecbd268cdc424356f0e65edc24728.jpg',0,0),(11,50,'uploads/alibaba/58b7/58b779b4949c3ba6e664334449661ae7.jpg',0,0),(11,51,'uploads/alibaba/6604/6604434b6cc6464df532ea3d250a15a6.jpg',0,0),(12,52,'uploads/alibaba/66b1/66b16bf31bf2b951dda0c9c6c14fa2ea.jpg',0,0),(12,53,'uploads/alibaba/1a00/1a00d8dfcf33a0b92f17aee50af0387e.jpg',0,0),(12,54,'uploads/alibaba/7023/7023aa1ee11800dbef0dee989fcad546.jpg',0,0),(13,55,'uploads/alibaba/a0ae/a0ae6c44cc0862f3f58c729b48b3fb78.jpg',0,0),(13,56,'uploads/alibaba/5258/525851ca855838ef2dbadee39d8e5004.jpg',0,0),(14,57,'uploads/alibaba/4cd6/4cd62f381420f28cc35c28494b48fb5c.jpg',0,0),(14,58,'uploads/alibaba/3101/3101995257084491b3ae9ef84045a46a.jpg',0,0),(14,59,'uploads/alibaba/56a1/56a177760dc15beaeecc2690721d3318.jpg',0,0),(14,60,'uploads/alibaba/b7fb/b7fb3e548ad6113c12377c1175bbfd82.jpg',0,0),(15,61,'uploads/alibaba/0da5/0da58a767c3447895bda1e51a87fdaec.jpg',0,0),(15,62,'uploads/alibaba/cb1c/cb1c475ccecf0c7894b328abcf2238e4.jpg',0,0),(15,63,'uploads/alibaba/e448/e448526d86dbba8b2cacdce72803d470.jpg',0,0),(15,64,'uploads/alibaba/06ef/06efefaad22e923b9a74758225a7278f.jpg',0,0),(16,65,'uploads/alibaba/27d3/27d357b75d0dc72f06184695bf024f1f.jpg',0,0),(16,66,'uploads/alibaba/d497/d497df3a39fb97615a0406a045da5d44.jpg',0,0),(16,67,'uploads/alibaba/103f/103f336403af01238f2f77069ea2c580.jpg',0,0),(17,68,'uploads/alibaba/6cbe/6cbeb9fff3f82a81d6298542a0163bf8.jpg',0,0),(17,69,'uploads/alibaba/7d07/7d076581fa56d4b862f1534c7a3841f5.jpg',0,0),(17,70,'uploads/alibaba/31ff/31ffc8adc89af3aae2fb16dcd4f1eca9.jpg',0,0),(17,71,'uploads/alibaba/a754/a75415f80766dd5139fb1565f75ef0fb.jpg',0,0),(17,72,'uploads/alibaba/9cf6/9cf6205536d1e31a5c03bb84519b47df.jpg',0,0),(18,73,'uploads/alibaba/32a3/32a3ec45327839f7b12b1e5ac4e494ed.jpg',0,0),(18,74,'uploads/alibaba/1d29/1d296a82b88e029402396e842f781697.jpg',0,0),(18,75,'uploads/alibaba/0242/02426d6481f299aa39ae8f6c0531ad6a.jpg',0,0),(18,76,'uploads/alibaba/213c/213c75bfd7135fe014c7192200ad99e5.jpg',0,0),(19,77,'uploads/alibaba/efeb/efeb2f223778df6761ef74e2aebc7c35.jpg',0,0),(19,78,'uploads/alibaba/8012/80129ccb7bae823a4bf539948d9dbfbb.jpg',0,0),(19,79,'uploads/alibaba/1558/1558bf0a9664baa2495cf1a2ec2dee67.jpg',0,0),(19,80,'uploads/alibaba/9bcc/9bcce00925ca2728956c4a3648c21f04.jpg',0,0),(20,81,'uploads/alibaba/ef19/ef193aa9d98e855ee1fb903ee9a151c7.jpg',0,0),(20,82,'uploads/alibaba/dd04/dd04ee2f3d9c75e4323e0dce255ebe3f.jpg',0,0),(21,83,'uploads/alibaba/a543/a543233ccaf67d47259caecd9394d72d.jpg',0,0),(21,84,'uploads/alibaba/4627/4627b8b2ce2a21170deaf93933c43830.jpg',0,0),(21,85,'uploads/alibaba/f205/f20511222d7554e0abf145e130484b6b.jpg',0,0),(21,86,'uploads/alibaba/eff4/eff4d8e9e079ac79ebffb308b6eed079.jpg',0,0),(22,87,'uploads/alibaba/f785/f78593279a6b4a71ef20445589657ec7.jpg',0,0),(22,88,'uploads/alibaba/2112/2112dc165301ab86cbc936bf003ed2b2.jpg',0,0),(22,89,'uploads/alibaba/f376/f376d23683c2b711763c05e1182fbe0f.jpg',0,0),(23,90,'uploads/alibaba/0c5a/0c5a6003e40f641ddd70e4b2a042a86e.jpg',0,0),(23,91,'uploads/alibaba/ea99/ea995560d08888b55bc9c8c737eac991.jpg',0,0),(23,92,'uploads/alibaba/1ca6/1ca693aae03b0138ea93c538fe4f2414.jpg',0,0),(23,93,'uploads/alibaba/732c/732c6563e2d496b6d19ed89ba1512341.jpg',0,0),(24,94,'uploads/alibaba/e31e/e31e68d539cfad41faabd302a1b5fbc9.jpg',0,0),(24,95,'uploads/alibaba/db03/db03d32e63136677f9999e3b8c39fed4.jpg',0,0),(24,96,'uploads/alibaba/c193/c193a326b610f19434ac696241b0f95a.jpg',0,0),(24,97,'uploads/alibaba/ce70/ce70027c345fd19a9c076f89dba7fc55.jpg',0,0),(25,98,'uploads/alibaba/cdbf/cdbfe1cc8f8cae0cb2c89343a18a9e3f.jpg',0,0),(25,99,'uploads/alibaba/367b/367bd4b0341ddf6c129144ab321dd170.jpg',0,0),(25,100,'uploads/alibaba/a266/a2669a0e23b0b8a12da1ea91d9af85ec.jpg',0,0),(26,101,'uploads/alibaba/fb62/fb62378f4e8e1407d802b520d53e16f0.jpg',0,0),(26,102,'uploads/alibaba/7113/71138b381fab702aab2a49792aede0dd.jpg',0,0),(27,103,'uploads/alibaba/f778/f778c8f3b074114b33efa80355389f13.jpg',0,0),(27,104,'uploads/alibaba/4ae2/4ae284783d335d053cbe080959b96d3d.jpg',0,0),(27,105,'uploads/alibaba/f1f5/f1f55c2b97ddd6f90fc0ffa2068793fb.jpg',0,0),(27,106,'uploads/alibaba/7833/78332975d3a081f87e586fcded61dc8d.jpg',0,0),(28,107,'uploads/alibaba/3c51/3c51323151d854384b26df4d31e0a3f0.jpg',0,0),(28,108,'uploads/alibaba/b2ca/b2caebc8a8a86824fa1b1a58ffcfb6b5.jpg',0,0),(28,109,'uploads/alibaba/64af/64af300b2b422dcad1e1ac160e4da574.jpg',0,0),(29,110,'uploads/alibaba/6783/67837543479d1484d9d45de72f9be9a1.jpg',0,0),(29,111,'uploads/alibaba/efbc/efbc67f01cddd31319a4be7e54706515.jpg',0,0),(29,112,'uploads/alibaba/082c/082c1160559c3c9ab47ecec4121b21b9.jpg',0,0),(29,113,'uploads/alibaba/733b/733bc3f3dedfe918de599da3375d1b0e.jpg',0,0),(30,114,'uploads/alibaba/4ddb/4ddb07db243ef0f4a295be4ed3bde420.jpg',0,0),(30,115,'uploads/alibaba/fcda/fcdaf6925b2ffa80f8ee4c0523f40a45.jpg',0,0),(30,116,'uploads/alibaba/d197/d1973b7f5d6656bbf42c380fdd50f047.jpg',0,0),(30,117,'uploads/alibaba/8bbc/8bbc8ba6af709f7109f42f27ff2ca0da.jpg',0,0),(31,118,'uploads/alibaba/7bdc/7bdc8f17bdc5ddaae6eea1a0373d3e81.jpg',0,0),(31,119,'uploads/alibaba/0c78/0c7853ee00aa21f0e510001b5a13a4a2.jpg',0,0),(31,120,'uploads/alibaba/cb37/cb37b844780b047836463af7e1e499c6.jpg',0,0),(31,121,'uploads/alibaba/d7da/d7dadb0e5cb8f33debb2ad392a1f2f87.jpg',0,0),(32,122,'uploads/alibaba/1254/1254d18589adc4fb986223fc03f1794b.jpg',0,0),(32,123,'uploads/alibaba/4d3b/4d3b8c5345efce5b37fc6d5bbf4b6896.jpg',0,0),(32,124,'uploads/alibaba/42e7/42e778c27a67bff0068c412094fc295a.jpg',0,0),(32,125,'uploads/alibaba/014f/014f0262da625b1e2016ee01e95d6b6b.jpg',0,0),(33,126,'uploads/alibaba/a65a/a65a8b916124a6cd3487ec2e1314c30c.jpg',0,0),(33,127,'uploads/alibaba/85b7/85b7871dae2faf52a0b6b5e94fd8cee6.jpg',0,0),(33,128,'uploads/alibaba/111c/111cc78b7e5df82b2e32df2626ec3b1b.jpg',0,0),(33,129,'uploads/alibaba/ac26/ac266a072bafe67b094aa8c9edde3695.jpg',0,0),(34,130,'uploads/alibaba/9edf/9edffbb16c3818ff87f7834aeecd6494.jpg',0,0),(34,131,'uploads/alibaba/20c6/20c6b72b34f0d680ac807e1966806f8d.jpg',0,0),(34,132,'uploads/alibaba/01ed/01ed34badcd64086497a67e1ad76e833.jpg',0,0),(34,133,'uploads/alibaba/61fc/61fcf00490fe91effe110252a238b0b0.jpg',0,0),(35,134,'uploads/alibaba/99c5/99c5fdcac653f952c7b7ef1fe5da5658.jpg',0,0),(35,135,'uploads/alibaba/413e/413e8265b51cd6150cdc5c27f6aa1fa9.jpg',0,0),(35,136,'uploads/alibaba/624b/624b1d9497b07c8ca07900bda63b576b.jpg',0,0),(35,137,'uploads/alibaba/8825/88258d415011aedb46415db930dc645c.jpg',0,0),(35,138,'uploads/alibaba/9270/927014576357cb81907a2a69425c9893.jpg',0,0),(36,139,'uploads/alibaba/891d/891dcba475d87abed506129f47a16608.jpg',0,0),(36,140,'uploads/alibaba/e840/e8408b0060522db432892995ccca691a.jpg',0,0),(36,141,'uploads/alibaba/a206/a206591fbab6f6834c76e8e2bed9991b.jpg',0,0),(36,142,'uploads/alibaba/1a7b/1a7b8f2b227dd4465a36c8989ee0902b.jpg',0,0),(37,143,'uploads/alibaba/a723/a7230388d52f35fbad64fdc9026bd34d.jpg',0,0),(37,144,'uploads/alibaba/9c56/9c56b568b36012ad14e77bbb8c109035.jpg',0,0),(37,145,'uploads/alibaba/59d3/59d31f01d515d906eca4e3261b38b495.jpg',0,0),(37,146,'uploads/alibaba/79b5/79b505165a942a714e53b00c572601b7.jpg',0,0),(37,147,'uploads/alibaba/0218/02184e5feadd3aaa7f5a9015ee06af6d.jpg',0,0),(37,148,'uploads/alibaba/0218/02184e5feadd3aaa7f5a9015ee06af6d.jpg',0,0),(38,149,'uploads/alibaba/964f/964f42d5f004a4be7f24d5e592f377f7.jpg',0,0),(38,150,'uploads/alibaba/2000/20009bb787fa57486a10c20ae92c8472.jpg',0,0),(38,151,'uploads/alibaba/c711/c711c487cb881635e5b6c3c151cc9b0b.jpg',0,0),(38,152,'uploads/alibaba/3d1e/3d1e6add578855b84314e0812327a48c.jpg',0,0),(39,153,'uploads/alibaba/5597/5597e021bac184a2d2c0f20691b35924.jpg',0,0),(39,154,'uploads/alibaba/ea43/ea430bb0c7b82f65d92082a8616fe219.jpg',0,0),(39,155,'uploads/alibaba/8de4/8de42a21a118b6e773e02c221b115f12.jpg',0,0),(39,156,'uploads/alibaba/2090/20905249a5b769eaa7d5d16719475715.jpg',0,0),(40,157,'uploads/alibaba/d3f9/d3f9986616bdbc628ff0c3c668419060.jpg',0,0),(40,158,'uploads/alibaba/42f8/42f866859217268ab7369e21bcdf3e06.jpg',0,0),(40,159,'uploads/alibaba/367f/367f8c8c6bf9b2dd661974197a800a3f.jpg',0,0),(40,160,'uploads/alibaba/50d1/50d1567e7cb339de7e100a444ed593f5.jpg',0,0),(41,161,'uploads/alibaba/a1ab/a1ab8be048d3a3a07b5530de860e1770.jpg',0,0),(41,162,'uploads/alibaba/0db5/0db552662ed1173010ee21efea684df0.jpg',0,0),(41,163,'uploads/alibaba/bb06/bb067edfe97aeb3c77fb987b47288bc1.jpg',0,0),(41,164,'uploads/alibaba/da43/da4377ff58e55127c61de33371b35e0b.jpg',0,0),(42,165,'uploads/alibaba/758e/758eee92dc174602ad5948100e457838.jpg',0,0),(42,166,'uploads/alibaba/98c3/98c31a2be942578ec4946cc3f1490087.jpg',0,0),(42,167,'uploads/alibaba/6846/6846e0eed1772c9ee1254ec11aa87ea5.jpg',0,0),(42,168,'uploads/alibaba/d3b9/d3b9e479a7b3b0294c9b118310c0abf1.jpg',0,0),(43,169,'uploads/alibaba/a71d/a71dd7315f9d85c9bac1388f5946e395.jpg',0,0),(43,170,'uploads/alibaba/bfae/bfaee91f656f06db94d8df3cf88414aa.jpg',0,0),(43,171,'uploads/alibaba/e341/e341b3283b0d1921ae755b73468efbfe.jpg',0,0),(43,172,'uploads/alibaba/3489/34890c05b45648ff63910d8b10ea775a.jpg',0,0),(44,173,'uploads/alibaba/8ad5/8ad56dd1e35a0b5468f05aebda207739.jpg',0,0),(44,174,'uploads/alibaba/7440/7440eb922eb89c6505f33a36b56b71bd.jpg',0,0),(44,175,'uploads/alibaba/7c9c/7c9c90a39416e7a121d863919e382dd5.jpg',0,0),(45,176,'uploads/alibaba/1599/1599221ab40afdcfe4cae1ea7e6742c0.jpg',0,0),(45,177,'uploads/alibaba/114a/114af847d251a056ea27e67deefd2155.jpg',0,0),(45,178,'uploads/alibaba/f523/f5237595cd7c520165da3876f4bbb655.jpg',0,0),(45,179,'uploads/alibaba/53e5/53e538d71542707e479000e826c08449.jpg',0,0),(46,180,'uploads/alibaba/edc7/edc76ed8b3cb839fcfc677817d80e100.jpg',0,0),(46,181,'uploads/alibaba/9687/9687e1e003d9156251f11baada620322.jpg',0,0),(46,182,'uploads/alibaba/25b8/25b86e4f36196e4ec37a178fc4103d00.jpg',0,0),(46,183,'uploads/alibaba/7f20/7f204ba6b52bd6770b92af5c1a2bd239.jpg',0,0),(47,184,'uploads/alibaba/7cea/7cea9dbfc5adbfa4de900156effd11ac.jpg',0,0),(47,185,'uploads/alibaba/21f1/21f118a3832c99f44e1c6e00f4c17b2e.jpg',0,0),(48,186,'uploads/alibaba/7c69/7c69a1aaeda7c0c058819895c93309f4.jpg',0,0),(48,187,'uploads/alibaba/6e13/6e132f62d2427ef972cc73e6f77c7eb1.jpg',0,0),(48,188,'uploads/alibaba/a47b/a47b8b2ee4fd24e65c9f17591707dca1.jpg',0,0),(48,189,'uploads/alibaba/84b3/84b3c6248a0517fa7957900c66b571a3.jpg',0,0),(49,190,'uploads/alibaba/c385/c385fb40551664fb854a307e10ccefaf.jpg',0,0),(49,191,'uploads/alibaba/b94d/b94d98b8db1b2dd117b080e2104d5514.jpg',0,0),(49,192,'uploads/alibaba/4413/44134b143ef80270deb1b54724c01650.jpg',0,0),(49,193,'uploads/alibaba/5f43/5f43eee6e8a0cf4340535be972dc9db3.jpg',0,0),(49,194,'uploads/alibaba/b749/b74961d95879c6f0163bfe01aa2497f8.jpg',0,0),(50,195,'uploads/alibaba/ac10/ac10363305eb31b2affaa14e9eb7bfee.jpg',0,0),(50,196,'uploads/alibaba/3229/322946b51b26e9b2cddcb14a1cc74d0f.jpg',0,0),(50,197,'uploads/alibaba/443b/443b5b85ca432bfb7e4f0869d7504386.jpg',0,0),(50,198,'uploads/alibaba/139b/139b5d3f3f7b9f1d71de4e21418e5ccf.jpg',0,0),(50,199,'uploads/alibaba/d2d4/d2d4cf9262cc7ca24658368ca7eff3bf.jpg',0,0),(51,200,'uploads/alibaba/95d5/95d59f9f061b4f0db78c1b07aada3306.jpg',0,0),(51,201,'uploads/alibaba/e00c/e00ce7e2455c78d7ad3a52f556e796e7.jpg',0,0),(51,202,'uploads/alibaba/250d/250d12d40a2f2fc0900f6500910fa149.jpg',0,0),(51,203,'uploads/alibaba/531b/531bfccb29b47816b48491732b5ad50e.jpg',0,0),(52,204,'uploads/alibaba/a6f4/a6f44acc074b8a58d418d53c4d19e6e6.jpg',0,0),(53,205,'uploads/alibaba/a46c/a46c898531a8f59287b0371814c5cacf.jpg',0,0),(53,206,'uploads/alibaba/645f/645fedb00decae3ea6c9cacd813cdcea.jpg',0,0),(53,207,'uploads/alibaba/512e/512e729ee358df0f35137d5a28e65a60.jpg',0,0),(53,208,'uploads/alibaba/b357/b357b6ecd1184a39967b317a0e981307.jpg',0,0),(54,209,'uploads/alibaba/1aa5/1aa56216520eac599daf21a275d79944.jpg',0,0),(54,210,'uploads/alibaba/08fd/08fdb9e02f0327c464bff9023895e3c8.jpg',0,0),(54,211,'uploads/alibaba/4186/4186b6049757f5f756bf4f6386a6119c.jpg',0,0),(54,212,'uploads/alibaba/6b7d/6b7d2ec6a96f82699d3574b6fa3cca39.jpg',0,0),(55,213,'uploads/alibaba/d6b5/d6b5173424b0ebcef9c05cf2c10082e2.jpg',0,0),(55,214,'uploads/alibaba/535e/535e0b2abfe49be83520fc06ba790376.jpg',0,0),(55,215,'uploads/alibaba/3ade/3ade113d9d1412dd9edfb52c6b610e65.jpg',0,0),(55,216,'uploads/alibaba/94fd/94fd05318bf8f2919a064450992a28b2.jpg',0,0),(56,217,'uploads/alibaba/9c51/9c5141f97e09732042b0ea14cab5a94d.jpg',0,0),(56,218,'uploads/alibaba/d878/d87813081c8ee550666fd9a59578dd87.jpg',0,0),(56,219,'uploads/alibaba/be1b/be1bf1a696735fbb13b804b4285e91d6.jpg',0,0),(56,220,'uploads/alibaba/db60/db60fa831f6780b471abed7d838446a0.jpg',0,0),(56,221,'uploads/alibaba/39e9/39e948731d3ff27ed1494fb9f225c5c9.jpg',0,0),(57,222,'uploads/alibaba/2f76/2f76877c9079d7189eec40e7532c2b9d.jpg',0,0),(57,223,'uploads/alibaba/bff5/bff5dd007dd528e0aa0632b6bd647705.jpg',0,0),(57,224,'uploads/alibaba/99a4/99a408809d0ee1562b7f727d5bcf6772.jpg',0,0),(57,225,'uploads/alibaba/ea1b/ea1b9d4aa4b639bde6785f5da7bbdda8.jpg',0,0),(57,226,'uploads/alibaba/fd68/fd689f6376f0c5d46b2b8b2bd2aa02c4.jpg',0,0),(58,227,'uploads/alibaba/ab0d/ab0deb30f9033fded4a389d58219c52e.jpg',0,0),(58,228,'uploads/alibaba/e78e/e78e92d13bee73138c5c97f73856af46.jpg',0,0),(58,229,'uploads/alibaba/846d/846d869ba504dffe85817c27af4959da.jpg',0,0),(59,230,'uploads/alibaba/45ad/45addad819c514dd5e4679ce1267729d.jpg',0,0),(59,231,'uploads/alibaba/1254/1254abb1c466a425cb290be26454712c.jpg',0,0),(59,232,'uploads/alibaba/701d/701dc7d6a01bd9a4de97ee1913b17b24.jpg',0,0),(59,233,'uploads/alibaba/0914/0914b90fe52f889bfa45290ef2d2438f.jpg',0,0),(61,239,'uploads/alibaba/c12c/c12c50a4e41e4a6ae16a3ad004ee5020.jpg',0,0),(61,240,'uploads/alibaba/1ebb/1ebbd1647dcfb706e8b033a1f8d6b408.jpg',0,0),(61,241,'uploads/alibaba/f701/f7016ceb374e1bb2cdfb5c6a10bb8b00.jpg',0,0),(62,242,'uploads/alibaba/fd4b/fd4bb5417692ba197924985cffa32a1a.jpg',0,0),(62,243,'uploads/alibaba/012a/012a9fd3ed834aba2d02dca507bc113d.jpg',0,0),(62,244,'uploads/alibaba/eb5a/eb5ab77504b52f55a52758c771a48634.jpg',0,0),(62,245,'uploads/alibaba/24dd/24dda1e2427026729cbd0102e4732786.jpg',0,0),(63,246,'uploads/alibaba/be4f/be4fe73e5846c9ce73169ba864922507.jpg',0,0),(63,247,'uploads/alibaba/b3b7/b3b738fee925fe4c0799127bada22c13.jpg',0,0),(63,248,'uploads/alibaba/9543/954361986b601d9b309ed7aed657dba1.jpg',0,0),(63,249,'uploads/alibaba/8ca6/8ca61e8ccf67d90aad0235102fb986a0.jpg',0,0),(65,254,'uploads/alibaba/0e0a/0e0a2e26c9867ce426d3577d77ffea27.jpg',0,0),(65,255,'uploads/alibaba/8481/8481f81f1d9f5e9a9fa5065a8b6b376c.jpg',0,0),(65,256,'uploads/alibaba/380b/380b4851c76cfdc3f03033fe57bb44ca.jpg',0,0),(65,257,'uploads/alibaba/401d/401ddb10429a4f52c8b6b5cfedf9d1da.jpg',0,0),(64,258,'uploads/alibaba/6cc4/6cc4f6293a40d3cc1574ab6fc0d5c109.jpg',0,0),(64,259,'',1,0),(64,260,'',2,0),(64,261,'',3,0),(64,262,'',4,0),(60,292,'uploads/alibaba/3a8b/3a8b5e155f8df44803e69c8267a1bcbb.jpg',0,0),(60,293,'',1,0),(60,294,'',2,0),(60,295,'',3,0),(60,296,'',4,0),(66,322,'uploads/alibaba/99fe/99fe8c8a98cf56fef436fd455a7d8651.jpg',0,0),(66,323,'',1,0),(66,324,'',2,0),(66,325,'',3,0),(66,326,'',4,0);
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_onsale`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_onsale` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `sale_prices` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
  `store_id` int(8) NOT NULL DEFAULT '0' COMMENT '商店编号',
  `discount` int(3) NOT NULL DEFAULT '100' COMMENT '折扣',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_onsale`
--

LOCK TABLES `product_onsale` WRITE;
/*!40000 ALTER TABLE `product_onsale` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `product_onsale` VALUES (1,20.00,0,100),(2,8.00,0,100),(3,2.50,0,100),(4,2.50,0,100),(5,10.50,0,100),(6,15.00,0,100),(7,13.00,0,100),(8,8.50,0,100),(9,35.00,0,100),(10,6.00,0,100),(11,18.00,0,100),(12,15.00,0,100),(14,10.00,0,100),(16,11.50,0,100),(17,67.00,0,100),(18,21.00,0,100),(19,54.00,0,100),(20,10.00,0,100),(21,18.00,0,100),(22,30.00,0,100),(23,25.00,0,100),(25,8.00,0,100),(28,96.00,0,100),(31,13.00,0,100),(32,50.00,0,100),(33,38.00,0,100),(35,15.00,0,100),(37,3.00,0,100),(39,16.00,0,100),(41,10.00,0,100),(42,12.00,0,100),(43,38.00,0,100),(44,42.00,0,100),(45,12.00,0,100),(47,15.00,0,100),(48,13.00,0,100),(49,38.00,0,100),(50,38.00,0,100),(51,13.00,0,100),(52,10.00,0,100),(53,13.00,0,100),(54,12.00,0,100),(55,26.00,0,100),(56,17.00,0,100),(57,6.00,0,100),(58,38.00,0,100),(59,12.00,0,100),(60,0.00,0,100),(61,128.00,0,100),(62,35.00,0,100),(64,168.00,0,100),(65,58.00,0,100),(66,145.00,0,1);
/*!40000 ALTER TABLE `product_onsale` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_serials`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_serials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_name` varchar(255) DEFAULT NULL COMMENT '序列名称',
  `serial_image` varchar(255) DEFAULT NULL,
  `serial_desc` varchar(255) DEFAULT NULL,
  `relcat` tinyint(4) DEFAULT NULL,
  `relevel` tinyint(4) DEFAULT NULL,
  `sort` varchar(255) DEFAULT '0' COMMENT '排序',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_serials`
--

LOCK TABLES `product_serials` WRITE;
/*!40000 ALTER TABLE `product_serials` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `product_serials` VALUES (1,'默认','','',NULL,NULL,'0',0);
/*!40000 ALTER TABLE `product_serials` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `product_spec`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `spec_det_id1` int(11) DEFAULT NULL,
  `spec_det_id2` int(11) DEFAULT NULL,
  `sale_price` float(11,2) DEFAULT NULL,
  `market_price` float(11,2) DEFAULT '0.00',
  `instock` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品规格';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_spec`
--

LOCK TABLES `product_spec` WRITE;
/*!40000 ALTER TABLE `product_spec` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `product_spec` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `products_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_info` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `product_code` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '商品条码',
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '商品名称',
  `product_subname` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品颜色',
  `product_size` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品大小',
  `product_cat` int(11) NOT NULL DEFAULT '1',
  `product_brand` int(11) DEFAULT '0',
  `product_readi` int(11) NOT NULL DEFAULT '0',
  `product_desc` longtext CHARACTER SET utf8,
  `product_subtitle` text CHARACTER SET utf8,
  `product_serial` int(11) DEFAULT '0',
  `product_weight` varchar(11) CHARACTER SET utf8 DEFAULT '0.00',
  `product_online` tinyint(4) DEFAULT '1',
  `product_credit` int(11) DEFAULT '0',
  `product_prom` int(11) DEFAULT '0',
  `product_prom_limit` int(11) DEFAULT '0',
  `product_prom_limitdate` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_prom_limitdays` int(11) DEFAULT '0',
  `product_prom_discount` int(11) DEFAULT '0',
  `product_expfee` float(5,2) DEFAULT '0.00' COMMENT '商品快递费用',
  `product_supplier` int(11) DEFAULT '0',
  `product_storage` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '存储条件',
  `product_origin` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '商品产地',
  `product_unit` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '商品单位',
  `product_instocks` int(11) DEFAULT '0' COMMENT '商品库存，在没有规格的时候此字段可用',
  `product_indexes` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '商品分类搜索索引',
  `supply_price` float(11,2) DEFAULT '0.00',
  `sell_price` float(11,2) DEFAULT '0.00',
  `market_price` float(11,2) DEFAULT '0.00',
  `catimg` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(10) DEFAULT '0',
  `is_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='商品信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_info`
--

LOCK TABLES `products_info` WRITE;
/*!40000 ALTER TABLE `products_info` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `products_info` VALUES (1,'6946269600145','厂家批发炒粉调味料 炒河粉炒米粉香料汤粉小吃快餐调料 调味品',NULL,NULL,3,0,3,'<div id=\"offer-template-0\"></div><p>&nbsp;</p><p><span >公司出品炒粉料调味料就是针对这道小吃而开发的，有着独特配方设计，口味口感鲜美诱人。</span></p><p><span >&nbsp; &nbsp; &nbsp;当然，平常在烹饪中可代替味精、盐使用，享受独特美味，在拌面、烧烤、火锅及馄饨等点心小吃制作时添加个性调味。</span></p><p><span >【品名】御豪炒粉味料</span></p><p><span >【配料】食盐、植物油、味精、0水解蛋白、香辛料、I+G、脱水蔬菜。</span></p><p><span >【规格】454克/包 一箱20包&nbsp;</span></p><p><span >【使用方法】适用于炒饭、炒米粉、炒河粉、汤粉、汤面等！专业让美味变的更简适用于炒粉、拌粉、炒面、炒饭、亦可炖、焖、蒸、煮各种美味佳肴，是各类小吃、餐饮和居家烹饪的调味好帮手。</span></p><p><img  src=\"uploads/alibaba/3260/326006a14e0e055a53e95beb48f646a9.jpg\" /><br /><img  src=\"uploads/alibaba/8b95/8b95bd0972534b60520bc792fe4d3ef1.jpg\" /><br /><img  height=\"615.2125\" src=\"uploads/alibaba/b4ca/b4ca5abd526e1b090d1bae67fb5cf1bf.jpg\" width=\"790\" /><br /><img  height=\"789\" src=\"uploads/alibaba/453a/453a17e175b6e490b36191eb319020b6.jpg\" width=\"792\" /><br /><img  height=\"703.5157894736842\" src=\"uploads/alibaba/652e/652e95afd62d03f6ffca78cfe24c380c.jpg\" width=\"790\" /><img  height=\"587\" src=\"uploads/alibaba/ddd9/ddd95bffc5d94f3dda8d88dbbb05c70e.jpg\" width=\"809\" /><br /><img  height=\"271.09473684210525\" src=\"uploads/alibaba/44c6/44c6dcd35a1232d830c47ed54157c8e9.jpg\" width=\"790\" /></p><p><img  height=\"657\" src=\"uploads/alibaba/ab01/ab013fe6e31d3faad54b494226907d7d.jpg\" width=\"799\" /><br /><img  height=\"798\" src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" width=\"799\" /><br /><img  height=\"298\" src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" width=\"798\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span>&nbsp; &nbsp;<span >&nbsp;<span ><span >&nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></span></span></span></span><span ><span >户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></span></p><p>&nbsp;</p></td></tr></table><p><br /><img  height=\"722\" src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" width=\"800\" /><br /><img  height=\"968\" src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" width=\"797\" /></p><p><span >&nbsp; &nbsp; &nbsp;<span >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本 低毛利、低价格三高三低的经营理念。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系人：小味</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp;微信、电话：188 2581 9701</span></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,20.00,24.00,'uploads/alibaba/9397/9397d7386575a089ba5a8d4f03524660.jpg',0,0),(2,'6946269601197','御豪黄面包糠230g 炸鸡翅鸡柳鸡米花包裹粉 膨化糠面包屑炸鸡粉',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><p><a name=\"5770355\"></a></p><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/779/624/2899426977_772013751.jpg\" bgcolor=\"#ffffff\" colspan=\"4\" ></td></tr><tr><td ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/927/421/3070124729_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >披萨沙拉西餐原料玉米粒罐头400g 烘焙甜玉米罐头批发</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥ ？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/528639339889.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/025/041/3146140520_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >御豪菇爷酱280g 香菇酱蘑菇酱下饭酱 姑爷酱拌饭下饭菜调料酱批发</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/534159609914.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/313/918/3067819313_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发价</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/526315766040.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/872/344/2748443278_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >翻糖蛋糕饮料色素色油苹果绿水油两用食用色素 食品添加剂</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/526577569612.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td></tr><tr><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/071/321/3075123170_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >御豪玛利白色面包糠750g 炸鸡粉膨化糠 鸡腿鸡块鸡米花西餐原料厂</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/526563250588.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/213/224/2735422312_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >御豪袋装白胡椒粉454克 餐饮调料香辛料烧烤调味料调味品厂家</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/526388021838.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/922/364/3067463229_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >黄面包糠1kg炸鸡柳炸虾用裹粉面包屑厂家批发炸鸡粉</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/526323221209.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td><td align=\"center\" ><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"https://cbu01.alicdn.com/img/ibank/2016/718/662/3068266817_2074965439.170x170.jpg\" colspan=\"2\" ></td></tr><tr><td align=\"left\" colspan=\"2\" height=\"20\"><div >食用大红色素水溶性粉状烘焙色素 食品添加剂 饮料着色</div></td></tr><tr><td align=\"left\" height=\"20\"  valign=\"middle\" width=\"90\"><div >￥？元</div></td><td align=\"right\" height=\"20\"  width=\"80\"><a href=\"https://detail.1688.com/offer/526322377572.html\"  target=\"_blank\" title=\"立即购买\">立即购买</a></td></tr></table></td></tr></table></td></tr></table><p ><img  src=\"uploads/alibaba/a812/a8121184856f8d1d57677a939e428b83.jpg\" /><img  src=\"uploads/alibaba/593e/593ef51830a5903d0796b0b874596cc7.jpg\" /></p><p ><img  src=\"uploads/alibaba/f805/f805b1fc9445b01ad3537355b9a7bfde.jpg\"  /></p><p ><img  src=\"uploads/alibaba/935f/935f60d5e89fe8556dc5401a2b0242b6.jpg\" /><br /><img  src=\"uploads/alibaba/32e3/32e3ec8924f3d55e4486fd2c7a67d5bf.jpg\" /><img  src=\"uploads/alibaba/dba0/dba0c54e502de3f219e2e2776837804f.jpg\" /><img  src=\"uploads/alibaba/1c60/1c602e9dffc0f3ced2ceb7dcb2b25d8b.jpg\" /><img  src=\"uploads/alibaba/81f2/81f279f1c40452b42dad5928735af3a1.jpg\" /><br /><br /><img  src=\"uploads/alibaba/18f5/18f5658bafadd589d803be61cb5a0075.jpg\" /></p><p ><br /><img  src=\"uploads/alibaba/2f7f/2f7fb603ddbb43c0a1ffc8eba220f2f2.jpg\" /></p><p ><br /><img  src=\"uploads/alibaba/fa5d/fa5df4b3e8139296b4c89fec028f5e81.jpg\" /></p><p >&nbsp;</p><p >&nbsp;</p><p >&nbsp;</p><p ><img  src=\"uploads/alibaba/6df0/6df03e7f7c321463aa27c23e090f4c0e.jpg\" /><br /><br /><img  src=\"uploads/alibaba/2e38/2e382d1d516b9b43927cff8bf901d259.jpg\" /></p><p ><img  src=\"uploads/alibaba/990f/990f03458876a923b921816a75bb7441.jpg\" /><br /><br /><img  src=\"uploads/alibaba/62f8/62f8ea4cfdda4855680bc1052c270ce7.jpg\" /></p><p >&nbsp;</p><p ><span ><strong >&nbsp; &nbsp; &nbsp;我们是面包糠厂家支持一件代发，欢迎淘宝微商代理分销</strong></span></p><p ><span ><strong >&nbsp; &nbsp; &nbsp; &nbsp;<span ><strong>微信、</strong>电话：188 2581 9701 - 陈先生</span></strong></span></p><p ><span ><strong >&nbsp;<span >&nbsp; &nbsp;<span >微博：味趣网络</span></span></strong></span></p><p ><span >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,8.00,9.60,'uploads/alibaba/170f/170fe89b3e8728707197638d1bbd4f94.jpg',0,0),(3,'6912336173867','供应冠牌白醋500ml 酿造食用白醋 餐饮调料调味品美容洗脸醋批发',NULL,NULL,2,0,0,'<div id=\"offer-template-0\"></div><p><strong><span >酿造食醋500ml，1箱12瓶。</span></strong></p><p><strong><span >配料：水、食用酒精、食用盐、小麦、食品添加剂苯甲酸钠</span></strong></p><p><strong><span >用途：烹饪 烘焙 腌制等</span></strong></p><p><strong><span >产品加工工艺：液态发酵</span></strong></p><p><strong><span >保质期：2年</span></strong></p><p><strong><span >醋酸含量：&ge;3.50克/100ml</span></strong></p><p><strong><span >产地:广东东莞</span></strong></p><p>&nbsp;</p><p ><img  src=\"uploads/alibaba/0f87/0f877b6307b33aebccbb86c9c99c816e.jpg\" /><br /><img  src=\"uploads/alibaba/db6e/db6ef651f01d296a833b816c90ff4dc9.jpg\" /><br /><img  src=\"uploads/alibaba/158c/158cdcb2b0590348f006c016c96d2160.jpg\" /><br /><img  src=\"uploads/alibaba/2484/248454cfd73e82d2156f0ee068a66e57.jpg\" /><br /><img  src=\"uploads/alibaba/d01c/d01ce443082acd8478c48cbae4636b9e.jpg\" /><br /><img  src=\"uploads/alibaba/00c7/00c7c1e243761a047e9f73b0fe619091.jpg\" /></p><p ><img  src=\"uploads/alibaba/9f8f/9f8f7f0a89a0b4b81c286fa6c1df6958.jpg\" /></p><p><strong><span >白醋只发货运，亲提交订单时运费填写0，运费到付 ，亲要自己到网点提货；<span >广东省</span>1箱/2箱/3箱都是起步价30元（建议3箱起批），4至15箱10元/箱，15箱以上6元/箱；<span >其它省</span>份1箱/2箱到市区都是货运起步价30元（建议2箱起批），到县城40元；4至40箱到市区10元/箱，到县城15-20元/箱；40箱以上到市区7元/箱，到县城10-15元/箱（不含港澳台 新疆 西藏 内蒙古）。亲对运费不清楚的请咨询客服查询</span></strong><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img  src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img  src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img  src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp; &nbsp;<span >&nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><span >&nbsp;</span></p><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><span >&nbsp;</span></p><p><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img  src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img  src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</span></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</span></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp; 联系人：小味</span></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp; 微信、电话：188 2581 9701</span></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp; 微博：味趣网络</span></strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,2.50,3.00,'uploads/alibaba/299a/299a1a636bdf1bfa619667e6236af0b4.jpg',0,0),(4,'6921833000019','味趣玉米粒 披萨沙拉西餐原料玉米粒罐头400g 烘焙甜玉米罐头批发',NULL,NULL,9,0,2,'<div id=\"offer-template-0\"></div><p ><span ><span ><span >广东省满2箱包运费</span><span ><img alt=\"前期\" src=\"uploads/alibaba/8fd7/8fd7319dbd9a3243639a62109d4eeb80.jpg\" />其他省满20箱包运费（不含港澳台新疆 西藏 内蒙古 黑龙江）</span></span></span></p><p ><span ><strong><span >罐头优选秦皇岛农家玉米精心加工而成，经过</span></strong></span><strong >传统工艺脱粒、去杂质、清洗、数十道工序后焙制而成；是制作披萨、玉米烙、玉米沙拉、松仁玉米，玉米汁必备原料。<br /></strong></p><p><img alt=\"玉米粒罐头\" src=\"uploads/alibaba/6cd1/6cd16267a08167558d170764bdd7860b.jpg\" /><br /><br /><br /><br /><br /><img alt=\"玉米粒罐头厂家\" src=\"uploads/alibaba/0eca/0eca0ba3bfbbb7cffdaaa1ab95effecd.jpg\" /><br /><br /><br /><br /><br /><img alt=\"玉米粒罐头批发\" src=\"uploads/alibaba/ceae/ceae46d505a43826351715e04bcff38b.jpg\" /><br /><br /><br /><img alt=\"玉米粒罐头怎么吃\" src=\"uploads/alibaba/8cbd/8cbd3c57e6780c4b8ca92e0761cda648.jpg\" /><br /><img alt=\"奶香玉米烙\" src=\"uploads/alibaba/8bb6/8bb6a1eafbd95f1f561f788aa3a48beb.jpg\" /><br /><img alt=\"甜玉米粒\" src=\"uploads/alibaba/0522/0522ed5a342875fc8c566921f7b8eaf5.jpg\" /><br /><img alt=\"玉米粒罐头价格\" src=\"uploads/alibaba/8796/87964d91e04dcb3d93d130344cd3f0eb.jpg\" /><br /><img alt=\"玉米粒\" src=\"uploads/alibaba/e125/e1253829b43b02b51f15ad1c984cce9f.jpg\" /><br /><img alt=\"甜玉米粒\" src=\"uploads/alibaba/985f/985f16d35d24c0c8977baeab83bcd5bb.jpg\" /><br /><br /><br /><strong><img alt=\"玉米粒的做法大全\" src=\"uploads/alibaba/cd33/cd33905c5a30139e530d2d543a1c997b.jpg\" /></strong></p><p><strong><span >所有产品支持一件代发，玉米粒量大可以发货运自提，外省大概需要多少运费请咨询客服查询</span><img alt=\"玉米粒的家常做法\" src=\"uploads/alibaba/4566/45661a91d24a30f1cfaafa2d3d8658c0.jpg\" /><br /></strong><img alt=\"蛋黄焗玉米\" src=\"uploads/alibaba/d5cd/d5cde14e8ebe6872b57b78cb5b991131.jpg\"  /></p><p>&nbsp;</p><p><img alt=\"玉米披萨的做法\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"玉米披萨\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp; &nbsp; &nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"味趣玉米粒罐头\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"味趣玉米粒\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp;<span >&nbsp;&nbsp;<strong>味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></span></p><p><span ><strong>&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></span></p><p><span ><strong>&nbsp; &nbsp; &nbsp; 联系人：小味</strong></span></p><p><span ><strong>&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,2.50,3.00,'uploads/alibaba/76f3/76f3b928e31f344b9c131912eeb9dc6b.jpg',0,0),(5,'6946269601371','御豪玛利白色面包糠750g 炸鸡粉膨化糠 鸡腿鸡块鸡米花西餐原料厂',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><p ><strong ><img  src=\"uploads/alibaba/2d41/2d4120223cacb2119cc63d9bcd4b8c5a.jpg\" /><br /><br /></strong></p><p>&nbsp;</p><p><strong><br /><img alt=\"1\" src=\"uploads/alibaba/de16/de16a7c3f298f786d7c72017d5bbad12.jpg\" /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><br /><img alt=\"3\" src=\"uploads/alibaba/5a07/5a077069040ae6a9d19f64008a2eb6a8.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"7面包糠模板750g_01\" src=\"uploads/alibaba/25c2/25c20fad3869dfe5fa58777304a641e2.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"5面包糠模板750g_03\" src=\"uploads/alibaba/88b0/88b06623bff05452efc9fa84c1caa67c.jpg\" /><br /><br /><img alt=\"6面包糠750gshyff_03\" src=\"uploads/alibaba/0bd4/0bd446b2263a4b97c887a91b2ccc6356.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"8面包糠模板750g_02\" src=\"uploads/alibaba/9365/936514f6f9b976582bf2bc700a22d0e9.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img  height=\"759.2639327024185\" src=\"uploads/alibaba/3721/37218ca75fc766814511d1141212cfbc.jpg\" width=\"790\" /><br /><br /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"面包糠模板750g_05\" src=\"uploads/alibaba/941d/941d11d2529efb09197f4dac18535066.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"面包糠模板750g_06\" src=\"uploads/alibaba/d855/d8550ba41178d6a080432351666e182a.jpg\" /><br /></strong></p><p><strong><img alt=\"面包糠模板750g_07\" src=\"uploads/alibaba/19d6/19d6064e2d230db0a025cfce158fee30.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"面包糠模板750g_08\" src=\"uploads/alibaba/ac7d/ac7d91764801149e3a2468e84dc2f474.jpg\" /><br /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img alt=\"面包糠模板750g_09\" src=\"uploads/alibaba/ca26/ca265412a54202473d89f4fc27b4ff3e.jpg\" /></strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><strong><br /><img  src=\"uploads/alibaba/d294/d294283b2658dfba6d59bf44fe5362a7.jpg\" /><br /><br /><br /></strong></p><p><span ><strong>&nbsp; 微信、电话：188 2581 9701 &nbsp; &nbsp; 陈先生</strong></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,10.50,12.60,'uploads/alibaba/bfeb/bfeba6516c6e36234d8a5315fdb1cf88.jpg',0,0),(6,'6946269603115','顶豪胭脂红烘培食用色粉 饮料蛋糕色素色粉 食品染料厂家批发色粉',NULL,NULL,4,0,1,'<div id=\"offer-template-0\"></div><p><img  src=\"uploads/alibaba/ca27/ca2779c0abb756a313e15885e90cf0b5.jpg\" /></p><h2><span ><strong>单笔订单满1000元，免快递运费，送50元优惠券（订单满51元便可用）</strong></span></h2><p><br /><br /><img  src=\"uploads/alibaba/03fe/03fe030500b0bf5db7f4b21f7420c66c.jpg\" /><br /><img  src=\"uploads/alibaba/84d1/84d171cef2aa7f0a75571a464614c3d8.jpg\" /><br /><img  src=\"uploads/alibaba/de6b/de6b54bdcf9b7869373dc715f3ddb4ed.jpg\" /><br /><img  src=\"uploads/alibaba/2cb0/2cb0b584a8102bbbaf24862de59ff1f9.jpg\" /><br /><img  src=\"uploads/alibaba/14ba/14ba0f154ae3d578b6443b963dda09d6.jpg\" /><br /><img  src=\"uploads/alibaba/3d50/3d50c1aa9aa5ab919cdccaeb9e77110e.jpg\" /><br /><br /><br /><img  src=\"uploads/alibaba/f6bd/f6bdc52d3cd43694eba22fdb1214d81e.jpg\" /><br /><img  src=\"uploads/alibaba/fd3e/fd3eea582391c99af4491e26f49aa7ca.jpg\" /><br /><img  src=\"uploads/alibaba/d050/d050d43e53d00b4d8496687de9fb1722.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/5f73/5f738e15fb59e52790dc1d8ebc8b9e14.jpg\" width=\"790\" /><br /><br /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,15.00,18.00,'uploads/alibaba/da52/da526797f1b1b9362c707739374c3866.jpg',0,0),(7,'6946269603139','顶豪橙黄色素粉300g 翻糖蛋糕食用级染色剂 天然色素食品添加剂',NULL,NULL,4,0,0,'<div id=\"offer-template-0\"></div><p>&nbsp;<img  height=\"1015.0062735257214\" src=\"uploads/alibaba/af1d/af1d6c6a2e874ee976d82f29da0934d3.jpg\" width=\"790\" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p><h2><span ><strong>单笔订单满1000元，免快递运费，送50元优惠券（订单满51元便可用）</strong></span></h2><p><br /><img  src=\"uploads/alibaba/417d/417dcffb04b53863c51ba000e8349961.jpg\" /><br /><img  src=\"uploads/alibaba/0449/04494ec841081b788951a7ab64f35579.jpg\" /><br /><img  src=\"uploads/alibaba/adfc/adfc3421a0d38d040dbfd8546b7d6de0.jpg\" /><br /><br /><img  src=\"uploads/alibaba/1e00/1e00705500dd38e833269b6adc505dad.jpg\" /><br /><img  src=\"uploads/alibaba/55b0/55b0cc1195e60d47e8a2094270ba8fc8.jpg\" /><br /><img  src=\"uploads/alibaba/de83/de83dc504e928d247c60b89b9abcd8e2.jpg\" /><br /><img  src=\"uploads/alibaba/680e/680e7383a49709e0941c756738ecad1c.jpg\" /><br /><img  height=\"573.8933030646992\" src=\"uploads/alibaba/597d/597d0869e03258f2249760d58e4609cd.jpg\" width=\"790\" /><br /><br /><img  src=\"uploads/alibaba/9eac/9eac524f014a1a2c73794cd76459b66b.jpg\" /><br /><br /><br /><img  height=\"682.7263157894737\" src=\"uploads/alibaba/ccad/ccadb8604c4c7ae4711628a84ec2ae8c.jpg\" width=\"790\" /><br /><br /><img  height=\"405.26553672316385\" src=\"uploads/alibaba/4c12/4c1229213e1003d88913e6bbe2dc815d.jpg\" width=\"790\" /><br /><br /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,13.00,15.60,'uploads/alibaba/7a68/7a68bf46f35e9bf1a74c59bdf8b1ad93.jpg',0,0),(8,'6946269601388','御豪白色面包糠750g 炸鸡翅鸡柳猪排面包屑厂家 餐饮调料炸粉',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/68c2/68c27015a2cf2e82726108107fd45395.jpg\" /><br /><img  src=\"uploads/alibaba/53b8/53b8119c5ed0af7cc2d8639b43179195.jpg\" /><br /><img  src=\"uploads/alibaba/f423/f4232d2567784b309afbaf81ac4fae1d.jpg\" /><br /><br /><img  src=\"uploads/alibaba/62a3/62a3be2079b115a4d058673edec21d91.jpg\" /><br /><img  src=\"uploads/alibaba/05e9/05e93947c0996f971df709306e49c1d8.jpg\" /><br /><img  src=\"uploads/alibaba/7ae6/7ae673968cff96811557f8f432c4e6a4.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/97ba/97ba7038f7c7ea8f6aeb4de099b555f2.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/76d7/76d7cbcff984c370cd9242c585224809.jpg\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp;<span >&nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,8.50,10.20,'uploads/alibaba/1d2c/1d2cb3c64b24c03ff17a05918e7a80c4.jpg',0,0),(9,'69462669600185','御豪大骨浓汤粉908g 炒河粉汤粉大锅菜用调味品 汤料配方火锅底料',NULL,NULL,3,0,0,'<div id=\"offer-template-0\"></div><p><span >本品推荐用于汤粉、炒粉、火锅、大锅菜、关东煮、煲仔</span></p><p><img  src=\"uploads/alibaba/46bb/46bbda4f976226b9030dfe939ce9e41e.jpg\" /><br /><img  src=\"uploads/alibaba/38d1/38d1bf415bf342576330de2cc54ea4af.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/76bc/76bc63e25dcee43e820fd39d375f4d52.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/41eb/41eb350e4a8c0979d2834946e7d631ad.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/d15a/d15a4fe2a7742e700ca6a33e47eb7367.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/0a24/0a2481639dcd99ae9c1c31348fabdea4.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/0e86/0e867165d3c1c0cad47ecf0b12079b04.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/8daf/8daf3e9f671cdbcf9d978dbc5f251fe5.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/77b4/77b4c3e02ad246591e39ff15a064db08.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" height=\"573\" src=\"uploads/alibaba/36a3/36a3820bcd0302ae2575f193fb2eb6db.jpg\" width=\"790\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/00a5/00a5a48fa642e9ebcf119c777e455898.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" height=\"682\" src=\"uploads/alibaba/4872/4872b12d3ac3d3a148b3370c80923b30.jpg\" width=\"790\" /></p><p><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\"  /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"御豪大骨浓汤粉908g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,35.00,37.45,'uploads/alibaba/1fae/1faebb6d0d373422ae9776af1807ab54.jpg',0,0),(10,'6911567881169','东古一品鲜 特级纯酿造黄豆酱油 餐饮调料东古调味品批发价格代理',NULL,NULL,1,0,2,'<div id=\"offer-template-0\"></div><p ><span >品质保证，如假包换，放心拿货</span><img alt=\"东古一品鲜 (2)\" src=\"uploads/alibaba/097f/097f1944c88787ec753445dc4d5489d1.jpg\" /><br /><img alt=\"东古一品鲜 (3)\" src=\"uploads/alibaba/7fe6/7fe6549ff28950a46b681f9c1eca301e.jpg\" /><br /><img alt=\"东古一品鲜 (4)\" src=\"uploads/alibaba/3cfd/3cfd531f338dd6628dc3b75e3782a290.jpg\" /><br /><img alt=\"东古一品鲜 (5)\" src=\"uploads/alibaba/b1ce/b1ceec79f6d6aca87d6f649773c787ec.jpg\" /><br /><img alt=\"东古一品鲜 (6)\" src=\"uploads/alibaba/e511/e511d8309a4c246a8cb267543a0e0535.jpg\" /><br /><img alt=\"东古一品鲜 (7)\" src=\"uploads/alibaba/875b/875b68044c40c6c68201de387cc06cd6.jpg\" /><br /><img alt=\"东古一品鲜 (8)\" src=\"uploads/alibaba/7f32/7f3287b250c98e1f45037fa0f1c3a5e6.jpg\" /><br /><br /></p><p ><span >&nbsp; &nbsp;品牌介绍<img  src=\"uploads/alibaba/51a8/51a80d1ac1d94b52d22f81b0e8b9df6a.jpg\" /></span><br /><img  src=\"uploads/alibaba/8892/88927bea1591849b675a9c7f7bfd5b4a.jpg\" /><img  src=\"uploads/alibaba/b4bc/b4bc070237c74f001eaaa42b470e60a5.jpg\" /><br /><img  src=\"uploads/alibaba/4698/4698d3a1fc82514ad65ef10bd24de15d.jpg\" /><br /><img  src=\"uploads/alibaba/3220/322002449caf75f8f0ab1b62a7ffad84.jpg\" /><br /><img  src=\"uploads/alibaba/957d/957ddaa424727daa2b2d48f6f59edf06.jpg\" /><br /><img  src=\"uploads/alibaba/7060/70602fbb8c687653b01447899c3d0e03.jpg\" /><br /><img  src=\"uploads/alibaba/f930/f930261090075a001d4ef7709f0e5190.jpg\" /><br /><img  src=\"uploads/alibaba/5b51/5b5192c41a61c2caa42fee1fe710d2ab.jpg\" /><br /><img  src=\"uploads/alibaba/5c89/5c8943b9d37b5142c3fbd57400b6bd5a.jpg\" /><br /><img  src=\"uploads/alibaba/3d61/3d61e5d5a9c561c3816eaa5afc5ebc30.jpg\" /><br /><img  src=\"uploads/alibaba/cc6c/cc6cfd94bf225a12edf56b5add4d8bcc.jpg\" /><img  src=\"uploads/alibaba/ae04/ae04c9539adc519decb9fe79de3b2b27.jpg\" /></p><p ><span >东莞市内达到限额，可以免费送货上门！外地客户基本都是发物流运输，运费到付！（亲要到货运网点提货哦）亲提交订单时运费填写0 ，另：若特殊原因需要走其他运输，如快递等，请联系销售客服</span></p><p><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\"  /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span>&nbsp; &nbsp;<span >&nbsp;<span ><span >&nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></span></span></span></span><span ><span >户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p><span >&nbsp; &nbsp; &nbsp;<span >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本 低毛利、低价格三高三低的经营理念。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系人：小味</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</span></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,6.00,7.20,'uploads/alibaba/b38c/b38ca25387a4afb71fd75ea17d172239.jpg',0,0),(11,'6939571970150','御豪菇爷酱280g 香菇酱蘑菇酱下饭酱 姑爷酱拌饭下饭菜调料酱批发',NULL,NULL,9,0,2,'<div id=\"offer-template-0\"></div><p><img alt=\"姑爷酱 (2)\" height=\"790\" src=\"uploads/alibaba/c361/c36188c6869979f00a6c7743dd016ff6.jpg\" width=\"790\" /><br /><img alt=\"姑爷酱 (5)\" src=\"uploads/alibaba/6626/6626df6562164b86eda7acb8493a127e.jpg\" /><br /><img alt=\"姑爷酱 (6)\" src=\"uploads/alibaba/a6b9/a6b9c18cfe09a3a3aaf4acac5c19acb9.jpg\" /><br /><img alt=\"姑爷酱 (8)\" src=\"uploads/alibaba/33bb/33bb093c44607e807f6b953d94397004.jpg\" /><br /><img alt=\"姑爷酱 (9)\" src=\"uploads/alibaba/5830/5830965528184b2cd82f1fb7ab289339.jpg\" /><br /><img  src=\"uploads/alibaba/54cf/54cf1a36b4e4ed6ccb25fc6d0a493375.jpg\" /><br /><img alt=\"姑爷酱 (10)\" src=\"uploads/alibaba/777c/777cb1f1542682acfcfc7cbbfca1e535.jpg\" /><br /><img  src=\"uploads/alibaba/c8fe/c8fea65f48a1b9b9cc16fc8f819798df.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /><br /><br /></p><p><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p>&nbsp; &nbsp;&nbsp;<span >&nbsp;&nbsp;<strong>深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><strong><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></strong></p><p><br /><strong><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></strong></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><br /></p><p><span >&nbsp; &nbsp; &nbsp;<strong>&nbsp;味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本 低毛利、低价格三高三低的经营理念。</strong></span></p><p><strong><span >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</span></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp; 联系人：小味</span></strong></p><p><strong><span >&nbsp; &nbsp; &nbsp;<span >微信、电话：188 2581 9701</span></span></strong></p><p><br /><br /><br /><br /><br /></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,18.00,21.60,'uploads/alibaba/c13e/c13ecbd268cdc424356f0e65edc24728.jpg',0,0),(12,'JHDHSY250','劲豪大红色油200ml 翻糖蛋糕水油两用液体红色食用色素餐饮调料',NULL,NULL,4,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/f44d/f44d1e8646c0fa2d7ea2d8b0c66d48e4.jpg\" /><br /><img  src=\"uploads/alibaba/5a67/5a6794ead6ff9bd71d080f67ec70da47.jpg\" /><br /><img  src=\"uploads/alibaba/aa22/aa22c6c509897c2c72a2f9b5f12eb413.jpg\" /><br /><img  src=\"uploads/alibaba/8b63/8b633ce0224dc54c46c99b379dce48f1.jpg\" /><br /><img  src=\"uploads/alibaba/2222/22224785049cd2bc5151977b7086a610.jpg\" /><br /><br /></p><p><span >厂家直销、批发 食品级色素，烘焙蛋糕 棉花糖色素 顶好水油两用液体食用日落黄色素,</span></p><p>公司所有产品都是厂家直营，正品保证，欢迎加盟代理! 共有七种颜色可选[果绿][柠檬黄][橙黄][日落红][橙红][日落黄]<br />本产品的作用，能促进人的食欲，增加消化液的分泌，因而有利于消化和吸收，是食品的重要感官指标。 本品应用范围可用于果汁、汽水、酒、糖果、糕点、果味粉、罐头、冷饮等食品的着色，也可用于日用化工产品（如牙膏等）的添加剂，在医药工业用作药片外衣的着色，还可用于化妆品的着色。</p><p><br /><br /><br /><img  height=\"573\" src=\"uploads/alibaba/ad54/ad54a43d972dda7a98d37cd560877404.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/241c/241cbc3985c1fac594e6ae57cbfd4b9c.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/079e/079e9acd8cc54c7c9c4d6510ff6d7246.jpg\" width=\"790\" /></p><p><img  src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img  src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></p><p><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></p><p><br /><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></p></td></tr></table><p><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img  src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,15.00,18.00,'uploads/alibaba/66b1/66b16bf31bf2b951dda0c9c6c14fa2ea.jpg',0,0),(14,'6946269600183','大骨浓汤粉180g 炒河粉汤粉大锅菜多用汤料配方 火锅底料调味品厂',NULL,NULL,3,0,0,'<div id=\"offer-template-0\"></div><p><img alt=\"大骨汤\" src=\"uploads/alibaba/46bb/46bbda4f976226b9030dfe939ce9e41e.jpg\" /><br /><img  src=\"uploads/alibaba/0284/0284d2175e26aeaa2b0249935b392c3f.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/b235/b23515b4f6ff68878004b85a83e33f7a.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/23fa/23fa659eb34bcdbfc2442e20091d1bbf.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/9c50/9c5097e6b901bd91ba92d26edf380c91.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/e80f/e80fd7c89212a59ce5c39f715c2432b1.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/0cfd/0cfdc2d42bd6febadfa24e3bfc1a9603.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/b0ef/b0ef768e37ed845ab4aed85acc9fe1d2.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" height=\"682\" src=\"uploads/alibaba/18b4/18b49d15266e6d5532a246a814f1f3c7.jpg\" width=\"790\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/980e/980ed8a96fd17382a50da2131fbb51db.jpg\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" height=\"573\" src=\"uploads/alibaba/b2df/b2df5006e5db994d1815f629e50ccf0f.jpg\" width=\"790\" /><br /><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/5b3e/5b3eb7acea0d025a17038ce01fa05071.jpg\" /></p><p><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"御豪大骨浓汤粉180g 高汤大骨浓汤粉调味料 御豪大骨浓汤粉批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,10.00,20.20,'uploads/alibaba/4cd6/4cd62f381420f28cc35c28494b48fb5c.jpg',0,0),(16,'6946269601371','白色面包糠1kg 餐饮调料炸鸡汉堡裹粉 膨化面包糠diy炸粉厂家批发',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><div><span >面包糠是一种广泛使用的食品添加附料，用于油炸食品表面，如：炸鸡肉、鱼肉、海产品（虾）、鸡腿、鸡翼、洋葱圈等。其味香酥脆软、可口鲜美、营养丰富。批发扎菲黄色面包屑面包糠1000G&nbsp; 优质发酵型面包屑/黄糠，欢迎加盟代理！量大从优！</span><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/5b20/5b2003d5485719ad2af15909bf8e06fc.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/36ee/36ee71c6d406c6e22fe9dcc86341f9dc.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/17bf/17bf47141388ba0b115df176d128ed9a.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/a4dd/a4dda89ab4c615b7f94257a686a4e937.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/1ae1/1ae1b4a807a01fe907b4faef9b59b398.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/a48e/a48e5ecd0c154c8c42cdc37fae065918.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" height=\"573\" src=\"uploads/alibaba/3ae4/3ae478cf9a45aae6bfd2553cc84d78e8.jpg\" width=\"790\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" src=\"uploads/alibaba/8ba1/8ba1d40c50a512f9a42a8bfc46132056.jpg\" /><br /><img alt=\"御豪玛利1kg白色面包糠 炸鸡汉堡裹粉膨化面包糠 批发御豪面包糠\" height=\"682\" src=\"uploads/alibaba/b9c0/b9c0364dd03ee7eaa2b30833735c8ea4.jpg\" width=\"790\" /><br /><p>&nbsp;</p><p><img  src=\"uploads/alibaba/ab01/ab013fe6e31d3faad54b494226907d7d.jpg\" /><br /><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span >&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p><span >&nbsp;</span></p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p><span >&nbsp; &nbsp; &nbsp;<span >&nbsp;</span></span></p></div><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,11.50,13.80,'uploads/alibaba/27d3/27d357b75d0dc72f06184695bf024f1f.jpg',0,0),(17,'6902088900064','餐饮调料 家乐鸡粉罐装调味料2KG 家乐牌鸡粉调味品',NULL,NULL,6,0,0,'<div id=\"offer-template-0\"></div><div><br />&nbsp;</div><div><br /><br /></div><p><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/b2b0/b2b0ea0b1b5498e102f84d4f106dd909.jpg\" /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/e3f1/e3f1b141c9817efbade5d5d0134375c0.jpg\" /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/1453/1453445541884ac3a43604fb028530ef.jpg\" /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" height=\"750\" src=\"uploads/alibaba/296b/296bb930bea33af2816bf24507c4ba62.jpg\" width=\"750\" /><br /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/a754/a75415f80766dd5139fb1565f75ef0fb.jpg\" /><br /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/9cf6/9cf6205536d1e31a5c03bb84519b47df.jpg\" /><br /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/31ff/31ffc8adc89af3aae2fb16dcd4f1eca9.jpg\" /><br /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/7d07/7d076581fa56d4b862f1534c7a3841f5.jpg\" /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/5267/52678f1dc7e519e999d85c46b09880dd.jpg\" /><br /><br /><br /></p><p><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong><span >&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong></span><span ><strong><span >户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></strong></span></p><p><br /><span ><strong><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></strong></span></p><p><br /><span ><strong><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></strong></span></p></td></tr></table><p><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"家乐鸡粉调味料罐装2KG 家乐鸡粉供应实体批发 家乐鸡粉一件代发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,67.00,71.69,'uploads/alibaba/6cbe/6cbeb9fff3f82a81d6298542a0163bf8.jpg',0,0),(18,'6911567881268','东古一品鲜1.6L 特级纯酿造黄豆酱油 调味品调料东古酱油批发价格',NULL,NULL,1,0,0,'<div id=\"offer-template-0\"></div><div >&nbsp;<img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" height=\"790\" src=\"uploads/alibaba/c620/c62058db0d6ac8e95d6b93932b9e4eaa.jpg\" width=\"790\" /></div><div ><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" height=\"790\" src=\"uploads/alibaba/8235/82353f7c1b30a5cbb912116e79ff363f.jpg\" width=\"790\" /><br /><br /><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/4386/438628744fb0bd7137a8a5292e9444bd.jpg\" /><br /><br /><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/9a83/9a839ab7bbd4cb1d5bc0f046769fe3dd.jpg\" /><br /><br /></div><p><em><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/ab02/ab02cdca46b1e0468ee678db59980cbd.jpg\" /><br /><br /></em></p><p align=\"left\"><span ><span >宝贝名称：东古一品鲜酱油</span></span></p><p align=\"left\"><span ><span >净 含 量：1.6L</span></span></p><p align=\"left\"><span ><span >规&nbsp;&nbsp;&nbsp;&nbsp;格：6桶/箱</span></span></p><p align=\"left\"><span ><span >保 质 期：18个月</span></span></p><p align=\"left\">&nbsp;</p><p align=\"left\"><span ><span >东古一品鲜特级酿造酱油</span></span></p><p align=\"left\"><span ><span >&nbsp;&nbsp;&nbsp;是以精选优质东北大豆、小麦粉、食用盐为原料，利用天然日晒进行长达二个多月的高盐稀态发酵酿造而成。豉香味浓郁、色泽红润、鲜味十足。适用于蘸点鱼、虾、蟹，拌食粉面、凉菜等。亦可烹调各种菜肴。只需滴少许即能增加各种菜肴的鲜味</span></span></p><p align=\"left\">&nbsp;</p><p align=\"left\">&nbsp;</p><p align=\"left\">&nbsp;</p><p><span >&nbsp;<img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/34db/34db0981f3fd5258c4d0b943d6836c2d.jpg\" /><br /><br /><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/6ee1/6ee159c2e14e3e7bcb14500f57ccd6e8.jpg\" /><br /><br /></span></p><p>&nbsp;</p><p><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"东古一品鲜1.6L*6桶 东古一品鲜特级酿制酱油 东古一品鲜一件代发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,21.00,25.20,'uploads/alibaba/32a3/32a3ec45327839f7b12b1e5ac4e494ed.jpg',0,0),(19,'6911567888618','东古一品鲜5L  纯酿造黄豆酱油 餐饮调料东古调味品批发价格',NULL,NULL,1,0,0,'<div id=\"offer-template-0\"></div><p><img  src=\"uploads/alibaba/f138/f1386d650274373ca989d63c07de7460.jpg\" /><br /><img  src=\"uploads/alibaba/366c/366ce445d54ba207c60c93c2539c38e0.jpg\" /><br /><img  src=\"uploads/alibaba/5aa2/5aa2c858ea0aeb06f808b6091ebd98e7.jpg\" /><br /><img  src=\"uploads/alibaba/3021/30216dad7904450be927332a72b5cd17.jpg\" /><br /><img  src=\"uploads/alibaba/6cfa/6cfa1528098e5b97fbc092d5e83a4ca9.jpg\" /><br /><img  src=\"uploads/alibaba/88a0/88a017bf4d37b74dc4a85ba7c514977d.jpg\" /><br /><img  src=\"uploads/alibaba/1cd0/1cd0e6e2e330a8a4d603bf9b95223c1b.jpg\" /><br /><img  src=\"uploads/alibaba/6904/6904632ba7b6a87b2e8954729f74bea2.jpg\" /><br /><img  src=\"uploads/alibaba/1dd7/1dd71075a34d00faa23288703ca1ee8b.jpg\" /><br /><br /><img  src=\"uploads/alibaba/e1a6/e1a6d97bdbc2445ca596efbb87a3ac1b.jpg\" /><br /><br /><img  src=\"uploads/alibaba/1373/13739c2b47edc64bfb08d02d6376377c.jpg\" /><br /><br /><img  src=\"uploads/alibaba/31e9/31e9d1dbeeae24fb48c7dfff5f795faf.jpg\" /><br /><img alt=\"东古一品鲜\" src=\"uploads/alibaba/cc6c/cc6cfd94bf225a12edf56b5add4d8bcc.jpg\" /><br /><img alt=\"22222\" src=\"uploads/alibaba/ae04/ae04c9539adc519decb9fe79de3b2b27.jpg\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></p><p><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></p><p><br /><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,54.00,57.78,'uploads/alibaba/efeb/efeb2f223778df6761ef74e2aebc7c35.jpg',0,0),(20,'6939471900423','御豪一品鲜黑豆酱油500ml 生态古法酿制农家餐饮调料',NULL,NULL,1,0,1,'<div id=\"offer-template-0\"></div><p><img  src=\"uploads/alibaba/1b68/1b68c027cd627d8d317a1d7748db6d60.jpg\" /><br /><br /><img  src=\"uploads/alibaba/aabb/aabb3e004fbaf0809b5133bd5b419559.jpg\" /><br /><br /><img  src=\"uploads/alibaba/8295/8295e837580322d1d2eb6e5b8f91baff.jpg\" /><br /><br /><img  src=\"uploads/alibaba/91b9/91b9739fa30568bc46a3dde754ce1181.jpg\" /><br /><br /><img  src=\"uploads/alibaba/3aa7/3aa73d3d5a7a9fbd3dc7d311ca2bff4a.jpg\" /><br /><br /><img  src=\"uploads/alibaba/0292/0292f9141c2b69c59b29f67ab55ac59d.jpg\" /><br /><br /><img  src=\"uploads/alibaba/12b0/12b0f1801d90710d1708bac2e416ce63.jpg\" /><br /><br />&nbsp;</p><p><img  src=\"uploads/alibaba/ab01/ab013fe6e31d3faad54b494226907d7d.jpg\" /><br /><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span>&nbsp; &nbsp;<span >&nbsp;<span ><span >&nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></span></span></span></span><span ><span >户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p><span >&nbsp; &nbsp; &nbsp;<span >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本 低毛利、低价格三高三低的经营理念。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系人：小味</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</span></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,10.00,20.20,'uploads/alibaba/ef19/ef193aa9d98e855ee1fb903ee9a151c7.jpg',0,0),(21,'6939571970167','御豪拌饭酱280g 豆豉香菇意大利面粉馒头下饭菜香辣黑豆酱原味',NULL,NULL,9,0,2,'<div id=\"offer-template-0\"></div><p>&nbsp;</p><p><img  src=\"uploads/alibaba/ab01/ab013fe6e31d3faad54b494226907d7d.jpg\" /><br /><img  src=\"uploads/alibaba/274a/274a878182e9f5475fda2eb75b61a295.jpg\" /><br /><br /><img  src=\"uploads/alibaba/086f/086f342713ad915d82beff978ab47258.jpg\" /><br /><br /><img  src=\"uploads/alibaba/f282/f2822dfb3307da957155cc58afdfa3e4.jpg\" /><br /><br /><img  src=\"uploads/alibaba/ea38/ea3849acbe92ecd9ea69c6aa40bbe4d5.jpg\" /><br /><br /><img  src=\"uploads/alibaba/72ce/72ceab919f4e642ea40318f984a3a5ec.jpg\" /><br /><br /><br /><br /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span>&nbsp; &nbsp;<span >&nbsp;<span ><span >&nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></span></span></span></span><span ><span >户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></span></p><p><br /><span ><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></span></p><p>&nbsp;</p></td></tr></table><p><br /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p><span >&nbsp; &nbsp; &nbsp;<span >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本 低毛利、低价格三高三低的经营理念。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系人：小味</span></span></p><p><span ><span >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</span></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,18.00,21.60,'uploads/alibaba/a543/a543233ccaf67d47259caecd9394d72d.jpg',0,0),(22,'6939571900034','御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑酿制 礼品酱油厂家批发价',NULL,NULL,1,0,0,'<div id=\"offer-template-0\"></div><p><span >关注家人健康，从纯天然<strong><span >黑豆酱油</span></strong>开始&hellip;&hellip;</span><br /><span >我国人对健康、安全的诉求越来越重视，购买绿色无公害蔬菜、到处寻找不打激素的土鸡土鸭、不惜高价购买进口食材，只为家一片安全的净土，但别忽视了酱油这些&ldquo;老伴儿&rdquo;。</span></p><p>&nbsp;</p><p><span >&nbsp;<img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"468\" src=\"uploads/alibaba/48fc/48fc578892da788c325b77d950397f57.jpg\" width=\"750\" /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"366\" src=\"uploads/alibaba/636c/636c67741caf6d218157634ff81b4c79.jpg\"  width=\"750\" /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"412\" src=\"uploads/alibaba/c516/c516879ac185747e00d5f9f7b7477d3c.jpg\" width=\"750\" /><br /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"763\" src=\"uploads/alibaba/95c9/95c90dcdef44621677514056728e2d8b.jpg\" width=\"750\" /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"397\" src=\"uploads/alibaba/6623/6623f3be64dfd7a14f2c307bde626ea7.jpg\" width=\"750\" /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"324\" src=\"uploads/alibaba/685e/685effb4843fa660fab0374342bc7cda.jpg\" width=\"750\" /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"343\" src=\"uploads/alibaba/823b/823bf82c25f55cbe3f6531c61a9b909d.jpg\"  width=\"750\" /><br /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"269\" src=\"uploads/alibaba/f891/f89181fd652fbed3ea19c891fe622a2a.jpg\" width=\"750\" /><br /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"319\" src=\"uploads/alibaba/12ba/12baf2266814e30c3f24e660dc1b7588.jpg\"  width=\"691\" /><span ><strong>营养从口入，不知不觉中&hellip;&hellip;</strong></span><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"329\" src=\"uploads/alibaba/3bd7/3bd7b1d1aeb7d591e58a5e197d286746.jpg\" width=\"750\" /><br /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" src=\"uploads/alibaba/63d3/63d3fd28619e190fbf4ee09d00f90a91.jpg\" /><br /><br /><img alt=\"御豪天然酿黑豆酱油 古法瓦罐生晒黑豆黑小麦酿制 酱油厂家批发\" height=\"300\" src=\"uploads/alibaba/6185/61858fdc647124b3f54fa0503fa15cd3.jpg\" width=\"750\" /></span></p><p>&nbsp;</p><p align=\"left\">&nbsp;</p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,30.00,0.00,'uploads/alibaba/f785/f78593279a6b4a71ef20445589657ec7.jpg',0,0),(23,'6946269600268','劲豪鲜味汁880克 中国料理 鲜味汁餐饮调料调味品厂家批发价格',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><div><img  src=\"uploads/alibaba/9f29/9f293cfd3dc2c5848a7f655feffc43f6.jpg\" /><br /><img  src=\"uploads/alibaba/67b2/67b203f303aed1aedf65ae50b808852d.jpg\" /><br /><img  src=\"uploads/alibaba/de74/de74afde64bd90ef508f5cf149d54c86.jpg\" /><br /><img  src=\"uploads/alibaba/7829/7829b93ebd388f0f12991a05fec88368.jpg\" /></div><p><img  src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /></p><p><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp;&nbsp; &nbsp;<span >&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',0,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,25.00,30.00,'uploads/alibaba/0c5a/0c5a6003e40f641ddd70e4b2a042a86e.jpg',0,0),(25,'6946269601395','御豪豆捞百搭海鲜调味汁 豆捞酱油佐餐鱼片寿司调味品批发',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><div>&nbsp;</div><p><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"856\" src=\"uploads/alibaba/a8f4/a8f4e2500471fc8c6115edf758ce38b1.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"1086\" src=\"uploads/alibaba/f049/f0492e2c9807229ddda31fb991928b3d.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"837\" src=\"uploads/alibaba/01d0/01d01104a50571c37a23ff7112f672bd.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"765\" src=\"uploads/alibaba/9ab7/9ab7fb0684eebcd693b76b0500b1f742.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"725\" src=\"uploads/alibaba/26dc/26dcc1003c4b00814aecad9ed05b2e97.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"1395\" src=\"uploads/alibaba/85c1/85c115f0f7a65706b5b8603c7a4ef3ec.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"655\" src=\"uploads/alibaba/53df/53df74d1fed61bc0687d4e8fa356c69f.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"626\" src=\"uploads/alibaba/d91e/d91e4cf3c6f34c47ddc0f4fe2dc4d50e.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"446\" src=\"uploads/alibaba/4917/4917e6259dde79a37c2dfdbb5399fe33.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"1710\" src=\"uploads/alibaba/dbf9/dbf931fc2c44b8b24092159adf86de73.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"1733\" src=\"uploads/alibaba/1a42/1a4222fd7e5be3890628c252787a2c2c.jpg\" width=\"750\" /><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"2570\" src=\"uploads/alibaba/ac6b/ac6b40f02d29e4abb9a9f7b3ca2e3e42.jpg\" width=\"750\" /></p><div><span >酱油俗称<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">豉油</a>，主要由<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">大豆</a>、<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">淀粉</a>、<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">小麦</a>、<a href=\"https://detail.1688.com/offer/523957465014.html?spm=a2615.7691456.0.0.dTb4So\" target=\"_blank\">食盐</a>经过制油、<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">发酵</a>等程序酿制而成的。酱油的成分比较复杂，除食盐的成分外，还有多种<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">氨基酸</a>、糖类、有机酸、色素及香料等成分。以咸味为主，亦有鲜味、香味等。它能增加和改善<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">菜肴</a>的口味，还能增添或改变菜肴的色泽。我国人民在数千年前就已经掌握<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">酿制</a>工艺了。酱油一般有<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">老抽</a>和<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">生抽</a>两种：生抽较咸，用于提鲜；老抽较淡，用于提色。</span></div><p><span >厂家直营，批发酱油、澳门鱼生寿司海鲜火锅豆捞酱油、 御豪寿司豆捞酱油&nbsp;&nbsp;<img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" src=\"uploads/alibaba/87e4/87e47a013770832aaac7788a3a4a0b5b.gif\"  />本产品为公司酿造酱油！公司为推广本澳门豆捞酱油特推出零利润促销！<img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" src=\"uploads/alibaba/30da/30da4a5333b3671f51b6cd0ccc4f8d94.gif\"  />，，亲们不要再议价哦！欢迎代理加盟及天猫，淘宝分销</span></p><p><br /><img alt=\"御豪豆捞百搭鲜调味汁200ml*12 豆捞酱油佐餐调味汁 调味汁批发\" height=\"579\" src=\"uploads/alibaba/69c5/69c5674b2a91776677637fea4f0ccd66.jpg\" width=\"750\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,8.00,9.60,'uploads/alibaba/cdbf/cdbfe1cc8f8cae0cb2c89343a18a9e3f.jpg',0,0),(28,'0000025','劲豪鲍鱼汁390g*12 味浓醇香 增鲜去味 餐饮调料 酒店专用调味品',NULL,NULL,8,0,1,'<div id=\"offer-template-0\"></div><div>&nbsp;</div><p><img  src=\"uploads/alibaba/d336/d336bf7484b9bde82b1eb9a634e5f7d8.jpg\" /><br /><img  src=\"uploads/alibaba/ddba/ddba1babe72c97d7c5e0e52cc010923a.jpg\" /><br /><img  src=\"uploads/alibaba/3b8e/3b8ebf579bbdc7c093e50a341e8cff83.jpg\" /><img  src=\"uploads/alibaba/8acc/8accd8a0f30e5a47e5ccd8791f6cbfc7.jpg\" /><br /><img  src=\"uploads/alibaba/df48/df4854c6503ba8e52801a4d4f64ee475.jpg\" /><br /><img  src=\"uploads/alibaba/65b9/65b92df912faee7881b6c2e77222e52b.jpg\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,96.00,100.80,'uploads/alibaba/3c51/3c51323151d854384b26df4d31e0a3f0.jpg',0,0),(31,'6918678310038','李锦记上等财神蚝油907克 高端调味品李锦记蚝油 餐饮调料调味品',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><div>&nbsp;</div><p><img  height=\"702\" src=\"uploads/alibaba/fd40/fd4050cc311e0cce008a99fbd16bf8b3.jpg\" width=\"749\" /><br /><br /><img  height=\"754\" src=\"uploads/alibaba/7461/7461a9a553b0907ca8a4228fbe6430ad.jpg\" width=\"751\" /><br /><img alt=\"QQ截图20151225142324\" height=\"820\" src=\"uploads/alibaba/401f/401fb9cde7da980bb7ba6a60315b67b0.jpg\" width=\"750\" /><br /><img alt=\"QQ截图20151225142342\" height=\"453\" src=\"uploads/alibaba/11c4/11c42d233cd4da2b4c94e3f409c06cb2.jpg\" width=\"748\" /><br /><img alt=\"QQ截图20151225142024\" height=\"598\" src=\"uploads/alibaba/545c/545c5471cb8ee0cb6263adf6989ae164.jpg\" width=\"750\" /></p><p><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><img alt=\"面包糠 (14)\" src=\"uploads/alibaba/de81/de819bfe2fa49176d1a51f3401ea5d68.jpg\" /><br /><br /><br /><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp;&nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"湘汝辣王之王1.2kg 湖南特产农家剁辣椒 剁椒鱼头专用超辣辣椒酱\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,13.00,15.60,'uploads/alibaba/7bdc/7bdc8f17bdc5ddaae6eea1a0373d3e81.jpg',0,0),(32,'DGZ600','御豪大骨汁600克 煲汤专用汤料配方高汤底火锅底料餐饮调料调味品',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/e529/e52994f93088d7940d2a5734f41f91a8.jpg\" /><br /><img  src=\"uploads/alibaba/7a1f/7a1fc6ab8f141563deff0b2ef4b96f8e.jpg\" /><br /><img  src=\"uploads/alibaba/c626/c626414ec60a34d64ba0bc36c3924a28.jpg\" /><br /><img  src=\"uploads/alibaba/b833/b83376ff4b09b1bc7f01f0933c19ad73.jpg\" /><br /><img  src=\"uploads/alibaba/fe9b/fe9b4e7977f691edd9bc1559fe253cc2.jpg\" /><br /><img  src=\"uploads/alibaba/36f9/36f9a074c18450d7dcf85c71104922c5.jpg\" /></p><p><strong>御豪大骨汁产品制作工艺：</strong></p><p><strong>御豪大骨汁是以新骨头和瘦肉为原料，经高温熬煮分离浓缩后，配以蔬菜、香辛料等原料精心调配而成，产品状态均匀，调度适中，流动性好，色泽自然且耐熬煮，口感醇厚，骨肉感强，回味好，回味无穷。</strong></p><p><strong>&nbsp;产品描述:</strong></p><p align=\"left\"><strong>【品名】：御豪大骨汁</strong></p><p align=\"left\"><strong>【配料】：水、食盐、新鲜肉骨头、火腿、瑶柱、鲍鱼、白砂糖、淀粉、食品添加剂（黄源胶、山梨酸钾）</strong></p><p align=\"left\"><strong>【规格】：600g*6/件 2000gX6/件</strong></p><p align=\"left\"><strong>【保质期】18个月</strong></p><p align=\"left\"><strong>【保存方法】：开封后，存放于阴凉干爽处，尽快食用。</strong></p><p align=\"left\"><strong>【产品标准号】：Q/YH008</strong></p><p align=\"left\"><strong>【产品用途】：</strong></p><p align=\"left\"><strong>该产品为御豪食品公司独创汤类特色调料产品之一，可代替鸡汁、汤皇高汤使用，肉味强，用途广泛，适用于配制高汤、上汤、二汤、羹汤、米线汤、粉面汤、火锅汤底、云吞汤、水饺汤、老火锅、中西例汤等各种用汤以及中西菜肴烹调。</strong></p><p><br /><img  src=\"uploads/alibaba/3ca1/3ca19b757b7962e3938ddc4d64f13657.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/428c/428cdd8d45134aeb08c9aafe71a348e7.jpg\" width=\"790\" /><br /><img  height=\"682\" src=\"uploads/alibaba/fc97/fc97937e5b497f2570fa4ed044f59e91.jpg\" width=\"790\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,50.00,53.50,'uploads/alibaba/1254/1254d18589adc4fb986223fc03f1794b.jpg',0,0),(33,'THGT600','御豪汤皇高汤600g 麻辣烫汤l调味品酒店餐饮调料 小吃料火锅底料',NULL,NULL,8,0,1,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/1502/15023d6c24ac662facf8bdfe25525db5.jpg\" /><br /><img  src=\"uploads/alibaba/8356/83565b863d6ced6abb0e2412d2d322f3.jpg\" /><br /><img  src=\"uploads/alibaba/7f06/7f06f83b917754b83403db2fb8371cb9.jpg\" /><br /><img  src=\"uploads/alibaba/f105/f105584a4103cac9567d0d2c9398ee25.jpg\" /><br /><img  src=\"uploads/alibaba/06f2/06f22a4b0f60e11207e6cfb04697d88b.jpg\" /><br /><img  src=\"uploads/alibaba/955c/955c0d20d1e6873d04683637bf150086.jpg\" /></p><p><span >&ldquo;唱戏的腔，厨师的汤&rdquo;，我国历代厨师都非常重视和讲究高汤的制作和应用。所谓高汤（也叫鲜汤、上汤、吊汤）就是采用呈味物质和营养成分含量丰富的动物性或植物性原料，经过一段时间煮、炖，取其精华而制作的一种鲜味均衡、香味浓郁、留香悠长、厚味突出，肉香、脂香独特醇厚的汤汁。这种高汤在烹饪过程中起着十分重要的调味作用，是我国历代厨师在烹饪过程中重要的鲜香味来源。</span></p><p><span >&nbsp;</span></p><p><span >肉类食品经烹调后，能释放出肌溶蛋白、肌肽、肌酸、肌酐、嘌呤碱和氨基酸等物质，这些总称为含氮浸出物。如果肉汤中含氮浸出物越多，味道就越浓，越香，对胃液分泌的刺激作用也越大。2008年，御豪公司研发部开始了汤皇肉汤复合调味料的研究开发工作，借鉴厨房制作高汤的原料和方法，选用优质的鸡、鸭、猪肉、火腿、牛肉及龙眼为原料，采用文火长时间熬煮浓缩，将传统制作高汤的方法融入到现代科学工艺中，产品含氮浸出物含量非常高，只需加入数滴便能带出佳肴原有的色、香、味，适合烹调（煎、炒、焖、炖、蒸）各式菜肴，尤其适合制作高汤，营养丰富、汤汁清澈，口感清甜鲜美，醇厚浓香，可以与厨师在厨房中熬制的高汤产品媲美。2010年汤皇上市后，在餐饮行业得到广泛的应用，有的酒店直接用御豪汤皇直接代替厨房熬制的高汤，不仅方便快捷，而且价廉物美，深受广大厨师和家庭消费者的喜爱。</span></p><p><span >产品突出特点：</span></p><p><span >1、产品工艺：借鉴高档酒店熬制高汤的工艺和配料，是传统工艺和现代生产技术的完美结合。</span></p><p><span >2、使用：省时省力、方便快捷、风味稳定：省却了传统制作高汤的复杂程序，只需在开水中加入汤皇，短时间内便能得到鲜香味美的优质高汤，因是工业化、标准化生产，产品风味稳定。</span></p><p><span >3、应用，特别适合制作高汤:汤色清澈透明，汤味清甜鲜美。</span></p><p><br /><img  src=\"uploads/alibaba/0a09/0a09e533780f9aec54670c4a8714e480.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/4205/420573003f2b8fe9d4ddf6dd6397651a.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/3903/3903cb7b7e26abc8d0c822f422918cbd.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/f2a4/f2a4b6be26f76c331e78b8d920e2b7a1.jpg\" width=\"790\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,38.00,40.66,'uploads/alibaba/a65a/a65a8b916124a6cd3487ec2e1314c30c.jpg',0,0),(35,'6946269600565','劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 鲍鱼汁厂家一件代发',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><p>&nbsp;</p><p><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/9e40/9e4084fac73796c5c2f100537429a26b.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/b122/b12213517c601923f52dfeb57a2e6ff2.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/01be/01be211f806c30bd152404a7b7e0906d.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/94a9/94a94f8af225ee69517ea66c7ca66427.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/ec04/ec04badaf327c1800af8942ba4bee1d2.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/ebfa/ebfa9a8471ed686ac5229da261df95aa.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/ee35/ee35cf1794dacb8936aee42386c2f5ac.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" height=\"573.8933030646992\" src=\"uploads/alibaba/e30b/e30bc7ab850b5e335386f48e0dc2e553.jpg\" width=\"790\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/83b6/83b6d2f9c5472e431febc84c403a8f53.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" height=\"682.7263157894737\" src=\"uploads/alibaba/14e5/14e55275e74cd56c33ea28408a1d6621.jpg\" width=\"790\" /></p><p><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"劲豪鲍鱼汁390g 提鲜即食捞饭调味用鲍鱼汁 劲豪鲍鱼汁一件代发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,15.00,18.00,'uploads/alibaba/99c5/99c5fdcac653f952c7b7ef1fe5da5658.jpg',0,0),(37,'6946269600701','御豪水晶鸡配料 湛江蒸鸡调味品 餐饮调料调味料酒店调味香料批发',NULL,NULL,3,0,0,'<div id=\"offer-template-0\"></div><div><img alt=\"20160116_143800_120\" src=\"uploads/alibaba/924f/924fa18ed5c2f060b1b441d833496dc8.jpg\" /><br /><br /><br /><br /><br /><img alt=\"20160116_143800_124\" src=\"uploads/alibaba/63f9/63f9bd7364fe995a4ba7d0f181d9bc4a.jpg\" /><br /><img alt=\"20160116_143800_125\" src=\"uploads/alibaba/ca88/ca885086a13be186a8907c790916fcee.jpg\" /><br /><img alt=\"20160116_143800_127\" src=\"uploads/alibaba/1e12/1e1279ade85ba3036b9bccf41d4aa5a6.jpg\" /><br /><img  src=\"uploads/alibaba/4555/45556ea81aec268a9dc8c928a6e2172b.jpg\" /><br /><img  src=\"uploads/alibaba/8d0e/8d0e8839006c666969f71388933de407.jpg\" /><br /><img  src=\"uploads/alibaba/6b49/6b4977d5c95ecaab4695ce9065c0304b.jpg\" /><br /><img  src=\"uploads/alibaba/422a/422acd2e70ef61aa69b3e94d5c846bea.jpg\" /><br /><img  src=\"uploads/alibaba/b39c/b39ca0c4bccbbfde5679d9e7e2d47ce3.jpg\" /><br /><img  src=\"uploads/alibaba/c281/c281ab2926f19186e8a485913b1556ad.jpg\" /><br /><img  src=\"uploads/alibaba/eda0/eda04d1ebf8ac9ef4aaa2de4f7b31afe.jpg\" /><br /><img  src=\"uploads/alibaba/7080/70802a1f511268b833bcb2abae2ed57a.jpg\" /><br /><img  src=\"uploads/alibaba/84c4/84c4fbf3d16e98ac4c91af683ce85588.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/4dcc/4dcc03f1816f9e7eba6ac14500763341.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/7a81/7a817495f1f36885c2aeb06eef3fe39c.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/4afd/4afd9956349a3dfdf96421183f76a532.jpg\" width=\"790\" /><br /><p>&nbsp;</p><p><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p>&nbsp;<span >&nbsp;&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p><span>&nbsp; &nbsp;</span></p></div><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,3.00,3.60,'uploads/alibaba/a723/a7230388d52f35fbad64fdc9026bd34d.jpg',0,0),(39,'JHYPJZ600','劲豪一品浓缩鸡汁600g 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发价',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><p><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/bfbe/bfbe88f0a707576c9302079afc9050b4.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/4849/4849e1bef94331a167e3a1ad358befa6.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/ed34/ed343430175cc710cbb956b69e4f830b.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/9bba/9bba9a1bb879a716070387c8c23d5d04.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/f146/f1466210745ddeac77e9c0055699bac9.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/32bb/32bbaaf9bb1dd895b627040d4b8caa7e.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/bb5d/bb5d0f4ed83e1938542d365c622eeedb.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" height=\"573\" src=\"uploads/alibaba/324a/324a92a6e367214a6065719ca8cdec17.jpg\" width=\"790\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" src=\"uploads/alibaba/026b/026bc0dae1d68a1fdf7ac0fda92a71ef.jpg\" /><br /><img alt=\"劲豪一品浓缩鸡汁600g/1kg 增鲜去味浓缩鸡汁调料 浓缩鸡汁批发\" height=\"682\" src=\"uploads/alibaba/7691/76911c268ca8911af69ac3cf0bd5c4ed.jpg\" width=\"790\" /></p><p><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,16.00,19.20,'uploads/alibaba/5597/5597e021bac184a2d2c0f20691b35924.jpg',0,0),(41,'6946269600220','御豪浓缩鸡汁280g 餐饮调料鸡汁 黄焖鸡米饭酱料调味品厂家批发价',NULL,NULL,8,0,2,'<div id=\"offer-template-0\"></div><div><span >御豪浓缩鸡汁，新推出小包装产品，欢迎各大客户咨询购买，所有产品质量保证，按需生产，生产日期</span></div><p><span >都是最近的，放心选购</span></p><h2><span ><strong>单笔订单满1000元，免快递运费，送50元优惠券（订单满51元便可用）</strong></span></h2><p><img  src=\"uploads/alibaba/9cbc/9cbce11a13bf481ddd636f867ef33be8.jpg\" /><br /><img  src=\"uploads/alibaba/26ac/26ac63ad7cd306e6f1ec562c087bc59d.jpg\" /><br /><img  src=\"uploads/alibaba/e1cf/e1cfe4e98edf842e2e7466c5787772d3.jpg\" /><br /><img  src=\"uploads/alibaba/8d03/8d032aca88677e7d0961833dac70eac7.jpg\" /><br /><img  src=\"uploads/alibaba/8f49/8f4976c09167e07d4f7eeca15b8cde4e.jpg\" /><br /><img  src=\"uploads/alibaba/9f35/9f35b638034d91408cafe2cd91713f46.jpg\" /><br /><img  src=\"uploads/alibaba/c9b4/c9b426843d18e75b7d41133894953bd2.jpg\" /><br /><img  src=\"uploads/alibaba/6cde/6cde8be9f1d3abca0b6b8c6ab89a6924.jpg\" /><br /><img  src=\"uploads/alibaba/0639/063953958c0d5baa12aee18a82b96088.jpg\" /><br /><img  src=\"uploads/alibaba/7caf/7cafce4370e0c5b975aa496edbe44249.jpg\" /><br /><br /><img  height=\"682\" src=\"uploads/alibaba/ac13/ac134343ad76da01d53286a5425e3c8d.jpg\" width=\"790\" /></p><p><br /><br /></p><p><img  src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img  src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></p><p><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></p><p><br /><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></p></td></tr></table><p><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img  src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,10.00,20.20,'uploads/alibaba/a1ab/a1ab8be048d3a3a07b5530de860e1770.jpg',0,0),(42,'YHHWBK908','厂家批发炸鸡面包糠 御豪好味908g面包糠 白色面包糠 袋装面包糠',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><p><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/b02a/b02af3db6992c69c80810d4504509e80.jpg\" /><br /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/e40e/e40e5ca6d2dd8da26a1cb64cb9078320.jpg\" /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/66d2/66d2ff83188c0be83cab8bfac5e940c3.jpg\" /><br /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/8c61/8c61af6e76c72ad030a2f33cc03dad7e.jpg\" /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/a0e7/a0e748a38229f00ed4a66f4dde83c15b.jpg\" /><br /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/f097/f09726cc2ad6e5914db5278f2c6b2568.jpg\" /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" height=\"573\" src=\"uploads/alibaba/142c/142c8b677dd4c03858fb45a5d85fa178.jpg\" width=\"790\" /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/3dac/3dac584b21ed4b24c7c63677f8d551a6.jpg\" /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" height=\"682\" src=\"uploads/alibaba/8405/840548973c9911042305c16792c21e41.jpg\" width=\"790\" /></p><p><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp;<span >&nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"御豪好味面包糠908g 多用途酥炸裹粉白面包糠 御豪面包糠批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,12.00,14.40,'uploads/alibaba/758e/758eee92dc174602ad5948100e457838.jpg',0,0),(43,'6939571930017','御豪汉醋杂粮香醋500ml 五谷杂粮传统古法酿造醋调味品 礼品醋',NULL,NULL,2,0,1,'<div id=\"offer-template-0\"></div><p>&nbsp;<img  src=\"uploads/alibaba/8d4e/8d4ea568dafd64717104ced84802b40d.jpg\" /><br /><br /><img  src=\"uploads/alibaba/073b/073b3b68ac78e0b0ec5d5eb1029f8f88.jpg\" /><br /><br /><img  src=\"uploads/alibaba/9f7e/9f7ed953041b6ba001b50b8cc55b8189.jpg\" /><br /><br /><img  src=\"uploads/alibaba/39e8/39e8dd9fc6c160c5cfe32a79cda1be03.jpg\" /><br /><br /><img  src=\"uploads/alibaba/6c68/6c685dd5b5d56bdd01124e3ba1c3cc3e.jpg\" /><br /><br /><img  src=\"uploads/alibaba/57fa/57fa81e84f3824e6a0b48c93e61b9872.jpg\" /><br /><br /><img  src=\"uploads/alibaba/a9bc/a9bcd43765cf89ba60eb428d8da625c9.jpg\" /><br /><br /><img  src=\"uploads/alibaba/b4e3/b4e3f37d7c3aeae9535de17190046e75.jpg\" /><br /><br /><img  src=\"uploads/alibaba/541c/541cbd3cd55536e1175fbde287595604.jpg\" /><br /><br /><img  src=\"uploads/alibaba/c11c/c11cae5f1c95083bece732fd03b7d013.jpg\" /><br /><br /><img  src=\"uploads/alibaba/6812/6812bcaab82f3e6c9d642451c2a2403c.jpg\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><span ><img  src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /></span><br /><span ><img  src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></span></p><p><span >&nbsp;</span></p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><span ><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></span></p><p><span ><strong><img  src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></span></p><p><span >&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></span></p><p><span ><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></span></p><p><span ><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></span></p><p><span ><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></span></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,38.00,40.66,'uploads/alibaba/a71d/a71dd7315f9d85c9bac1388f5946e395.jpg',0,0),(44,'YHJZ1000','御豪浓缩鸡汁1000g 调味品精选土鸡醇香原味调味香料 黄焖鸡调料',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><p><span >浓缩鸡汁复合调味料是富含18种氨基酸及各种天然香辛料的复合调味料，能为各种菜肴带来醇厚的口感及十足的鲜味，同时能够掩盖肉类原材料中的腥膻异味，让菜品口感更加稳定，味道平衡，增强增食欲，是专业级厨师调味品.</span></p><p><span >厂家直营，批发劲豪浓缩鸡汁、调味品精选土鸡醇香原味浓缩鸡汁；欢迎代理加盟！量大从优！<img  src=\"uploads/alibaba/385d/385d5d0de098d20092dffa8a0325f96d.jpg\" /><br /><img  src=\"uploads/alibaba/4a36/4a36b928d294d8f1601c9a941a09c694.jpg\" /><br /><img  src=\"uploads/alibaba/d10f/d10f059a480d3e16ee56524fbc169167.jpg\" /><br /><img  src=\"uploads/alibaba/e800/e80089d4de08d7c76232aa6340246385.jpg\" /><br /><img  src=\"uploads/alibaba/f0a2/f0a2d908d037a5c146c5f933b3192d65.jpg\" /><br /><img  src=\"uploads/alibaba/5d2d/5d2d6a79959ae135d8bfa701d5d42057.jpg\" /><br /><img  src=\"uploads/alibaba/94b1/94b1a32c5861530164807f8aa3341fc8.jpg\" /><br /><img  src=\"uploads/alibaba/08f8/08f8cc164fd52b12b081fc2458352aca.jpg\" /><br /><img  src=\"uploads/alibaba/d80c/d80cd63b243c5b628066e3aeecaf5b15.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/1169/116991a56a241e05efc6eafd494e303e.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/c9f6/c9f60f798684f96c721b38c2eb1dcf2e.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/1aca/1acacf91750d5de9fb57c8e2821fd1ce.jpg\" width=\"790\" /><br /><br /></span></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,42.00,44.94,'uploads/alibaba/8ad5/8ad56dd1e35a0b5468f05aebda207739.jpg',0,0),(45,'6946269601111','御豪白面包糠1kg炸鸡裹粉肯德基炸鸡粉面包屑餐饮调料面包糠系列',NULL,NULL,7,0,2,'<div id=\"offer-template-0\"></div><p><img  src=\"uploads/alibaba/6bb2/6bb25adbb29faffc88677144fe0b0f1d.jpg\" /><br /><img  src=\"uploads/alibaba/9bca/9bcacfcab81bfdddd7f084d059b2cf2c.jpg\" /><br /><img  src=\"uploads/alibaba/94a6/94a61f2946631540534fcd70a0f21d23.jpg\" /><br /><img  src=\"uploads/alibaba/4771/4771d3448e23f8f15747582012e9b3ff.jpg\" /><br /><img  src=\"uploads/alibaba/2519/251943eb71f0554faa94412e2d7c8684.jpg\" /><br /><img  src=\"uploads/alibaba/8336/83360dfb273f5379b13e1be0b8355cbb.jpg\" /><br /><img  src=\"uploads/alibaba/6d12/6d12ba8283ec47181e364cf04956f373.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/c874/c874e621ef378b349584c202ba2528c8.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/8ee8/8ee80c258b75575a5050bd68e5a3c010.jpg\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,12.00,14.40,'uploads/alibaba/1599/1599221ab40afdcfe4cae1ea7e6742c0.jpg',0,0),(47,'6946269600135','御豪烧烤味料454g 烧烤料方便面小龙虾调料调味料 调味品厂家批发',NULL,NULL,3,0,3,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/098b/098bd91a0b1aa651b57baa16d74693f2.jpg\" /><br /><img  src=\"uploads/alibaba/ddf7/ddf7d491ec21b5432720422bfdf4beec.jpg\" /><br /><img  src=\"uploads/alibaba/fd7c/fd7cc105bce6a04b867d4a3cabf5eecd.jpg\" /><br /><img  src=\"uploads/alibaba/491c/491c0ded3444fefb82075c51b5ce6433.jpg\" /><br /><img  src=\"uploads/alibaba/7bb3/7bb34ca6f223136b9493d7f9a0fb7933.jpg\" /><br /><img  src=\"uploads/alibaba/6482/6482ad58e4796317db573d596f67fbd9.jpg\" /><br /><img  src=\"uploads/alibaba/5bf6/5bf6baf48015fd2dff7aa6c38748794d.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/8b20/8b20b5789d6d4782f2a3f3d6f0a75923.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/4908/490847a55f5f10c61b15574ac382f04d.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/86e6/86e6497e84c54731d761aab26b474227.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/d108/d108654f933ed961140130a9f75c1760.jpg\" /></p><p><img  src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img  src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp; &nbsp;<span >&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img  src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,15.00,18.00,'uploads/alibaba/7cea/7cea9dbfc5adbfa4de900156effd11ac.jpg',0,0),(48,'JHRLHSF300','翻糖蛋糕烘焙色素 劲豪日落黄食品水溶染色剂 食用染料 色素 批发',NULL,NULL,4,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/df17/df17bacac5b6de44075e20c5e26fbc94.jpg\" /><br /><img  src=\"uploads/alibaba/3bc8/3bc82d533a1b861a2b22399f7129a7d3.jpg\" /><br /><img  src=\"uploads/alibaba/4177/4177a556cd9d8f676106887aa9b1b98b.jpg\" /><br /><img  src=\"uploads/alibaba/66c3/66c3a7d986205ffb61042ea73ac34aa9.jpg\" /><br /><img  src=\"uploads/alibaba/2365/2365cd16226b1d1f1aef1f64febc663b.jpg\" /><br /><img  src=\"uploads/alibaba/36a1/36a14cb2d6b20eaed8a52be68d9ededc.jpg\" /><br /><img  src=\"uploads/alibaba/32ad/32adf5e9cf302059c0fac04af2c68e4e.jpg\" /><br /><br /><img  height=\"682\" src=\"uploads/alibaba/590d/590d83018eac65ea6713875909a5e044.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/5def/5defcb0231a65715126218a448b879bc.jpg\" /><br /><br /><img  src=\"uploads/alibaba/039b/039b49d5eae4c6256ccfee7e33b7d739.jpg\" /><br /><img  src=\"uploads/alibaba/79df/79df84e91a064235340b3bd92725d9eb.jpg\" /><br /><img  height=\"592\" src=\"uploads/alibaba/f262/f2624c32fb37a76f1ce7303c41b83785.jpg\" width=\"790\" /><br /><img  height=\"573\" src=\"uploads/alibaba/cb25/cb25e8d60732430995a8f02cbf7c20f4.jpg\" width=\"790\" /><br /><img  height=\"682\" src=\"uploads/alibaba/acea/aceac4c4ab9bd7d48478ed4d0eca972d.jpg\" width=\"790\" /><br /><br /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,13.00,15.60,'uploads/alibaba/7c69/7c69a1aaeda7c0c058819895c93309f4.jpg',0,0),(49,'6939571930024','御豪汉醋赞阳双头陈醋500ml 古法手工酿造礼品醋 批发调味礼品醋',NULL,NULL,2,0,0,'<div id=\"offer-template-0\"></div><div>&nbsp;</div><p><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/be2d/be2dc55b504519ba43ca155a103bfdeb.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/84a6/84a65770c8d8e12e5b0343d8c1956a5d.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/bf17/bf17ec83a22a473c1310e2b51f99cf97.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/e1e6/e1e672bc01c37464d16eb7a27b697cad.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/7b35/7b35a23bbde27f88a49b9dde05498343.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/392f/392f8997661c02cbe2d84281b29b9170.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/27f0/27f0a4fd8008c4e8819ba6f776c41e5b.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/644a/644a26011fe95baa82e4ddf73caeb818.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/28ee/28eeb84eba290bc904f13efcfbf8959f.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/d53f/d53fda1daaa3ceae087f175b1a9b3a78.jpg\" /><br /><br /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p><p><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /></p><p><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp;<span >&nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"御豪汉醋赞阳双头陈醋500ml 古法手工固体发酵酿造陈醋 陈醋批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,38.00,40.66,'uploads/alibaba/c385/c385fb40551664fb854a307e10ccefaf.jpg',0,0),(50,'JHXWZ800','劲豪鲜味汁800ml 酒店提鲜专用调味品酱油 餐饮调料厂家批发代理',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/fd49/fd49a4cc3787e455397acf52c4c57b9c.jpg\" /><br /><img  src=\"uploads/alibaba/8dfd/8dfdda56f23b30d972313e5f06552e28.jpg\" /><br /><img  src=\"uploads/alibaba/ad5e/ad5ef717077f1c9264a65173f7e955fd.jpg\" /><br /><img  src=\"uploads/alibaba/37ce/37cece9eeb715b4dc7dba48a96dd49cc.jpg\" /><br /><img  src=\"uploads/alibaba/25d4/25d48b348d3799ac17e02d7f6977d28c.jpg\" /><br /><img  src=\"uploads/alibaba/b0a2/b0a2e9bc257e59325f007019c120e680.jpg\" /><br /><br /><img  height=\"1835\" src=\"uploads/alibaba/9e99/9e997492d2410e2d1c46073a5007083f.jpg\" width=\"790\" /><br /><br /><img  height=\"573\" src=\"uploads/alibaba/f4a0/f4a0ae8ac2e5b7dc3703198bf5bd4018.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/7ed4/7ed430cc287d796c31c151de580f6b76.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/9e75/9e7519b2f30fda8042d8fb1d96946bae.jpg\" width=\"790\" /></p><p><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span >&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p>&nbsp; &nbsp; &nbsp;</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,38.00,40.66,'uploads/alibaba/ac10/ac10363305eb31b2affaa14e9eb7bfee.jpg',0,0),(51,'6946269601098','御豪扎菲瑞黄糠1kg 油炸鸡腿炸鸡柳鸡翅膨化糕粉面包糠 一件代发',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><p><span >面包糠是一种广泛使用的食品添加附料，用于油炸食品表面，如：炸鸡肉、鱼肉、海产品（虾）、鸡腿、鸡翼、洋葱圈等。其味香酥脆软、可口鲜美、营养丰富。批发扎菲黄色面包屑面包糠1000G&nbsp; 优质发酵型面包屑/黄糠，欢迎加盟代理！量大从优！</span><br /><img  src=\"uploads/alibaba/7808/7808fc2190ce679a78400b92cf1b7dd6.jpg\" /><br /><img  src=\"uploads/alibaba/e3c2/e3c252089601a9a5910d243cba3d38e8.jpg\" /><br /><img  src=\"uploads/alibaba/e58f/e58fb0e1e989467ebf832439a5ae0d84.jpg\" /><br /><img  src=\"uploads/alibaba/6fca/6fcaa786cd4ec6b489703e45798a50b0.jpg\" /><br /><img  src=\"uploads/alibaba/be39/be397b69ee786cf1a0da8ec391397508.jpg\" /><br /><img  src=\"uploads/alibaba/c999/c999032ef8344ce55e79349ea6e5a3ea.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/fd35/fd353c420326921927382128c2c7c0fb.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/18bb/18bb26fcb763a133549e0dc48aaa849d.jpg\" /></p><p>&nbsp;</p><p><img  src=\"uploads/alibaba/ab01/ab013fe6e31d3faad54b494226907d7d.jpg\" /><br /><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span >&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p>&nbsp; &nbsp; &nbsp;</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >前海味趣，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,13.00,15.60,'uploads/alibaba/95d5/95d59f9f061b4f0db78c1b07aada3306.jpg',0,0),(52,'6946269600109','御豪家庭袋装白胡椒粉454克 餐饮调料香辛料烧烤调味料调味品厂家',NULL,NULL,3,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/a662/a66260eb3861355442532cf84e92e790.jpg\" /><br /><img  src=\"uploads/alibaba/3a10/3a10fc434aaa5077bc49ddb1c7dee312.jpg\" /><br /><img  src=\"uploads/alibaba/34b7/34b7a2ce4c81d206e0385298b4679fa2.jpg\" /><br /><img  src=\"uploads/alibaba/5ef1/5ef176220a55e7037ea7cf2fb27837ac.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/e4b7/e4b754643973606f82b0a42a4e62abae.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/2c59/2c596f58cdb9c336bb6d93bb39e6b0b3.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/8dc3/8dc32148010c2559712e4d0a99cb43c2.jpg\" width=\"790\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,10.00,20.20,'uploads/alibaba/a6f4/a6f44acc074b8a58d418d53c4d19e6e6.jpg',0,0),(53,'JHYZHSF300','劲豪胭脂虫红食用色粉300g 饮料色素翻糖蛋糕色素食品添加剂批发',NULL,NULL,4,0,0,'<div id=\"offer-template-0\"></div><div><span >胭脂虫红实质就是<span >胭脂红</span>，只是有的地方习惯叫胭脂红为胭脂虫红</span><br /><img  src=\"uploads/alibaba/7608/76087a5c153595d40e88c229e1cf0594.jpg\" /><br /><img  src=\"uploads/alibaba/3653/36537e5fee2b473c9fda30de4e93d017.jpg\" /><br /><img  src=\"uploads/alibaba/3577/35779bda9eb366b9da05607acc212066.jpg\" /><br /><img  src=\"uploads/alibaba/f73f/f73f4f7ebf9850cd8aef73069ebc48c3.jpg\" /><br /><img  src=\"uploads/alibaba/388f/388f7b4594738329dc14e52b7c5746d3.jpg\" /><br /><img  src=\"uploads/alibaba/6833/68330e28a24f1df438c2389cdda19161.jpg\" /><br /><img  src=\"uploads/alibaba/e336/e33676692ac34414bdb63bb8051fe2bd.jpg\" /><br /><img  src=\"uploads/alibaba/3510/3510a9cd56960e05138c73953286ba57.jpg\" /><br /><img  src=\"uploads/alibaba/2043/2043d20c5cdc69311e92eabd670ec5a5.jpg\" /><br /><img  src=\"uploads/alibaba/910b/910b49027e994f02263b16e3c23792d8.jpg\" /><br /><br /><br /></div><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,13.00,15.60,'uploads/alibaba/a46c/a46c898531a8f59287b0371814c5cacf.jpg',0,0),(54,'6946269601043','御豪酸辣饺子蘸酱500g 手抓饼蘸料捞面拌饭调料餐饮调料调味品',NULL,NULL,8,0,2,'<div id=\"offer-template-0\"></div><p><img  src=\"uploads/alibaba/8292/8292020611cfb449f4f58f452265c1d0.jpg\" /><br /><img  src=\"uploads/alibaba/6bb6/6bb6c0fb5d9a4d227f542c2031e5ab9e.jpg\" /><br /><img  src=\"uploads/alibaba/e829/e829edc1ef36d3dfd5eeaacd6819d0bd.jpg\" /><br /><img  src=\"uploads/alibaba/f4d2/f4d2998274f021c784858b4029de1a52.jpg\" /><br /><img  src=\"uploads/alibaba/adf8/adf8b45d7c78e0eb3c8dad3e857c8e7e.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/4916/49162ca273c526c09b96d555b68675ee.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/9035/9035e65ed1553452fa837db1eeaaa3c4.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/d5fe/d5fe7b8ae2fefb472b1b9ccf3f9f4227.jpg\" width=\"790\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,12.00,14.40,'uploads/alibaba/1aa5/1aa56216520eac599daf21a275d79944.jpg',0,0),(55,'YHXWZ800','御豪鲜味汁800ml 增鲜酱油 调味料 餐饮调料厂家直销批发调味品',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><div><span ><strong>厂家直营，批发御豪鲜味汁800ml调味酱，欢迎代理加盟！鲜味汁是一种新型的调味料</strong></span></div><div><span ><strong>有降盐增鲜的作用。</strong></span></div><div><div><span ><strong>配料：酵母抽提物、猪肉、食品添加剂（增味剂）</strong><strong>、增稠剂、食用香精、脱氢乙酸钠）、麦芽糊精、酿造酱油、猪脂肪油、香辛料</strong></span></div><div><div><span ><strong>用于烹饪、在烧菜、炒菜、煲汤、煮面条、火锅、调馅等制作时均可使用。特点是耐高温、耐沸煮。</strong></span></div><div><strong><strong><span><strong>鲜味汁适用于炒、沾、凉拌等烹调方式，轻洒几滴，不必再加酱油或味精，就可提升食物的原味，让菜肴更可口好吃</strong></span><img  src=\"uploads/alibaba/e8fe/e8fedbccd981d1635ab2563c4d19e312.jpg\" /><br /><img  src=\"uploads/alibaba/099b/099b19cc234a2ce345423a58f4ac5c29.jpg\" /><br /><img  src=\"uploads/alibaba/ac0d/ac0dc18b001788261f44a1ddea594456.jpg\" /><br /><img  src=\"uploads/alibaba/f504/f504c0a4ec5135f42daba2fd1fd877e0.jpg\" /><br /><img  src=\"uploads/alibaba/cc0a/cc0aa74bb23d41c5bf374da7f8d01a2b.jpg\" /><br /><img  src=\"uploads/alibaba/9211/9211f9c14b220c1fec0a09cb1711d412.jpg\" /><br /><img  src=\"uploads/alibaba/b27a/b27a5838add714292afd19c9b7514712.jpg\" /><br /><img  src=\"uploads/alibaba/6806/68069a70f8a7b6dd896afc3cd5884655.jpg\" /><br /></strong></strong><p>&nbsp;</p><p><img  src=\"uploads/alibaba/ab01/ab013fe6e31d3faad54b494226907d7d.jpg\" /><br /><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p>&nbsp; &nbsp;<span >&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p></div></div><div>&nbsp;</div></div><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,26.00,31.20,'uploads/alibaba/d6b5/d6b5173424b0ebcef9c05cf2c10082e2.jpg',0,0),(56,'YHLXL380','御豪辣鲜露380g 麻辣火锅调味料 餐饮调料调味品厂家批发价 代理',NULL,NULL,3,0,0,'<div id=\"offer-template-0\"></div><p><br /><img  src=\"uploads/alibaba/7eda/7eda0dfde8d29ba67b0d59b149dd8239.jpg\" /><br /><img  src=\"uploads/alibaba/9fbc/9fbcc4b086a1e9972113accbe2b6d01d.jpg\" /><br /><img  src=\"uploads/alibaba/63d7/63d7610cc7d693f190aa656b7c6a4c75.jpg\" /><br /><img  src=\"uploads/alibaba/660e/660ecd47b7fc62270d4ecd6b602710af.jpg\" /><br /><img  src=\"uploads/alibaba/39ac/39acc3441efe5f498d5dc35ab33ffa7d.jpg\" /><br /><img  src=\"uploads/alibaba/fbd1/fbd13e60e21101ebd6086bbace1b8720.jpg\" /><br /><img  src=\"uploads/alibaba/8d86/8d863144b313598ba0214dd4c0d3f552.jpg\" /><br /><img  src=\"uploads/alibaba/a067/a0678309aa606cc192390515d7208949.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/2690/269095f2a7420a181e3e63ef087ac25f.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/9127/91277720a06e9b9adca266cf2d8c1b48.jpg\" /><br /><img  height=\"682\" src=\"uploads/alibaba/7be7/7be71aeb41a14c8f1dcdc6aac579e9af.jpg\" width=\"790\" /><br /><img  height=\"592\" src=\"uploads/alibaba/f432/f432b6a5011671b6e135a1d816bd0923.jpg\" width=\"790\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img  src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img  src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><strong>&nbsp; &nbsp;<span >&nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</span></strong><span ><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"模板-2_05\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img  src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,17.00,20.40,'uploads/alibaba/9c51/9c5141f97e09732042b0ea14cab5a94d.jpg',0,0),(57,'6946269601395','厂家批发 日式调料寿司火锅鱼生酱油 豆捞酱油寿司酱油 海鲜调料',NULL,NULL,1,0,0,'<div id=\"offer-template-0\"></div><p>&nbsp;</p><p><img  src=\"uploads/alibaba/a314/a31412cb9d4426dbdee72ebfca0546cf.jpg\" /><br /><img  src=\"uploads/alibaba/8dd0/8dd098ea8dccb5ef8b078aee52b7309c.jpg\" /><br /><img  src=\"uploads/alibaba/3954/39541a5606b4b39c313ba488ddc44fcb.jpg\" /><br /><img  src=\"uploads/alibaba/de30/de3061303fce2f02ad65daa0b494c550.jpg\" /><br /><img  src=\"uploads/alibaba/a899/a899f0c77aca94bf937375bf43a7995e.jpg\" /><br /><img  src=\"uploads/alibaba/bfb0/bfb0cd7633a3c4a949261f8785258a17.jpg\" /><br /><img  src=\"uploads/alibaba/30f5/30f52d59bf6375efcf6ae90eca1f46f2.jpg\" /><br /><img  src=\"uploads/alibaba/b3c1/b3c19a32569929b643f8de6f2110ec61.jpg\" /><br /><br /><img  src=\"uploads/alibaba/a41c/a41cbf184e0a3d317aa7a2cb17416f26.jpg\" /><br /><img  src=\"uploads/alibaba/eb06/eb0672368fd453cdd045683261539589.jpg\" /><br /><img  src=\"uploads/alibaba/fe99/fe99a653ed7b8c288bc870341ff503ff.jpg\" /><br /><img  src=\"uploads/alibaba/a223/a223eb5037e40dd4a38accc47ffcc636.jpg\" /><br /><img  src=\"uploads/alibaba/003e/003e0d6d210132bcda26bb19cc4cac4e.jpg\" /></p><div><span >酱油俗称<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">豉油</a>，主要由<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">大豆</a>、<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">淀粉</a>、<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">小麦</a>、<a href=\"https://detail.1688.com/offer/523957465014.html?spm=a2615.7691456.0.0.YFZ9Ux\" target=\"_blank\">食盐</a>经过制油、<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">发酵</a>等程序酿制而成的。酱油的成分比较复杂，除食盐的成分外，还有多种<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">氨基酸</a>、糖类、有机酸、色素及香料等成分。以咸味为主，亦有鲜味、香味等。它能增加和改善<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">菜肴</a>的口味，还能增添或改变菜肴的色泽。我国人民在数千年前就已经掌握<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">酿制</a>工艺了。酱油一般有<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">老抽</a>和<a href=\"http://955556.cn.alibaba.com/\" target=\"_blank\">生抽</a>两种：生抽较咸，用于提鲜；老抽较淡，用于提色。</span></div><p><span >厂家直营，批发酱油、澳门鱼生寿司海鲜火锅豆捞酱油、 御豪寿司豆捞酱油&nbsp;&nbsp;<img src=\"uploads/alibaba/87e4/87e47a013770832aaac7788a3a4a0b5b.gif\" />本产品为公司特级酿造酱油！公司为推广本澳门豆捞酱油特推出零利润促销！<img src=\"uploads/alibaba/30da/30da4a5333b3671f51b6cd0ccc4f8d94.gif\" />，，亲们不要再议价哦！欢迎代理加盟及天猫，淘宝分销。</span><br /><br /><img  height=\"573.8933030646992\" src=\"uploads/alibaba/9993/9993e7627a786d5c65abacbc2b1b08bb.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/624b/624bfcc3e8f5ddb62a9ed66e88962e01.jpg\" /><br /><img  height=\"682.7263157894737\" src=\"uploads/alibaba/243b/243b279651b77bb5f3aa8913597a8ab6.jpg\" width=\"790\" /><br /><br /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,6.00,7.20,'uploads/alibaba/2f76/2f76877c9079d7189eec40e7532c2b9d.jpg',0,0),(58,'6946269600190','御豪鸡汁酒店 广东菜肴调味浓缩鲜鸡汁1kg 黄焖鸡酱料 餐饮调料价',NULL,NULL,8,0,0,'<div id=\"offer-template-0\"></div><div><span >御豪浓缩鸡汁是富含18种氨基酸及各种天然香辛料的复合调味料，能为各种菜肴带来醇厚的口感及十足的鲜味，同时能够掩盖肉类原材料中的腥膻异味，让菜品口感更加稳定，味道平衡，增强增食欲，是专业级厨师调味品.御豪鸡汁是精选农家文昌鸡经科学工艺提取后（采用现代生物酶解抽提技术，将鸡肉中的全部营养和风味物质全部抽提出来）精制而成的浓缩调味汁，鸡鲜味十足、口感自然,适合使用煎、炒、焖、蒸、煮等多种菜式的的烹调。是第4代的最新鲜调味品.</span></div><p><span >&nbsp;<img  src=\"uploads/alibaba/7371/73719cdb7acb6df88c73b5f5c06a1e48.jpg\" /><br /><img  src=\"uploads/alibaba/1205/120549fec68054fea25811e49a5a144c.jpg\" /><br /><img  src=\"uploads/alibaba/c965/c965b4a45a0bdaeb567a4ebf390b5dc8.jpg\" /><br /><img  src=\"uploads/alibaba/86b5/86b59877043e8f059484ef618655c2de.jpg\" /><br /><img  src=\"uploads/alibaba/9fd5/9fd59f469a9c1f821ba4908cb3d4176b.jpg\" /><br /><img  src=\"uploads/alibaba/95fa/95fac0b105e1148b4790a1480d7fd517.jpg\" /><br /><img  src=\"uploads/alibaba/ab92/ab9280280bfd2d532983c62a21d7074c.jpg\" /><br /><img  src=\"uploads/alibaba/8ef9/8ef99e69bb1af6c46ece311d5ae95471.jpg\" /><br /><img  src=\"uploads/alibaba/1f4f/1f4fb7e4ef5dfb1dbbc2f5e3efeb1acd.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/f7d7/f7d7cbad1981c616f05493410cbacb41.jpg\" width=\"790\" /><br /><img  src=\"uploads/alibaba/8caf/8cafe98cc3a9966046361a5aab1477ec.jpg\" /><br /><br /><img alt=\"2605079093_444062630\" height=\"790\" src=\"uploads/alibaba/846d/846d869ba504dffe85817c27af4959da.jpg\" width=\"790\" /><br /><img alt=\"2605064931_444062630\" height=\"790\" src=\"uploads/alibaba/e78e/e78e92d13bee73138c5c97f73856af46.jpg\" width=\"790\" /><br /><img alt=\"2602279993_444062630\" height=\"790\" src=\"uploads/alibaba/ab0d/ab0deb30f9033fded4a389d58219c52e.jpg\" width=\"790\" /><br /><br /><br /></span></p><p><span >厂家直营，批发御豪浓缩鸡汁、调味品精选土鸡醇香原味浓缩鸡汁；欢迎代理加盟！量大从优！</span></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,38.00,40.66,'uploads/alibaba/ab0d/ab0deb30f9033fded4a389d58219c52e.jpg',0,0),(59,'YHZFRBK908','厂家批发劲豪白面包糠908g 肯德基炸鸡裹粉炸虾面包屑 烘焙原料',NULL,NULL,7,0,0,'<div id=\"offer-template-0\"></div><p><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" src=\"uploads/alibaba/0a80/0a80d490aea546c3f61abdc015261065.jpg\" /><br /><br /><br /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" src=\"uploads/alibaba/0ca3/0ca3a758a60334a247cc64fd3e3e380c.jpg\" /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" src=\"uploads/alibaba/253c/253cfe0273e4246a8d046166422ed28a.jpg\" /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" src=\"uploads/alibaba/6d06/6d0661e32fe099e87bcd888c95d61462.jpg\" /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" src=\"uploads/alibaba/732e/732e87cd90ff6235e8d15abbf7f6d3e3.jpg\" /></p><div><span >面包糠是一种广泛使用的食品添加附料，用于油炸食品表面，如：炸鸡肉、鱼肉、海产品（虾）、鸡腿、鸡翼、洋葱圈等。其味香酥脆软、可口鲜美、营养丰富。</span></div><p><span >&nbsp;</span></p><div><span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司所有产品是都是厂家直销，品质保证，欢迎代理加盟！</span></div><p><br /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" height=\"592\" src=\"uploads/alibaba/bfa8/bfa84255cb39066931cdf1b596b0c01b.jpg\" width=\"790\" /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" height=\"573\" src=\"uploads/alibaba/90af/90af21bd7f29eb33c8a53775e63b1d61.jpg\" width=\"790\" /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" src=\"uploads/alibaba/2a8a/2a8a16ccf2e6475709e84957c945104c.jpg\" /><br /><img alt=\"劲豪白面包糠908g 劲豪多用途酥炸裹粉白面包糠 劲豪面包糠批发\" height=\"682\" src=\"uploads/alibaba/a741/a7415ad48f7624d79ee8b412326e6d45.jpg\" width=\"790\" /><br /><br /></p><p><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p><span>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,12.00,14.40,'uploads/alibaba/45ad/45addad819c514dd5e4679ce1267729d.jpg',0,0),(60,'6946269603122','厂家批发顶豪果绿色素300g 翻糖蛋糕食用色粉食品加工着色剂调料','',NULL,4,0,4,'&lt;div id=\\&quot;offer-template-0\\&quot;&gt;&lt;/div&gt;&lt;div&gt;&lt;span&gt;厂家直销、批发 食品级色素，烘焙蛋糕 棉花糖色素果绿食用色素,公司所有产品都是厂家直营，正品保证，欢迎加盟代理! 共有七种颜色可选&lt;/span&gt;&lt;/div&gt;&lt;p&gt;&lt;span&gt;[果绿][柠檬黄][橙黄][日落红][橙红][日落黄]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span&gt;本产品的作用，能促进人的食欲，增加消化液的分泌，因而有利于消化和吸收，是食品的重要感官指标。 本品应用范围可用于果汁、汽水、酒、糖果、糕点、果味粉、罐头、冷饮等食品的着色&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=\\&quot;uploads/alibaba/0db7/0db7a82d71a438c653c46d53d36ad01a.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/7c4d/7c4d35361c3f45c085f34e5518d5f6a9.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/1ced/1cedbd732279548e4eabb5edd8c4cc00.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/ffb3/ffb35d774e79d6b1938203ef6a990ef4.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/0863/0863e5dbf1e136daa989193c726ccbe7.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/56c6/56c6a9e9fc2afcd59425afdfc26cbbb5.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/51dd/51dd8410d920b9f335327ceb3bf33be5.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/318b/318b4279259e30490b60ceb08cf33b88.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/9a5d/9a5d37b11aa34af655510d8ebeaf5718.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img height=\\&quot;592\\&quot; src=\\&quot;uploads/alibaba/8266/82668753551ce6fa153be020e3c5dbb6.jpg\\&quot; width=\\&quot;790\\&quot;/&gt;&lt;br/&gt;&lt;img height=\\&quot;573\\&quot; src=\\&quot;uploads/alibaba/ae27/ae27fa8e94839d7e72b0e39d001604fa.jpg\\&quot; width=\\&quot;790\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&lt;strong&gt;味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&amp;amp;ldquo;味&amp;amp;rdquo;事业。&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; 联系人：小味&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; 联系电话：188 2581 9701&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&amp;quot;&lt;/p&gt;','',0,'249',1,0,0,0,'',0,0,0.00,0,'','','',0,'',0.00,15.00,18.00,'uploads/alibaba/e251/e251086d424a26f53aca3667052d613c.jpg',0,0),(61,'6939571900379','御豪黑豆原酿酿造长辈酱油200ml 黑豆酱油厂家批发餐饮调料调味品',NULL,NULL,1,0,0,'<div id=\"offer-template-0\"></div><p><img  src=\"uploads/alibaba/38bb/38bb5b08c19259b12fa6824a2cb6b744.jpg\" /><br /><br /><img  src=\"uploads/alibaba/7888/78886146b12920c36f3d46ace7ab9add.jpg\" /><br /><br /><img  src=\"uploads/alibaba/09f3/09f32f5c23e8370eb7dd4a491556d360.jpg\" /><br /><img  src=\"uploads/alibaba/1b97/1b97300c862cbfc753ab65802e784193.jpg\" /><br /><br /><img  src=\"uploads/alibaba/bd9d/bd9dfe291b6026ecc136342d0acdf15c.jpg\" /><br /><br /><img  src=\"uploads/alibaba/c162/c162c20cb746102753b1593e7e3613db.jpg\" /><br /><br /><img  src=\"uploads/alibaba/591e/591eb9ea6016ac17f9c7f4084f65a3eb.jpg\" /><br /><br /><img  src=\"uploads/alibaba/2502/250261ea4ff9a7c3dc488f6878281cf7.jpg\" /><br /><br /><img  src=\"uploads/alibaba/df4d/df4d2c65f056e3866194a88eeb4f5003.jpg\" /><br /><br /><img  src=\"uploads/alibaba/c4fe/c4fe2170c129820de19eea04a0c3221b.jpg\" /><br /><br /><img  src=\"uploads/alibaba/5e12/5e129460654a184c999ccb32d8208a57.jpg\" /><br /><br /><br /><img  src=\"uploads/alibaba/216f/216fe4c0a9349cff00a153f3f7921573.jpg\" /></p><p><img alt=\"拿货须知模板\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,128.00,130.56,'uploads/alibaba/c12c/c12c50a4e41e4a6ae16a3ad004ee5020.jpg',0,0),(62,'YHMYCSZ1800','御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调料 炭烧调味品厂家批发',NULL,NULL,8,0,1,'<div id=\"offer-template-0\"></div><div><br />&nbsp;</div><p><span ><strong>厂家直营批发御豪鳗鱼炭烧汁1800ml,</strong></span></p><p><span ><strong>&nbsp; &nbsp; &nbsp;</strong><strong>欢迎代理加盟！外焦内嫩&rdquo;是大多数烧烤食品的终极目</strong><strong>标；</strong><strong>炭火火焰的物理特性，奠定了其为烤鳗加热方式；</strong><strong>日本的鳗专卖料理店当然是用炭火；</strong><strong>有利必有弊，炭火是自然火，其火候的掌握是需要多年的经验；工业量化生产一直以来对炭火可望而不可</strong><strong>及。</strong></span></p><p><span ><strong>&nbsp;&nbsp;多年来经各方面共同的努力，炭火烧烤鳗量化生产总算得以实现，专业料理店口味进入寻常百姓家。因其加工过程各方面起点很高，所以炭火烧烤鳗不愧是蒲烧烤鳗。</strong></span></p><p><strong><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/1e9c/1e9c21064e335798e52c14097b5a5c7d.jpg\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/3d6c/3d6cdc5ae9d4ba1c5fc009880c4189df.jpg\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/e766/e766421b99dc8db4050200e4e25d608d.jpg\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" height=\"573\" src=\"uploads/alibaba/8fd3/8fd3c692d3786c66d099363adfa9628e.jpg\" width=\"790\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" height=\"901\" src=\"uploads/alibaba/0ac0/0ac0786638da584a7479b8b355a06db6.jpg\" width=\"790\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" height=\"682\" src=\"uploads/alibaba/18d0/18d051c5e948acaa3e9329b15ad14007.jpg\" width=\"790\" /><br /></strong></p><p><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\" /><br /><br /></p><p><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\" /><br /><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/2f11/2f11a252348d75380a0c4b4a7c0ecd9e.jpg\" /></p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" ><tr><td ><p><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用</strong><strong>户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</strong></span></p><p><br /><span ><strong>&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</strong></span></p></td></tr></table><p><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\" /></p><p><strong><img alt=\"御豪鳗鱼炭烧调味汁1.8L 烧鳗鱼风味炭烧调味汁 炭烧调味汁批发\" src=\"uploads/alibaba/13d8/13d8c11fae154de08cd74c09d7a88c45.jpg\" /></strong></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,35.00,37.45,'uploads/alibaba/fd4b/fd4bb5417692ba197924985cffa32a1a.jpg',0,0),(64,'6939571900393','调味品厂家批发 御豪黑豆酱油 礼品酱油 传统酿造酱油 食用酱油','',NULL,1,0,1,'&lt;div id=\\&quot;offer-template-0\\&quot;&gt;&lt;/div&gt;&lt;p&gt;&lt;img src=\\&quot;uploads/alibaba/08bc/08bc8323273b4e5317ea2ed5cd9cb577.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/6c2a/6c2a98df39fb762233ca7a61a45d574f.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/3e71/3e7166be5614a8ab2cfebe6b269c93c2.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/7078/7078c2b9637fb1cdece4ce2122c13f11.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/01c9/01c925fb18bf225bddf9d4d3fdde87af.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/0988/0988e53774be2057814b90886b9837c7.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/b9b4/b9b4c9468a418eca448ca5a48f5a6398.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/125e/125e0beab1970b3a9bc14713f06faa6c.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;img src=\\&quot;uploads/alibaba/e571/e57175de50122c2b1281089e3cc4adf7.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;img alt=\\&quot;拿货须知模板\\&quot; src=\\&quot;uploads/alibaba/afd9/afd9ca1f5dbe44c573517ed41cd4522a.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;img alt=\\&quot;海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\\&quot; src=\\&quot;uploads/alibaba/a00e/a00ec7d3184429b66481bead496c2199.jpg\\&quot;/&gt;&lt;br/&gt;&lt;img alt=\\&quot;海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\\&quot; src=\\&quot;uploads/alibaba/43fd/43fd14f72a9244f54192832f75e64ce5.jpg\\&quot;/&gt;&lt;br/&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;span&gt;御豪食品旗下实体机构&lt;/span&gt;&lt;br/&gt;&lt;img alt=\\&quot;海天yes黄豆酱2千克*6桶 海天特制黄豆酱调味料 海天黄豆酱批发\\&quot; src=\\&quot;uploads/alibaba/446f/446f5520f18e55717914497a528d1d37.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;strong&gt;诚招加盟、经销、批发商，一起发展&amp;amp;ldquo;味&amp;amp;rdquo;事业。&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; 联系人：小味&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; 联系电话：188 2581 9701&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&amp;quot;&lt;/p&gt;','',0,'249',1,0,0,0,'',0,0,0.00,0,'','','',0,'',0.00,168.00,171.36,'uploads/alibaba/d2ee/d2ee8070fd054f3643b88cae9b2adefa.jpg',0,0),(65,'6946269600701','御豪水晶鸡配料18g 酒店调味料蒸鸡配料调味品厂家批发调味品',NULL,NULL,3,0,0,'<div id=\"offer-template-0\"></div><div><span ><strong>厂家直销、批发御豪水晶鸡专用配料湛江隔水蒸鸡料 特色名菜,<img src=\"uploads/alibaba/30da/30da4a5333b3671f51b6cd0ccc4f8d94.gif\" />真心感谢亲们支持！<img src=\"uploads/alibaba/b0f7/b0f712ba90f856a3fb185a1d43ec2706.gif\" />欢迎代理加盟!</strong></span><img  src=\"uploads/alibaba/ef9b/ef9bba55a3c7f463825ee3ef8dacf592.jpg\" /><br /><br /><br /><br /><img  src=\"uploads/alibaba/e79f/e79fb4201e21837f78eef9666d948b23.jpg\" /><br /><img  src=\"uploads/alibaba/4614/4614e0d3183f28b8cbe986d2c9f21bdb.jpg\" /><br /><img  src=\"uploads/alibaba/3cd4/3cd42944d10c8e73656ef2be14bbe344.jpg\" /><br /><img  src=\"uploads/alibaba/86e0/86e022f5b69f2b0630187c02487a4e05.jpg\" /><br /><img  src=\"uploads/alibaba/a045/a045bd44967428b9b484016238cba58c.jpg\" /><br /><img  src=\"uploads/alibaba/5435/5435b1ce94a33e2cf72942a09906bd7e.jpg\" /><br /><img  src=\"uploads/alibaba/279e/279e59d5c0ea7e3d682bb808dc3fd1f3.jpg\" /><br /><img  src=\"uploads/alibaba/be41/be41badcec8e9e5d4571b5eebc5deca9.jpg\" /><br /><img  src=\"uploads/alibaba/e6f4/e6f449aadc1cdc90c7eda19f8523cd5e.jpg\" /><br /><img  src=\"uploads/alibaba/0ee3/0ee3a354437fd0d0ab2ca23cc8183d43.jpg\" /><br /><img  src=\"uploads/alibaba/51cd/51cd7ae1c4f03937a9d302ae461dec5e.jpg\" /><br /><img  src=\"uploads/alibaba/dac6/dac6b7b141759b310310cf896f0f87c8.jpg\" /><br /><img  height=\"573\" src=\"uploads/alibaba/776e/776e21c8bf0906effa5ce956a61e6275.jpg\" width=\"790\" /><p><img  src=\"uploads/alibaba/6c13/6c139bcfc781ea83209283f090a3c499.jpg\" /><br /><img  src=\"uploads/alibaba/5a60/5a607e3862e05a539ce31cce4e77ac47.jpg\" /></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table border=\"0\" cellpadding=\"0\" cellspacing=\"1\"><tr><td><p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<span >深圳市前海味趣网络科技有限公司（简称：味趣网络），专注于厨房健康食材的深耕与服务，满足用户日益增长的对&ldquo;味&rdquo;的需求与渴望，让人们放心用&ldquo;料&rdquo;、安心用&ldquo;料&rdquo;、省心用&ldquo;料&rdquo;</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; &nbsp;味趣网络自成立以来，本着三高（高品质、高效率、高服务）三低（低成本、低毛利、低价格）的经营理念以及对健康食材、对调味品业的深度理解与专致，发展迅速，已成功搭建PC+移动端+第三方线上平台、线下贸易、实体连锁体系以及大数据服务与分析系统；支撑线上线下的高效运转，并在东莞市细村市场、大岭山信立农批市场、东莞市南城区设立了分公司，体验店与大型仓库中心；立足珠三角，深耕广东，布局全国，现已放开加盟店合作条件。</span></p><p><br /><span >&nbsp; &nbsp; &nbsp; 用真诚的态度、用诚心的价格，用贴心的服务，味趣网络，值得您长期信赖的合作伙伴！</span></p><p>&nbsp;</p></td></tr></table><p><br /><img  src=\"uploads/alibaba/4f72/4f7231d8dd4210cfa1756c28826650a9.jpg\" /><br /><img  src=\"uploads/alibaba/40c9/40c91e449b9dba29fde78ac94552fc24.jpg\" /></p><p>&nbsp; &nbsp; &nbsp;</p></div><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong >味趣网络，专注于厨房健康食材的经营与服务,高品质、高效率、高服务、低成本、低毛利、低价格三高三低的经营理念。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 现公司处在高速发展的阶段，诚招加盟、经销、批发商，一起发展&ldquo;味&rdquo;事业。</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系人：小味</strong></p><p><strong >&nbsp; &nbsp; &nbsp; 联系电话：188 2581 9701</strong></p>\"',NULL,0,'249',1,0,0,0,NULL,0,0,0.00,0,'','','',0,'',0.00,2.50,4.00,'uploads/alibaba/0e0a/0e0a2e26c9867ce426d3577d77ffea27.jpg',0,0),(66,'0001269','【一件】新鲜大号鸡蛋','',NULL,9,0,29,'&lt;p&gt;&lt;img src=\\&quot;/uploads/alibaba/6416/6416c59c24785e82c67f0b4939be1500.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/83db/83db78f2610f61f511953f9ffa2c2620.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/0ca0/0ca0a6bb0af62ee0a31b8bbe689313cb.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/a6b6/a6b646500b7304c5e587601dbb864a1e.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/99fe/99fe8c8a98cf56fef436fd455a7d8651.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/3fa5/3fa51ad244780e906dbe24bf92ad47b7.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/1b51/1b5165b55d4a603bca5c7d12e98598ef.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/632e/632eddc0cdc09178497cf1d9d6576c97.jpg\\&quot;/&gt; &lt;img src=\\&quot;/uploads/alibaba/2f6c/2f6cecb52db803334a8398f2077df4ac.jpg\\&quot;/&gt;&lt;/p&gt;','',0,'0.00',1,0,0,0,'',0,0,0.00,0,'','','',0,'',0.00,145.00,170.00,'/uploads/default/1fd5/2895d/1fd59821aee8a8e23fb307e4aecc8ee3.jpg',0,0);
/*!40000 ALTER TABLE `products_info` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wechat_autoresponse`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wechat_autoresponse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `message` text,
  `rel` int(11) DEFAULT '0',
  `reltype` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wechat_autoresponse`
--

LOCK TABLES `wechat_autoresponse` WRITE;
/*!40000 ALTER TABLE `wechat_autoresponse` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wechat_autoresponse` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wechats`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wechats` (
  `wechat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wechat_name` varchar(32) NOT NULL COMMENT '公众号名称',
  `account` varchar(32) NOT NULL COMMENT '帐号',
  `original_account` varchar(32) NOT NULL COMMENT '原始帐号',
  `app_id` varchar(64) NOT NULL,
  `app_secret` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL COMMENT '验证token',
  `entry_hash` varchar(64) NOT NULL COMMENT '入口hash用于区别所属公众号',
  PRIMARY KEY (`wechat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信公众号表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wechats`
--

LOCK TABLES `wechats` WRITE;
/*!40000 ALTER TABLE `wechats` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wechats` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_banners`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(255) DEFAULT NULL,
  `banner_href` varchar(255) DEFAULT NULL,
  `banner_image` varchar(255) DEFAULT NULL,
  `banner_position` tinyint(4) DEFAULT '0',
  `reltype` tinyint(4) DEFAULT NULL,
  `relid` varchar(255) DEFAULT '0',
  `sort` tinyint(4) DEFAULT '0',
  `exp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_banners`
--

LOCK TABLES `wshop_banners` WRITE;
/*!40000 ALTER TABLE `wshop_banners` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_banners` VALUES (1,'关于味趣','/?/Gmess/view/id=2','/uploads/default/9369/80596/93695085d5c363cb63af7cf79772eafd.png',0,3,'NaN',0,'0000-00-00 00:00:00'),(2,'加盟味趣','/?/Gmess/view/id=1','/uploads/default/9c78/1bc87/9c78cb159ca87563dd11bd0de8c5c06c.png',0,3,'NaN',0,'0000-00-00 00:00:00'),(3,'商家入驻','http://www.shop.com/','/uploads/default/3a50/f7c05/3a50c7fba7aea311048f52973f688679.png',0,3,'NaN',0,'0000-00-00 00:00:00'),(4,'积分购物','/?/Uc/credit_exchange/','/uploads/default/4a53/57135/4a53175803b5253a5c4e996856fd5119.png',0,3,'NaN',0,'0000-00-00 00:00:00'),(5,'会员中心','','/uploads/default/b2b4/34e4b/b2b4e43eabc5e571993327010f321fb8.jpg',2,3,'NaN',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `wshop_banners` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_board_messages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_board_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `mtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_board_messages`
--

LOCK TABLES `wshop_board_messages` WRITE;
/*!40000 ALTER TABLE `wshop_board_messages` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_board_messages` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_expresstaff`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_expresstaff` (
  `id` int(11) NOT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `headimg` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `isnotify` tinyint(1) DEFAULT '0',
  `isexpress` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_expresstaff`
--

LOCK TABLES `wshop_expresstaff` WRITE;
/*!40000 ALTER TABLE `wshop_expresstaff` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_expresstaff` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_level` tinyint(2) DEFAULT '0' COMMENT '错误级别',
  `log_info` text COMMENT '错误信息',
  `log_url` varchar(255) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `log_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='系统日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_logs`
--

LOCK TABLES `wshop_logs` WRITE;
/*!40000 ALTER TABLE `wshop_logs` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_logs` VALUES (1,0,'访问错误：方法不存在 WdminPage->group_buy() 不存在','http://www.shop.com/?/WdminPage/group_buy','2016-08-09 16:29:23','127.0.0.1'),(2,0,'访问错误：方法不存在 WdminPage->group_buy() 不存在','http://www.shop.com/?/WdminPage/group_buy','2016-08-09 16:29:23','127.0.0.1'),(3,0,'访问错误：方法不存在 WdminPage->group_buy() 不存在','http://www.shop.com/?/WdminPage/group_buy','2016-08-09 16:29:28','127.0.0.1'),(4,0,'访问错误：方法不存在 WdminPage->group_buy() 不存在','http://www.shop.com/?/WdminPage/group_buy','2016-08-09 16:29:28','127.0.0.1'),(5,0,'访问错误：方法不存在 WdminPage->group_buy() 不存在','http://www.shop.com/?/WdminPage/group_buy','2016-08-09 16:34:02','127.0.0.1'),(6,0,'访问错误：方法不存在 WdminPage->group_buy() 不存在','http://www.shop.com/?/WdminPage/group_buy','2016-08-09 16:55:53','127.0.0.1');
/*!40000 ALTER TABLE `wshop_logs` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_menu`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relid` int(11) DEFAULT NULL,
  `reltype` tinyint(4) DEFAULT NULL,
  `relcontent` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_menu`
--

LOCK TABLES `wshop_menu` WRITE;
/*!40000 ALTER TABLE `wshop_menu` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_menu` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_recomment_company`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_recomment_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` enum('unfix','fixed','close') DEFAULT 'unfix',
  `content` text,
  `comid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_recomment_company`
--

LOCK TABLES `wshop_recomment_company` WRITE;
/*!40000 ALTER TABLE `wshop_recomment_company` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_recomment_company` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_search_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_search_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_search_record`
--

LOCK TABLES `wshop_search_record` WRITE;
/*!40000 ALTER TABLE `wshop_search_record` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_search_record` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_settings`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(512) DEFAULT NULL,
  `last_mod` datetime NOT NULL,
  `remark` varchar(255) DEFAULT '无',
  PRIMARY KEY (`key`),
  KEY `index_key` (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统设置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_settings`
--

LOCK TABLES `wshop_settings` WRITE;
/*!40000 ALTER TABLE `wshop_settings` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_settings` VALUES ('admin_setting_icon','https://cdns.ycchen.cn/images/iwshop_logo.jpg','2016-07-25 11:25:54','无'),('admin_setting_qrcode','/uploads/images/weixin.jpg','2016-07-25 11:25:54','无'),('auto_envs','0','2016-07-25 11:25:54','无'),('company_on','0','2016-07-25 11:25:54','无'),('copyright','© 2014-2016 wayqu All rights reserved.','2016-07-25 11:25:54','无'),('credit_ex','0.1','2016-07-25 11:25:54','无'),('credit_order_amount','100','2016-07-25 11:25:54','无'),('expcompany','ems,guotong,shentong,shunfeng,tiantian,yousu,yuantong,yunda,zhongtong','2016-07-22 14:10:08','无'),('exp_weight1','1000','2015-07-23 23:24:06',''),('exp_weight2','1000','2015-07-23 23:24:06',''),('order_cancel_day','30','2016-07-25 11:25:54','无'),('order_confirm_day','7','2016-07-25 11:25:54','无'),('order_express_openid','','2016-07-22 14:10:08','无'),('order_notify_openid','','2016-07-22 14:10:08','无'),('reci_cont','','2016-07-25 11:25:54','无'),('reci_exp_open','0','2016-07-25 11:25:54','无'),('reci_open','0','2016-07-25 11:25:54','无'),('reci_perc','','2016-07-25 11:25:54','无'),('reg_credit_default','','2016-07-25 11:25:54','无'),('shopname','味趣网络','2016-07-25 11:25:54','无'),('sign_credit','0','2016-07-25 11:25:54','无'),('sign_daylim','','2016-07-25 11:25:54','无'),('statcode','','2016-07-25 11:25:54','无'),('ucenter_background_image','/uploads/banner/hyzx.jpg','2016-07-25 11:25:54','无'),('welcomegmess','','2016-07-25 11:25:54','无');
/*!40000 ALTER TABLE `wshop_settings` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_settings_expfee`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_settings_expfee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `province` varchar(255) DEFAULT '',
  `citys` varchar(255) DEFAULT NULL,
  `ffee` float DEFAULT NULL,
  `ffeeadd` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统设置-运费模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_settings_expfee`
--

LOCK TABLES `wshop_settings_expfee` WRITE;
/*!40000 ALTER TABLE `wshop_settings_expfee` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_settings_expfee` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_settings_nav`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_settings_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_name` varchar(255) NOT NULL COMMENT '菜单名称',
  `nav_ico` varchar(255) NOT NULL COMMENT '显示ICO图片',
  `nav_type` int(11) NOT NULL COMMENT '菜单类型（0.超链接，1.产品分类）',
  `nav_content` text,
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='系统设置-导航';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_settings_nav`
--

LOCK TABLES `wshop_settings_nav` WRITE;
/*!40000 ALTER TABLE `wshop_settings_nav` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_settings_nav` VALUES (1,'酱油','/uploads/default/18d2/1f02d/18d20f1b92c082b685173be02f87da42.png',1,'1',0),(2,'陈醋白醋','/uploads/default/2193/55639/219365510a265ace75bebf2400a7a156.png',1,'2',0),(3,'调味酱','/uploads/default/83b1/0b91b/83b19b0798aaedf87b5be91608b68272.png',1,'3',0),(4,'色油色粉','/uploads/default/e752/38e25/e752e83ab9473936e331731b4149de46.png',1,'4',0),(5,'调味料','/uploads/default/1d95/e3d59/1d95d3ee070ecb580a7ed45d25a9689b.png',1,'5',0),(6,'鸡精鸡粉','/uploads/default/dcdb/be5bd/dcdb5ebc50a8f215efdc2255ee2b9bed.png',1,'6',0),(7,'面包糠','/uploads/default/a8f7/7927f/a8f72974f6c8e6bce6a561555f602c60.png',1,'7',0),(8,'清仓特供','/uploads/default/b2ad/42fda/b2adf2440fd091429b0d30f8202a8dec.png',1,'9',0);
/*!40000 ALTER TABLE `wshop_settings_nav` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_settings_section`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_settings_section` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `reltype` varchar(1) DEFAULT '0' COMMENT '首页版块类型0：产品分类 展示版块 1：产品列表 展示版块 2:图文消息 展示版块 3:超链接 展示版块 4:广告列表 展示版块',
  `relid` int(5) DEFAULT NULL,
  `bsort` tinyint(5) DEFAULT '0',
  `ftime` datetime DEFAULT NULL,
  `ttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_settings_section`
--

LOCK TABLES `wshop_settings_section` WRITE;
/*!40000 ALTER TABLE `wshop_settings_section` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_settings_section` VALUES (1,'清仓啦','66,21,11,4','/uploads/default/c4f7/f977f/c4f779f0eb1e8918d6313a2b5403db9c.png','1',1,0,NULL,NULL),(2,'包邮啦','41,35,33,31,28,23,51,45,2,49,43,10','/uploads/default/deb4/2044b/deb4402ae585bf2b6b1f5d52b2f5c194.png','1',1,0,NULL,NULL);
/*!40000 ALTER TABLE `wshop_settings_section` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_spec`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_name` varchar(255) NOT NULL,
  `spec_remark` varchar(255) DEFAULT NULL,
  `spec_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_spec`
--

LOCK TABLES `wshop_spec` WRITE;
/*!40000 ALTER TABLE `wshop_spec` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_spec` VALUES (1,'默认规格','',0);
/*!40000 ALTER TABLE `wshop_spec` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_spec_det`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_spec_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_id` int(11) NOT NULL,
  `det_name` varchar(255) NOT NULL,
  `det_sort` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_spec_det`
--

LOCK TABLES `wshop_spec_det` WRITE;
/*!40000 ALTER TABLE `wshop_spec_det` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_spec_det` VALUES (1,1,'默认规格',0);
/*!40000 ALTER TABLE `wshop_spec_det` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_suppliers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(120) DEFAULT NULL,
  `supp_phone` varchar(255) DEFAULT NULL,
  `supp_stime` varchar(255) DEFAULT NULL,
  `supp_sprice` varchar(255) DEFAULT NULL,
  `supp_sarea` varchar(255) DEFAULT NULL,
  `supp_desc` text,
  `supp_pass` varchar(255) DEFAULT NULL,
  `supp_lastlogin` datetime DEFAULT NULL,
  `is_verified` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`supp_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_suppliers`
--

LOCK TABLES `wshop_suppliers` WRITE;
/*!40000 ALTER TABLE `wshop_suppliers` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `wshop_suppliers` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_user_cumulate`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_user_cumulate` (
  `ref_date` date NOT NULL,
  `user_source` tinyint(2) NOT NULL DEFAULT '0',
  `cumulate_user` int(11) DEFAULT '0',
  PRIMARY KEY (`ref_date`,`user_source`),
  UNIQUE KEY `ref_date` (`ref_date`,`user_source`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信粉丝统计数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_user_cumulate`
--

LOCK TABLES `wshop_user_cumulate` WRITE;
/*!40000 ALTER TABLE `wshop_user_cumulate` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_user_cumulate` VALUES ('2016-07-17',0,2),('2016-07-18',0,2),('2016-07-19',0,2),('2016-07-20',0,2),('2016-07-21',0,2),('2016-07-22',0,2),('2016-07-23',0,2),('2016-07-24',0,2),('2016-07-25',0,3),('2016-07-26',0,3),('2016-07-27',0,3),('2016-07-30',0,3),('2016-07-31',0,3),('2016-08-01',0,3),('2016-08-02',0,3),('2016-08-03',0,3),('2016-08-04',0,3),('2016-08-05',0,3),('2016-08-06',0,3),('2016-08-07',0,3),('2016-08-08',0,3);
/*!40000 ALTER TABLE `wshop_user_cumulate` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

--
-- Table structure for table `wshop_user_summary`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wshop_user_summary` (
  `ref_date` date NOT NULL,
  `user_source` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0代表其他（包括带参数二维码） 3代表扫二维码 17代表名片分享 35代表搜号码（即微信添加朋友页的搜索） 39代表查询微信公众帐号 43代表图文页右上角菜单',
  `new_user` int(11) DEFAULT NULL,
  `cancel_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_date`,`user_source`),
  UNIQUE KEY `ref_date` (`ref_date`,`user_source`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信粉丝统计数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wshop_user_summary`
--

LOCK TABLES `wshop_user_summary` WRITE;
/*!40000 ALTER TABLE `wshop_user_summary` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `wshop_user_summary` VALUES ('2016-07-21',30,1,1),('2016-07-25',0,0,0),('2016-07-25',30,1,0);
/*!40000 ALTER TABLE `wshop_user_summary` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Tue, 09 Aug 2016 16:57:43 +0800
