-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `admin_account` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_last_login` datetime DEFAULT NULL,
  `admin_ip_address` varchar(255) DEFAULT NULL,
  `admin_auth` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`admin_account`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for admin_login_records
-- ----------------------------
DROP TABLE IF EXISTS `admin_login_records`;
CREATE TABLE `admin_login_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `ldate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `client_id` int(25) NOT NULL AUTO_INCREMENT COMMENT '会员卡号',
  `client_nickname` varchar(512) COLLATE utf8mb4_bin NOT NULL,
  `client_name` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '会员姓名',
  `client_sex` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会员性别',
  `client_phone` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '会员电话',
  `client_email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_head` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_head_lastmod` datetime DEFAULT NULL,
  `client_password` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '会员密码',
  `client_level` tinyint(3) DEFAULT '0' COMMENT '会员种类\\r\\n1为普通会员\\r\\n0为合作商',
  `client_wechat_openid` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '会员微信openid',
  `client_joindate` date NOT NULL,
  `client_province` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_city` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_address` varchar(60) COLLATE utf8mb4_bin DEFAULT '' COMMENT '会员住址',
  `client_money` float(15,2) NOT NULL DEFAULT '0.00' COMMENT '会员存款',
  `client_credit` int(15) NOT NULL DEFAULT '0' COMMENT '会员积分',
  `client_remark` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '会员备注',
  `client_groupid` int(11) DEFAULT '0',
  `client_storeid` int(10) DEFAULT '0' COMMENT '会员所属店号',
  `client_personid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_comid` int(11) DEFAULT '0' COMMENT '代理编号',
  `client_autoenvrec` tinyint(4) DEFAULT '0',
  `client_overdraft_amount` float(11,2) DEFAULT '0.00' COMMENT '用户信用总额',
  `is_com` tinyint(4) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `index_openid` (`client_wechat_openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户信息表';

-- ----------------------------
-- Table structure for client_addresses
-- ----------------------------
DROP TABLE IF EXISTS `client_addresses`;
CREATE TABLE `client_addresses` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `uname` varchar(255) COLLATE utf8_bin NOT NULL,
  `phone` varchar(255) COLLATE utf8_bin NOT NULL,
  `province` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `dist` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `addrs` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `poscode` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for client_autoenvs
-- ----------------------------
DROP TABLE IF EXISTS `client_autoenvs`;
CREATE TABLE `client_autoenvs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `envid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_cart
-- ----------------------------
DROP TABLE IF EXISTS `client_cart`;
CREATE TABLE `client_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) NOT NULL COMMENT '用户编号',
  `product_id` int(11) NOT NULL COMMENT '商品编号',
  `spec_id` int(11) DEFAULT '0' COMMENT '商品规格',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '商品数量',
  PRIMARY KEY (`id`),
  KEY `index_openid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_credit_record
-- ----------------------------
DROP TABLE IF EXISTS `client_credit_record`;
CREATE TABLE `client_credit_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `amount` int(5) DEFAULT NULL,
  `dt` datetime DEFAULT NULL,
  `reltype` tinyint(2) DEFAULT NULL,
  `relid` int(11) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_deposit_order
-- ----------------------------
DROP TABLE IF EXISTS `client_deposit_order`;
CREATE TABLE `client_deposit_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `amount` float(11,2) DEFAULT '0.00',
  `deposit_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `deposit_status` enum('wait','payed') DEFAULT 'wait',
  `deposit_serial` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `wepay_serial` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_openid` (`openid`(191))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='用户充值表';

-- ----------------------------
-- Table structure for client_envelopes
-- ----------------------------
DROP TABLE IF EXISTS `client_envelopes`;
CREATE TABLE `client_envelopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `envid` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `exp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_envelopes_type
-- ----------------------------
DROP TABLE IF EXISTS `client_envelopes_type`;
CREATE TABLE `client_envelopes_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `req_amount` float DEFAULT NULL,
  `dis_amount` float DEFAULT NULL,
  `pid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_feedbacks
-- ----------------------------
DROP TABLE IF EXISTS `client_feedbacks`;
CREATE TABLE `client_feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `feedback` text,
  `ftime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_level
-- ----------------------------
DROP TABLE IF EXISTS `client_level`;
CREATE TABLE `client_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `level_credit` int(11) NOT NULL,
  `level_discount` float DEFAULT NULL,
  `level_credit_feed` float DEFAULT NULL,
  `upable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_messages
-- ----------------------------
DROP TABLE IF EXISTS `client_messages`;
CREATE TABLE `client_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `msgtype` tinyint(2) DEFAULT '0',
  `msgcont` text,
  `msgdirect` tinyint(4) DEFAULT '0',
  `autoreped` tinyint(4) DEFAULT '0',
  `send_time` datetime DEFAULT NULL,
  `sreaded` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_message_session
-- ----------------------------
DROP TABLE IF EXISTS `client_message_session`;
CREATE TABLE `client_message_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `unread` int(11) DEFAULT '0',
  `undesc` varchar(255) DEFAULT NULL,
  `lasttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_order_address
-- ----------------------------
DROP TABLE IF EXISTS `client_order_address`;
CREATE TABLE `client_order_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel` varchar(255) COLLATE utf8_bin NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for client_product_likes
-- ----------------------------
DROP TABLE IF EXISTS `client_product_likes`;
CREATE TABLE `client_product_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `like_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni` (`openid`,`product_id`) USING BTREE,
  KEY `uopenid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_sign_record
-- ----------------------------
DROP TABLE IF EXISTS `client_sign_record`;
CREATE TABLE `client_sign_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dt` date DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `openid` varchar(150) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dt` (`dt`,`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_urecord
-- ----------------------------
DROP TABLE IF EXISTS `client_urecord`;
CREATE TABLE `client_urecord` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `amount` float(11,2) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_withdrawal_order
-- ----------------------------
DROP TABLE IF EXISTS `client_withdrawal_order`;
CREATE TABLE `client_withdrawal_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `amount` float(11,2) DEFAULT '0.00',
  `deposit_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `deposit_status` enum('wait','payed') DEFAULT 'wait',
  `deposit_serial` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `wepay_serial` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_openid` (`openid`(191))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='用户提现表';

-- ----------------------------
-- Table structure for companys
-- ----------------------------
DROP TABLE IF EXISTS `companys`;
CREATE TABLE `companys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL DEFAULT '0',
  `gid` int(11) DEFAULT '0' COMMENT '组ID',
  `name` varchar(200) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `return_percent` float(5,3) DEFAULT '0.050',
  `money` float DEFAULT '0',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account` varchar(255) DEFAULT NULL,
  `bank_personname` varchar(255) DEFAULT NULL,
  `person_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `utype` tinyint(4) DEFAULT NULL,
  `verifed` tinyint(4) DEFAULT '0',
  `alipay` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`name`,`email`,`phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='代理信息表';

-- ----------------------------
-- Table structure for company_bills
-- ----------------------------
DROP TABLE IF EXISTS `company_bills`;
CREATE TABLE `company_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comid` int(11) DEFAULT NULL,
  `bill_amount` float(10,2) DEFAULT NULL,
  `bill_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for company_income_record
-- ----------------------------
DROP TABLE IF EXISTS `company_income_record`;
CREATE TABLE `company_income_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float(11,2) NOT NULL DEFAULT '0.00',
  `date` datetime NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `com_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `pcount` int(11) NOT NULL,
  `is_seted` tinyint(4) DEFAULT '0',
  `is_reqed` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_level
-- ----------------------------
DROP TABLE IF EXISTS `company_level`;
CREATE TABLE `company_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `level_discount` float(11,2) DEFAULT NULL,
  `level_rebate_point` float(11,2) DEFAULT '0.00',
  `level_remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `level_addtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `upable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for company_spread_record
-- ----------------------------
DROP TABLE IF EXISTS `company_spread_record`;
CREATE TABLE `company_spread_record` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `com_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `product_id` int(11) NOT NULL,
  `readi` int(11) NOT NULL DEFAULT '1',
  `turned` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_spread_record_details
-- ----------------------------
DROP TABLE IF EXISTS `company_spread_record_details`;
CREATE TABLE `company_spread_record_details` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `spread_id` int(11) NOT NULL,
  `cclient_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_users
-- ----------------------------
DROP TABLE IF EXISTS `company_users`;
CREATE TABLE `company_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `comid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for credit_exchange_products
-- ----------------------------
DROP TABLE IF EXISTS `credit_exchange_products`;
CREATE TABLE `credit_exchange_products` (
  `product_id` int(11) NOT NULL,
  `product_credits` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ephone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for enterprise_users
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_users`;
CREATE TABLE `enterprise_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for envs_robblist
-- ----------------------------
DROP TABLE IF EXISTS `envs_robblist`;
CREATE TABLE `envs_robblist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `on` int(11) DEFAULT NULL,
  `remains` int(11) DEFAULT NULL,
  `envsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for envs_robrecord
-- ----------------------------
DROP TABLE IF EXISTS `envs_robrecord`;
CREATE TABLE `envs_robrecord` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `envsid` int(11) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for express_record
-- ----------------------------
DROP TABLE IF EXISTS `express_record`;
CREATE TABLE `express_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `confirm_time` datetime DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `costs` varchar(255) DEFAULT '0' COMMENT '配送时效',
  `openid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gmess_category
-- ----------------------------
DROP TABLE IF EXISTS `gmess_category`;
CREATE TABLE `gmess_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `sort` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gmess_page
-- ----------------------------
DROP TABLE IF EXISTS `gmess_page`;
CREATE TABLE `gmess_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `desc` varchar(255) DEFAULT NULL,
  `catimg` varchar(255) DEFAULT NULL,
  `thumb_media_id` varchar(255) DEFAULT NULL,
  `media_id` varchar(255) DEFAULT NULL,
  `createtime` date DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gmess_send_stat
-- ----------------------------
DROP TABLE IF EXISTS `gmess_send_stat`;
CREATE TABLE `gmess_send_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `send_date` datetime DEFAULT NULL,
  `send_count` int(11) DEFAULT NULL,
  `read_count` int(11) DEFAULT '0',
  `share_count` int(11) DEFAULT '0',
  `receive_count` int(11) DEFAULT NULL,
  `send_type` tinyint(4) DEFAULT '0',
  `msg_type` enum('text','images') DEFAULT 'images',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gmess_tasks
-- ----------------------------
DROP TABLE IF EXISTS `gmess_tasks`;
CREATE TABLE `gmess_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gmess_id` int(11) NOT NULL,
  `task_time` int(11) DEFAULT '0',
  `task_exec_time` int(11) DEFAULT '0',
  `task_finish_time` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `client_id` int(11) DEFAULT NULL COMMENT '客户编号',
  `order_time` datetime DEFAULT NULL COMMENT '订单交易时间',
  `receive_time` datetime DEFAULT NULL COMMENT '收货时间',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `order_balance` float(10,2) DEFAULT '0.00' COMMENT '余额抵现',
  `order_expfee` float(10,2) DEFAULT '0.00',
  `order_amount` float(10,2) DEFAULT '0.00' COMMENT '总价',
  `order_refund_amount` float(10,2) DEFAULT '0.00',
  `supply_price_amount` float(10,2) DEFAULT '0.00',
  `original_amount` float(10,2) DEFAULT '0.00',
  `company_id` varchar(255) COLLATE utf8_bin DEFAULT '0',
  `envs_id` int(11) DEFAULT '0',
  `product_count` int(11) DEFAULT '0',
  `order_dixian` float(10,2) DEFAULT '0.00',
  `serial_number` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `wepay_serial` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `wepay_openid` varchar(255) COLLATE utf8_bin DEFAULT '',
  `wepay_unionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bank_billno` varchar(255) COLLATE utf8_bin DEFAULT '',
  `leword` text COLLATE utf8_bin,
  `status` enum('unpay','payed','received','canceled','closed','refunded','delivering','reqing') COLLATE utf8_bin NOT NULL DEFAULT 'unpay' COMMENT '订单状态',
  `express_openid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `express_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `express_com` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `exptime` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `enterprise_id` int(11) DEFAULT '0',
  `reci_head` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `reci_cont` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `reci_tex` float(10,2) DEFAULT '0.00',
  `is_commented` tinyint(1) DEFAULT '0',
  `address_hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `supplier_id` int(11) DEFAULT '0',
  `rebated` tinyint(1) DEFAULT '0' COMMENT '是否已经返佣了',
  PRIMARY KEY (`order_id`),
  KEY `openid` (`wepay_openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders_address
-- ----------------------------
DROP TABLE IF EXISTS `orders_address`;
CREATE TABLE `orders_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel_number` varchar(255) COLLATE utf8_bin NOT NULL,
  `province` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders_comment
-- ----------------------------
DROP TABLE IF EXISTS `orders_comment`;
CREATE TABLE `orders_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `starts` tinyint(4) DEFAULT NULL,
  `content` text,
  `mtime` datetime DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `anonymous` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(20) NOT NULL COMMENT '订单编号',
  `product_id` int(20) NOT NULL COMMENT '商品编号',
  `product_count` int(10) NOT NULL COMMENT '商品数量',
  `product_discount_price` float(11,2) NOT NULL DEFAULT '0.00',
  `original_amount` float(11,2) DEFAULT NULL,
  `product_price_hash_id` int(11) NOT NULL DEFAULT '0',
  `refunded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for order_credit_available
-- ----------------------------
DROP TABLE IF EXISTS `order_credit_available`;
CREATE TABLE `order_credit_available` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cfrom` float(5,2) DEFAULT NULL,
  `cto` float(5,2) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for order_rebates
-- ----------------------------
DROP TABLE IF EXISTS `order_rebates`;
CREATE TABLE `order_rebates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `comid` int(11) DEFAULT NULL,
  `amount` float(11,5) DEFAULT '0.00000',
  `rate` float(11,2) DEFAULT '0.00' COMMENT '返佣比率',
  `rtime` datetime DEFAULT NULL,
  `status` enum('wait','pass','reject') CHARACTER SET utf8 DEFAULT 'wait',
  `rebated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_comid_orderid` (`orderid`,`comid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for order_refundment
-- ----------------------------
DROP TABLE IF EXISTS `order_refundment`;
CREATE TABLE `order_refundment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `refund_amount` float(10,2) DEFAULT '0.00',
  `refund_time` datetime DEFAULT NULL,
  `refund_type` tinyint(4) DEFAULT '0',
  `refund_serial` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `payment_type` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for products_info
-- ----------------------------
DROP TABLE IF EXISTS `products_info`;
CREATE TABLE `products_info` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `product_code` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '商品条码',
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '商品名称',
  `product_subname` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品颜色',
  `product_size` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品大小',
  `product_cat` int(11) NOT NULL DEFAULT '1',
  `product_brand` int(11) DEFAULT '0',
  `product_readi` int(11) NOT NULL DEFAULT '0',
  `product_desc` longtext COLLATE utf8_bin,
  `product_subtitle` text COLLATE utf8_bin,
  `product_serial` int(11) DEFAULT '0',
  `product_weight` varchar(11) COLLATE utf8_bin DEFAULT '0.00',
  `product_online` tinyint(4) DEFAULT '1',
  `product_credit` int(11) DEFAULT '0',
  `product_prom` int(11) DEFAULT '0',
  `product_prom_limit` int(11) DEFAULT '0',
  `product_prom_limitdate` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `product_prom_limitdays` int(11) DEFAULT '0',
  `product_prom_discount` int(11) DEFAULT '0',
  `product_expfee` float(5,2) DEFAULT '0.00' COMMENT '商品快递费用',
  `product_supplier` int(11) DEFAULT '0',
  `product_storage` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '存储条件',
  `product_origin` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '商品产地',
  `product_unit` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '商品单位',
  `product_instocks` int(11) DEFAULT '0' COMMENT '商品库存，在没有规格的时候此字段可用',
  `product_indexes` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '商品分类搜索索引',
  `supply_price` float(11,2) DEFAULT '0.00',
  `sell_price` float(11,2) DEFAULT '0.00',
  `market_price` float(11,2) DEFAULT '0.00',
  `catimg` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sort` int(10) DEFAULT '0',
  `is_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_brand
-- ----------------------------
DROP TABLE IF EXISTS `product_brand`;
CREATE TABLE `product_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_img1` varchar(255) DEFAULT NULL,
  `brand_img2` varchar(255) DEFAULT NULL,
  `brand_cat` int(11) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`brand_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_descs` text COLLATE utf8_bin,
  `cat_image` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cat_parent` int(11) NOT NULL DEFAULT '0',
  `cat_level` int(11) DEFAULT '0',
  `cat_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_credit_exanges
-- ----------------------------
DROP TABLE IF EXISTS `product_credit_exanges`;
CREATE TABLE `product_credit_exanges` (
  `product_id` int(11) NOT NULL,
  `product_credits` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for product_images
-- ----------------------------
DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(512) COLLATE utf8_bin NOT NULL,
  `image_sort` tinyint(4) DEFAULT '0',
  `image_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `index_product` (`product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_onsale
-- ----------------------------
DROP TABLE IF EXISTS `product_onsale`;
CREATE TABLE `product_onsale` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `sale_prices` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
  `store_id` int(8) NOT NULL DEFAULT '0' COMMENT '商店编号',
  `discount` int(3) NOT NULL DEFAULT '100' COMMENT '折扣',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_serials
-- ----------------------------
DROP TABLE IF EXISTS `product_serials`;
CREATE TABLE `product_serials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_name` varchar(255) DEFAULT NULL COMMENT '序列名称',
  `serial_image` varchar(255) DEFAULT NULL,
  `serial_desc` varchar(255) DEFAULT NULL,
  `relcat` tinyint(4) DEFAULT NULL,
  `relevel` tinyint(4) DEFAULT NULL,
  `sort` varchar(255) DEFAULT '0' COMMENT '排序',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_spec
-- ----------------------------
DROP TABLE IF EXISTS `product_spec`;
CREATE TABLE `product_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `spec_det_id1` int(11) DEFAULT NULL,
  `spec_det_id2` int(11) DEFAULT NULL,
  `sale_price` float(11,2) DEFAULT NULL,
  `market_price` float(11,2) DEFAULT '0.00',
  `instock` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_view_record
-- ----------------------------
DROP TABLE IF EXISTS `product_view_record`;
CREATE TABLE `product_view_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_wechat_recomemt
-- ----------------------------
DROP TABLE IF EXISTS `product_wechat_recomemt`;
CREATE TABLE `product_wechat_recomemt` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_wechat_recommend
-- ----------------------------
DROP TABLE IF EXISTS `product_wechat_recommend`;
CREATE TABLE `product_wechat_recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wechat_autoresponse
-- ----------------------------
DROP TABLE IF EXISTS `wechat_autoresponse`;
CREATE TABLE `wechat_autoresponse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `message` text,
  `rel` int(11) DEFAULT '0',
  `reltype` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wechat_subscribe_record
-- ----------------------------
DROP TABLE IF EXISTS `wechat_subscribe_record`;
CREATE TABLE `wechat_subscribe_record` (
  `recordid` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8_bin NOT NULL,
  `date` date DEFAULT NULL,
  `dv` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`recordid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for wshop_banners
-- ----------------------------
DROP TABLE IF EXISTS `wshop_banners`;
CREATE TABLE `wshop_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `banner_href` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `banner_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `banner_position` tinyint(4) DEFAULT '0',
  `reltype` tinyint(4) DEFAULT NULL,
  `relid` varchar(255) COLLATE utf8_bin DEFAULT '0',
  `sort` tinyint(4) DEFAULT '0',
  `exp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for wshop_board_messages
-- ----------------------------
DROP TABLE IF EXISTS `wshop_board_messages`;
CREATE TABLE `wshop_board_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `mtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_expresstaff
-- ----------------------------
DROP TABLE IF EXISTS `wshop_expresstaff`;
CREATE TABLE `wshop_expresstaff` (
  `id` int(11) NOT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `headimg` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `isnotify` tinyint(1) DEFAULT '0',
  `isexpress` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_logs
-- ----------------------------
DROP TABLE IF EXISTS `wshop_logs`;
CREATE TABLE `wshop_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_level` tinyint(2) DEFAULT '0' COMMENT '错误级别',
  `log_info` text CHARACTER SET utf8 COMMENT '错误信息',
  `log_url` varchar(255) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `log_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_menu
-- ----------------------------
DROP TABLE IF EXISTS `wshop_menu`;
CREATE TABLE `wshop_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relid` int(11) DEFAULT NULL,
  `reltype` tinyint(4) DEFAULT NULL,
  `relcontent` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_recomment_company
-- ----------------------------
DROP TABLE IF EXISTS `wshop_recomment_company`;
CREATE TABLE `wshop_recomment_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` enum('unfix','fixed','close') DEFAULT 'unfix',
  `content` text,
  `comid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_search_record
-- ----------------------------
DROP TABLE IF EXISTS `wshop_search_record`;
CREATE TABLE `wshop_search_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_settings
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings`;
CREATE TABLE `wshop_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(512) DEFAULT NULL,
  `last_mod` datetime NOT NULL,
  `remark` varchar(255) DEFAULT '无',
  PRIMARY KEY (`key`),
  KEY `index_key` (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统设置表';

-- ----------------------------
-- Table structure for wshop_settings_expfee
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings_expfee`;
CREATE TABLE `wshop_settings_expfee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `province` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `citys` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ffee` float DEFAULT NULL,
  `ffeeadd` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for wshop_settings_nav
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings_nav`;
CREATE TABLE `wshop_settings_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_name` varchar(255) NOT NULL COMMENT '菜单名称',
  `nav_ico` varchar(255) NOT NULL COMMENT '显示ICO图片',
  `nav_type` int(11) NOT NULL COMMENT '菜单类型（0.超链接，1.产品分类）',
  `nav_content` text,
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_settings_section
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings_section`;
CREATE TABLE `wshop_settings_section` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `pid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `reltype` varchar(1) CHARACTER SET utf8 DEFAULT '0' COMMENT '首页版块类型0：产品分类 展示版块 1：产品列表 展示版块 2:图文消息 展示版块 3:超链接 展示版块 4:广告列表 展示版块',
  `relid` int(5) DEFAULT NULL,
  `bsort` tinyint(5) DEFAULT '0',
  `ftime` datetime DEFAULT NULL,
  `ttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for wshop_spec
-- ----------------------------
DROP TABLE IF EXISTS `wshop_spec`;
CREATE TABLE `wshop_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_name` varchar(255) NOT NULL,
  `spec_remark` varchar(255) DEFAULT NULL,
  `spec_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_spec_det
-- ----------------------------
DROP TABLE IF EXISTS `wshop_spec_det`;
CREATE TABLE `wshop_spec_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_id` int(11) NOT NULL,
  `det_name` varchar(255) NOT NULL,
  `det_sort` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_suppliers
-- ----------------------------
DROP TABLE IF EXISTS `wshop_suppliers`;
CREATE TABLE `wshop_suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(120) DEFAULT NULL,
  `supp_phone` varchar(255) DEFAULT NULL,
  `supp_stime` varchar(255) DEFAULT NULL,
  `supp_sprice` varchar(255) DEFAULT NULL,
  `supp_sarea` varchar(255) DEFAULT NULL,
  `supp_desc` text,
  `supp_pass` varchar(255) DEFAULT NULL,
  `supp_lastlogin` datetime DEFAULT NULL,
  `is_verified` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`supp_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_user_cumulate
-- ----------------------------
DROP TABLE IF EXISTS `wshop_user_cumulate`;
CREATE TABLE `wshop_user_cumulate` (
  `ref_date` date NOT NULL,
  `user_source` tinyint(2) NOT NULL DEFAULT '0',
  `cumulate_user` int(11) DEFAULT '0',
  PRIMARY KEY (`ref_date`,`user_source`),
  UNIQUE KEY `ref_date` (`ref_date`,`user_source`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_user_summary
-- ----------------------------
DROP TABLE IF EXISTS `wshop_user_summary`;
CREATE TABLE `wshop_user_summary` (
  `ref_date` date NOT NULL,
  `user_source` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0代表其他（包括带参数二维码） 3代表扫二维码 17代表名片分享 35代表搜号码（即微信添加朋友页的搜索） 39代表查询微信公众帐号 43代表图文页右上角菜单',
  `new_user` int(11) DEFAULT NULL,
  `cancel_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_date`,`user_source`),
  UNIQUE KEY `ref_date` (`ref_date`,`user_source`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 导入默认分组
INSERT INTO `client_level` VALUES ('0', '普通会员', '0', '100', '0', '0');

-- 导入默认系列
INSERT INTO `product_serials` VALUES ('0', '默认', NULL, NULL, NULL, NULL, '0', '0');

-- 默认分组id=0
UPDATE `client_level`
SET id = 0;

-- 默认系列id=0
UPDATE `product_serials`
SET id = 0;

-- 导入管理员账户 账号admin，密码admin
INSERT INTO `admin` (admin_name, admin_account,admin_password, admin_auth) VALUE
  ('超级管理员', 'admin',
   '4a0894d6e8f3b5c6ee0c519bcb98b6b7fd0affcb343ace3a093f29da4b2535604b61f0aebd60c0f0e49cc53adba3fffb',
   'stat,orde,prod,gmes,user,comp,sett');

-- 导入默认设置
INSERT INTO `wshop_settings` VALUES ('company_on', '0', '2015-11-22 13:12:18','');
INSERT INTO `wshop_settings` VALUES ('copyright', '© 2014-2015 iWshop All rights reserved.', '2015-11-22 13:12:18','');
INSERT INTO `wshop_settings` VALUES ('credit_ex', '0.1', '2015-11-22 13:11:49','');
INSERT INTO `wshop_settings` VALUES ('credit_order_amount', '100', '2015-11-22 13:11:49','');
INSERT INTO `wshop_settings` VALUES ('expcompany', 'ems,guotong,shentong,shunfeng,tiantian,yousu,yuantong,yunda,zhongtong', '2015-11-15 00:08:36','');
INSERT INTO `wshop_settings` VALUES ('exp_weight1', '1000', '2015-07-23 23:24:06','');
INSERT INTO `wshop_settings` VALUES ('exp_weight2', '1000', '2015-07-23 23:24:06','');
INSERT INTO `wshop_settings` VALUES ('order_cancel_day', '30', '2015-11-22 13:12:18','');
INSERT INTO `wshop_settings` VALUES ('order_confirm_day', '30', '2015-11-22 13:12:18','');

-- 导入默认logo
INSERT INTO `wshop_settings` VALUES ('admin_setting_icon', 'https://cdn.iwshop.org/images/iwshop_logo.jpg', '2015-11-22 13:12:18','');

-- 导入默认值
INSERT INTO `company_level` VALUES(1,'0','I级','0.10','2016-01-25 00:00:00' );
INSERT INTO `company_level` VALUES(2,'1','II级','0.10','2016-01-25 00:00:00' );
INSERT INTO `company_level` VALUES(3,'2','III级','0.10','2016-01-25 00:00:00' );